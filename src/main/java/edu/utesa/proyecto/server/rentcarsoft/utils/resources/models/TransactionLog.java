package edu.utesa.proyecto.server.rentcarsoft.utils.resources.models;

import java.util.Date;

/**
 * EL typeClass dice la clase de conversión de los String.
 * <p>
 * Created by ronald on 4/8/17.
 */
public final class TransactionLog {

    Date dateTransaction;

    Long clientID;

    String typeClassClient;
    String typeClassServer;

    String serverFrame;
    String clientFrame;

    public TransactionLog() {
    }

    public TransactionLog(Long clientID, String typeClassClient, String typeClassServer, String serverFrame, String clientFrame) {
        dateTransaction = new Date();
        this.clientID = clientID;
        this.typeClassClient = typeClassClient;
        this.typeClassServer = typeClassServer;
        this.serverFrame = serverFrame;
        this.clientFrame = clientFrame;
    }

    public TransactionLog(Date dateTransaction, Long clientID, String typeClassClient, String typeClassServer, String serverFrame, String clientFrame) {
        this.dateTransaction = dateTransaction;
        this.clientID = clientID;
        this.typeClassClient = typeClassClient;
        this.typeClassServer = typeClassServer;
        this.serverFrame = serverFrame;
        this.clientFrame = clientFrame;
    }
}
