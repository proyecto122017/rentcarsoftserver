package edu.utesa.proyecto.server.rentcarsoft.utils.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Manage sessions
 * <p>
 * Created by ronald on 9/25/17.
 */
public class SessionManager {

    private static long timeExpire = 15 * 60 * 1000;

    private final List<Session> sessions = new ArrayList<>();

    public final void setTimeExpire(long timeToExpire) {
        timeExpire = timeToExpire;
    }

    private static boolean expire(Date date) {
        return new Date().getTime() - date.getTime() <= timeExpire;
    }

    private Session getSession(long id) {
        if (id <= 0) {
            return null;
        }
        for (Session session : sessions) {
            if (session.id == id) {
                if (expire(session.dateLastTransaction)) {
                    return null;
                }
                return session;
            }
        }
        return null;
    }

    private String revalidate(long idActivator) {
        Session session = getSession(idActivator);
        if (session == null) {
            return "";
        }
        if (!expire(session.dateLastTransaction)) {
            return session.idSession;
        }
        session.idSession = UUID.randomUUID().toString().replaceAll("-", "");
        session.dateLastTransaction = new Date();
        return session.idSession;
    }

    public final String login(long id) {
        if (id <= 0) {
            return null;
        }
        if (isLogin(id)) {
            return revalidate(id);
        }
        Session session = new Session();
        session.idSession = UUID.randomUUID().toString().replaceAll("-", "");
        session.id = id;
        session.dateLastTransaction = new Date();
        synchronized (sessions) {
            sessions.add(session);
        }
        return session.idSession;
    }

    public final synchronized boolean isLogin(long id) {
        if (id <= 0) {
            return false;
        }
        for (Session session : sessions) {
            if (session.id == id) {
                if (expire(session.dateLastTransaction)) {
                    return false;
                }
                session.dateLastTransaction = new Date();
                return true;
            }
        }
        return false;
    }

    public final synchronized boolean isLogin(String idSession) {
        if (idSession == null || idSession.equals("")) {
            return false;
        }
        for (Session session : sessions) {
            if (session.idSession.equals(idSession)) {
                if (expire(session.dateLastTransaction)) {
                    return false;
                }
                session.dateLastTransaction = new Date();
                return true;
            }
        }
        return false;
    }

    public final synchronized Long getLogin(String idSession) {
        if (idSession == null || idSession.equals("")) {
            return null;
        }
        for (Session session : sessions) {
            if (session.idSession.equals(idSession)) {
                if (expire(session.dateLastTransaction)) {
                    return null;
                }
                session.dateLastTransaction = new Date();
                return session.id;
            }
        }
        return null;
    }
}
