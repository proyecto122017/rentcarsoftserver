package edu.utesa.proyecto.server.rentcarsoft.utils.resources;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.utesa.proyecto.server.rentcarsoft.utils.resources.models.TransactionLog;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Log Transactions
 * <p>
 * Created by ronald on 9/25/17.
 */
public class ResourceLogger {

    private static ResourceLogger instance = null;

    private static final String DATE_HOUR_FORMAT = "MM-dd-yyyy HH:mm:ss";
    private final Gson gson = new GsonBuilder().setDateFormat(DATE_HOUR_FORMAT).setPrettyPrinting().create();

    private static final File LOGGER = new File("./LOGS/Transactions.elp");
    private PrintWriter writer;

    private ResourceLogger() throws FileNotFoundException, UnsupportedEncodingException {
        //noinspection ResultOfMethodCallIgnored
        LOGGER.mkdirs();
        writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(LOGGER, true), StandardCharsets.UTF_8)));
    }

    public synchronized static ResourceLogger get() throws FileNotFoundException, UnsupportedEncodingException {
        if (instance == null) {
            instance = new ResourceLogger();
        }
        return instance;
    }

    public final String log(Long clientID, Class clientClass, String client, Object server) {
        String response = stringify(server);
        loggerTransactionLog(new TransactionLog(clientID, clientClass.toString(), server.getClass().toString(), response, client));
        return response;
    }

    private void loggerTransactionLog(TransactionLog transactionLog) {
        writer.println(stringify(transactionLog));
        writer.flush();
    }

    private String stringify(Object object) {
        try {
            return gson.toJson(object);
        } catch (Exception ex) {
            return null;
        }
    }
}
