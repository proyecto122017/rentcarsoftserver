package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Offer
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("offerService")
@Transactional(rollbackFor = Exception.class)
class OfferService {

    private Offer createBase(Offer offer, String userName) {
        if (offer != null && !offer.hasErrors()) {
            if (Offer.exists(offer.id)) {
                offer.lastUpdated = new Date()
                offer.modifyBy = userName
                return offer.merge(flush: true, failOnError: true)
            } else {
                offer.createdBy = userName
                offer.dateCreated = new Date()
                offer = offer.save(flush: true, failOnError: true)
                return offer
            }
        }
        return null
    }

    Offer bootStrap(Offer offer) {
        return createBase(offer, Constants.ROOT)
    }

    Offer create(Offer offer, LoginManager loginManager) {
        return createBase(offer, loginManager.getUsername())
    }

    List<Offer> list(boolean enabled, int start, int size) {
        return Offer.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Offer> list(boolean enabled, boolean admin, int start, int size) {
        return Offer.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Offer> list(String name, int size) {
        return Offer.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Offer> list(boolean enabled) {
        return Offer.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Offer> list(boolean enabled, boolean admin) {
        return Offer.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Offer refresh(Long id) {
        Offer offer = Offer.findById(id)
        return offer
    }

    int count(boolean enabled) {
        return Offer.countByEnabled(enabled)
    }

    Offer delete(Offer offer, LoginManager loginManager) {
        if (!offer) {
            return null
        }
        offer = Offer.findById(offer.id)
        offer.enabled = !offer.enabled
        return create(offer, loginManager)
    }

    Offer byId(Long id) {
        return Offer.findByIdAndEnabled(id, true)
    }

    Offer changePassword(Offer offer, LoginManager loginManager) {
        return create(offer, loginManager)
    }

    Long findMaxCode() {
        Offer offer = Offer.withCriteria {
            order("id", "desc")
        }.find() as Offer
        if (!offer) {
            return 0
        }
        return offer.code
    }

}
