package edu.utesa.proyecto.server.rentcarsoft.domains.administration

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.models.communication.administration.EnterpriseAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Enterprise implements APIConverter<EnterpriseAPI> {

    int code
    GeneralInfo generalInfo

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [listRentcars: Rentcar]

    static constraints = {
        code unique: true

    }

    static mapping = {
        table "enterprises"

        generalInfo fetch: "join"
        listRentcars lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Enterprise) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    EnterpriseAPI toAPI() {
        EnterpriseAPI enterpriseAPI = new EnterpriseAPI()
        enterpriseAPI.code = code
        if (generalInfo) {
            enterpriseAPI.generalInfo = generalInfo.toAPI()
        }
        enterpriseAPI.rentcarList = new ArrayList<>()
        for (Rentcar rentcar : listRentcars) {
            enterpriseAPI.rentcarList.add(rentcar.toAPI())
        }
        return enterpriseAPI
    }
}
