package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.domains.security.Gender
import edu.utesa.proyecto.server.rentcarsoft.domains.security.Occupation
import edu.utesa.proyecto.server.rentcarsoft.domains.security.Permission
import edu.utesa.proyecto.server.rentcarsoft.domains.security.User
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Person
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.models.communication.LoginAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.GenderAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.OccupationAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.PermissionAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.UserAPI
import edu.utesa.proyecto.server.rentcarsoft.services.SecurityService
import edu.utesa.proyecto.server.rentcarsoft.services.security.GenderService
import edu.utesa.proyecto.server.rentcarsoft.services.security.OccupationService
import edu.utesa.proyecto.server.rentcarsoft.services.security.PermissionService
import edu.utesa.proyecto.server.rentcarsoft.services.security.UserService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/security")
@CompileStatic
class SecurityResources {

    static final SessionManager sessionManager = new SessionManager()

    @Autowired
    private SecurityService securityService
    @Autowired
    private UserService userService
    @Autowired
    private GenderService genderService
    @Autowired
    private OccupationService occupationService
    @Autowired
    private PermissionService permissionService

    @GET
    static String isUP() {
        return "OK"
    }

    @POST
    @Path('/login')
    String login(@FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
        LoginAPI loginAPI = Constants.instance.convert(request, LoginAPI.class)
        if (!loginAPI) {
            return ""
        }
        LoginManager loginManager = securityService.authenticate(loginAPI.username, loginAPI.password)
        if (loginManager) {
            String response = Constants.instance.stringify(sessionManager.login(loginManager.username))
            return response
        }
        return ""
    }

    @POST
    @Path('/create/gender')
    String createGender(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        GenderAPI genderAPI = Constants.instance.convert(request, GenderAPI.class)
        if (!genderAPI) {
            return ""
            // No parsea el objeto que enviaron
        }
        Gender gender = new Gender()
        gender.code = genderService.findMaxCode() + 1 as Integer
        gender.description = genderAPI.description
        genderService.bootStrap(gender)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/genders')
    String listGenders(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<GenderAPI> genderAPIS = new ArrayList<>()
        for (Gender gender : genderService.list(true)) {
            genderAPIS.add(gender.toAPI())
        }
        return Constants.instance.stringify(genderAPIS)
    }

    @POST
    @Path('/genderlike')
    String listGendersLike(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
        if (!sessionManager.isLogin(session)) {
            return ""
        }
        return genderService.likeGenres(request).size() + ""
    }

    @POST
    @Path('/create/occupation')
    String createOccupation(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        OccupationAPI occupationAPI = Constants.instance.convert(request, OccupationAPI.class)
        if (!occupationAPI) {
            // No parsea el objeto que enviaron
        }
        Occupation occupation = new Occupation()
        occupation.code = occupationService.findMaxCode() + 1 as Integer
        occupation.description = occupationAPI.description ?: ""
        occupationService.bootStrap(occupation)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/occupations')
    String listOccupations(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<OccupationAPI> occupationAPIS = new ArrayList<>()
        for (Occupation occupation : occupationService.list(true)) {
            occupationAPIS.add(occupation.toAPI())
        }
        return Constants.instance.stringify(occupationAPIS)
    }

    @POST
    @Path('/create/permissions')
    String createPermissions(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        PermissionAPI permissionAPI = Constants.instance.convert(request, PermissionAPI.class)
        if (!permissionAPI) {
            // No parsea el objeto que enviaron
        }
        Permission permission = new Permission()
        permission.code = permissionService.findMaxCode() + 1 as Integer
        permission.name = permissionAPI.name
        permission.description = permissionAPI.description ?: ""
        permissionService.bootStrap(permission)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/permissions')
    String listPermissions(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<PermissionAPI> permissionAPIS = new ArrayList<>()
        for (Permission permission : permissionService.list(true)) {
            permissionAPIS.add(permission.toAPI())
        }
        return Constants.instance.stringify(permissionAPIS)
    }

    @POST
    @Path('/create/users')
    String createUsers(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        UserAPI userAPI = Constants.instance.convert(request, UserAPI.class)
        if (!userAPI) {
            // No parsea el objeto que enviaron
        }
        User user = new User()
        user.code = userService.findMaxCode() + 1 as Integer
        user.username = userAPI.username
        user.password = userAPI.password
        user.name = userAPI.name
        user.person = userAPI.person as Person
        user.registerDate = userAPI.registerDate
        user.ingressDate = userAPI.ingressDate
        userService.bootStrap(user)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/users')
    String listUsers(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<UserAPI> users = new ArrayList<>()
        for (User user : userService.list(true)) {
            users.add(user.toAPI())
        }
        return Constants.instance.stringify(users)
    }
}
