package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Entities
import edu.utesa.proyecto.server.rentcarsoft.services.utils.EntitiesService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicEntities {

    @Autowired
    private EntitiesService entitiesService

    Entities vehiculo
    Entities pago
    Entities renta
    Entities mantenimiento
    Entities usuario
    Entities empresa
    Entities oficina
    Entities factura
    Entities identification

    BasicEntities() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        vehiculo = create("Vehiculo")
        pago = create("Pago")
        renta = create("Renta")
        mantenimiento = create("Mantenimiento")
        usuario = create("Usuario")
        empresa = create("Empresa")
        oficina = create("Oficina")
        factura = create("Factura")
        identification = create("Identification")
    }

    private Entities create(String name) {
        Entities entities = entitiesService.byDescription(name)
        if (entities) {
            return entities
        }
        entities = new Entities()
        entities.code = entitiesService.findMaxCode() + 1 as Integer
        entities.description = name
        return entitiesService.bootStrap(entities)
    }
}
