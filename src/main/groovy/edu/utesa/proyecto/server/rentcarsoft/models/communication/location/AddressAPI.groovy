package edu.utesa.proyecto.server.rentcarsoft.models.communication.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Sector
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class AddressAPI {

    int code
    String description
    SectorAPI sector

}
