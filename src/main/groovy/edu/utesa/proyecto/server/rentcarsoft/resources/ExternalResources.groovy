package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.stereotype.Component

import javax.ws.rs.GET
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/external")
@CompileStatic
class ExternalResources {

    static final SessionManager sessionManager = new SessionManager()

//    @Autowired
//    private ProfesorService profesorService
//    @Autowired
//    private MateriaService materiaService
//    @Autowired
//    private GradoService gradoService
//    @Autowired
//    private EstudianteService estudianteService
//    @Autowired
//    private InscripcionService inscripcionService

    @GET
    static String isUP() {
        return "OK"
    }

//    @GET
//    @Path('/profesors')
//    String listProfesors(@QueryParam(Constants.PARAM_DATA) String request) {
//    println "Me enviaron: " + request
//    if ( !sessionManager.isLogin ( session ) ) {
//        return ""
//    }
//        List<ProfesorAPI> profesorAPIS = new ArrayList<>()
//        for (Profesor profesor : profesorService.list(true)) {
//            profesorAPIS.add(profesor.toAPI())
//        }
//        return Constants.instance.stringify(profesorAPIS)
//    }
//
//    @GET
//    @Path('/grados')
//    String listGrados(@QueryParam(Constants.PARAM_DATA) String request) {
//    println "Me enviaron: " + request
//    if ( !sessionManager.isLogin ( session ) ) {
//        return ""
//    }
//        List<GradoAPI> gradoAPIS = new ArrayList<>()
//        for (Grado grado : gradoService.list(true)) {
//            gradoAPIS.add(grado.toAPI())
//        }
//        return Constants.instance.stringify(gradoAPIS)
//    }
//
//    @GET
//    @Path('/materias')
//    String listMaterias(@QueryParam(Constants.PARAM_DATA) String gradoName) {
//        println "Me enviaron: " + gradoName
//        List<MateriaAPI> materiaAPIS = new ArrayList<>()
//        Grado grado = gradoService.byName(gradoName)
//        for (Materia materia : materiaService.listGradoMaterias(grado)) {
//            materiaAPIS.add(materia.toAPI())
//        }
//        return Constants.instance.stringify(materiaAPIS)
//    }

//    @GET
//    @Path('/estudiantes')
//    String listEstudiantes(@QueryParam(Constants.PARAM_DATA) String gradoName) {
//        println "Me enviaron: " + gradoName
//        List<EstudianteAPI> materiaAPIS = new ArrayList<>()
//        Grado grado = gradoService.byName(gradoName)
//        for (Estudiante estudiante : inscripcionService.li(grado)) {
//            materiaAPIS.add(estudiante.toAPI())
//        }
//        return Constants.instance.stringifyCifrado(materiaAPIS)
//    }
}
