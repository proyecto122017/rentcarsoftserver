package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.OfferCross
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("offerCrossService")
@Transactional(rollbackFor = Exception.class)
class OfferCrossService {

    private OfferCross createBase(OfferCross offerCross, String userName) {
        if (offerCross != null && !offerCross.hasErrors()) {
            if (OfferCross.exists(offerCross.id)) {
                offerCross.lastUpdated = new Date()
                offerCross.modifyBy = userName
                return offerCross.merge(flush: true, failOnError: true)
            } else {
                offerCross.createdBy = userName
                offerCross.dateCreated = new Date()
                offerCross = offerCross.save(flush: true, failOnError: true)
                return offerCross
            }
        }
        return null
    }

    OfferCross bootStrap(OfferCross offerCross) {
        return createBase(offerCross, Constants.ROOT)
    }

    OfferCross create(OfferCross offerCross, LoginManager loginManager) {
        return createBase(offerCross, loginManager.getUsername())
    }

    List<OfferCross> list(boolean enabled, int start, int size) {
        return OfferCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OfferCross> list(boolean enabled, boolean admin, int start, int size) {
        return OfferCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<OfferCross> list(String name, int size) {
        return OfferCross.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<OfferCross> list(boolean enabled) {
        return OfferCross.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<OfferCross> list(boolean enabled, boolean admin) {
        return OfferCross.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    OfferCross refresh(Long id) {
        OfferCross offerCross = OfferCross.findById(id)
        return offerCross
    }

    int count(boolean enabled) {
        return OfferCross.countByEnabled(enabled)
    }

    OfferCross delete(OfferCross offerCross, LoginManager loginManager) {
        if (!offerCross) {
            return null
        }
        offerCross = OfferCross.findById(offerCross.id)
        offerCross.enabled = !offerCross.enabled
        return create(offerCross, loginManager)
    }

    OfferCross byId(Long id) {
        return OfferCross.findByIdAndEnabled(id, true)
    }

    OfferCross changePassword(OfferCross offerCross, LoginManager loginManager) {
        return create(offerCross, loginManager)
    }

    Long findMaxCode() {
        OfferCross offerCross = OfferCross.withCriteria {
            order("id", "desc")
        }.find() as OfferCross
        if (!offerCross) {
            return 0
        }
        return offerCross.code
    }

}
