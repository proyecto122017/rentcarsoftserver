package edu.utesa.proyecto.server.rentcarsoft.services.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Coordinates
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("coordinatesService")
@Transactional(rollbackFor = Exception.class)
class CoordinatesService {

    private Coordinates createBase(Coordinates coordinates, String userName) {
        if (coordinates != null && !coordinates.hasErrors()) {
            if (Coordinates.exists(coordinates.id)) {
                coordinates.lastUpdated = new Date()
                coordinates.modifyBy = userName
                return coordinates.merge(flush: true, failOnError: true)
            } else {
                coordinates.createdBy = userName
                coordinates.dateCreated = new Date()
                coordinates = coordinates.save(flush: true, failOnError: true)
                return coordinates
            }
        }
        return null
    }

    Coordinates bootStrap(Coordinates coordinates) {
        return createBase(coordinates, Constants.ROOT)
    }

    Coordinates create(Coordinates coordinates, LoginManager loginManager) {
        return createBase(coordinates, loginManager.getUsername())
    }

    List<Coordinates> list(boolean enabled, int start, int size) {
        return Coordinates.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Coordinates> list(boolean enabled, boolean admin, int start, int size) {
        return Coordinates.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Coordinates> list(String name, int size) {
        return Coordinates.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Coordinates> list(boolean enabled) {
        return Coordinates.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Coordinates> list(boolean enabled, boolean admin) {
        return Coordinates.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Coordinates refresh(Long id) {
        Coordinates coordinates = Coordinates.findById(id)
        return coordinates
    }

    int count(boolean enabled) {
        return Coordinates.countByEnabled(enabled)
    }

    Coordinates delete(Coordinates coordinates, LoginManager loginManager) {
        if (!coordinates) {
            return null
        }
        coordinates = Coordinates.findById(coordinates.id)
        coordinates.enabled = !coordinates.enabled
        return create(coordinates, loginManager)
    }

    Coordinates byId(Long id) {
        return Coordinates.findByIdAndEnabled(id, true)
    }

    Coordinates byCode(Integer id) {
        return Coordinates.findByCodeAndEnabled(id, true)
    }

    Coordinates changePassword(Coordinates coordinates, LoginManager loginManager) {
        return create(coordinates, loginManager)
    }

    Long findMaxCode() {
        Coordinates coordinates = Coordinates.withCriteria {
            order("id", "desc")
        }.find() as Coordinates
        if (!coordinates) {
            return 0
        }
        return coordinates.code
    }

}
