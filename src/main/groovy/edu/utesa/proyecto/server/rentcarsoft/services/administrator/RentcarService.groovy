package edu.utesa.proyecto.server.rentcarsoft.services.administrator

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Rentcar
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.criteria.CriteriaBuilder

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("rentcarService")
@Transactional(rollbackFor = Exception.class)
class RentcarService {

    private Rentcar createBase(Rentcar rentcar, String userName) {
        if (rentcar != null && !rentcar.hasErrors()) {
            if (Rentcar.exists(rentcar.id)) {
                rentcar.lastUpdated = new Date()
                rentcar.modifyBy = userName
                return rentcar.merge(flush: true, failOnError: true)
            } else {
                rentcar.createdBy = userName
                rentcar.dateCreated = new Date()
                rentcar = rentcar.save(flush: true, failOnError: true)
                return rentcar
            }
        }
        return null
    }

    Rentcar bootStrap(Rentcar rentcar) {
        return createBase(rentcar, Constants.ROOT)
    }

    Rentcar create(Rentcar rentcar, LoginManager loginManager) {
        return createBase(rentcar, loginManager.getUsername())
    }

    List<Rentcar> list(boolean enabled, int start, int size) {
        return Rentcar.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Rentcar> list(boolean enabled, boolean admin, int start, int size) {
        return Rentcar.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Rentcar> list(String name, int size) {
        return Rentcar.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            eq "enabled", true
        }.findAll()
    }

    List<Rentcar> list() {
        return Rentcar.findAll()
    }

    List<Rentcar> list(boolean enabled, boolean admin) {
        return Rentcar.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Rentcar refresh(Long id) {
        Rentcar rentcar = Rentcar.findById(id)
        return rentcar
    }

    int count(boolean enabled) {
        return Rentcar.countByEnabled(enabled)
    }

    Rentcar delete(Rentcar rentcar, LoginManager loginManager) {
        if (!rentcar) {
            return null
        }
        rentcar = Rentcar.findById(rentcar.id)
        rentcar.enabled = !rentcar.enabled
        return create(rentcar, loginManager)
    }

    Rentcar byId(Long id) {
        return Rentcar.findByIdAndEnabled(id, true)
    }

    Rentcar byCode(Integer code) {
        return Rentcar.findByCodeAndEnabled(code, true)
    }

    Rentcar changePassword(Rentcar rentcar, LoginManager loginManager) {
        return create(rentcar, loginManager)
    }

    Long findMaxCode() {
        Rentcar rentcar = Rentcar.withCriteria {
            order("id", "desc")
        }.find() as Rentcar
        if (!rentcar) {
            return 1
        }
        return rentcar.code
    }

}
