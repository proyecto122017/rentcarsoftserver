package edu.utesa.proyecto.server.rentcarsoft.domains.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Insurance
import edu.utesa.proyecto.server.rentcarsoft.domains.control.VehicleMaintenance
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Photos
import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleBranchOfficeAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class VehicleBranchOffice implements APIConverter<VehicleBranchOfficeAPI> {

    int code
    String chassis
    String year
    Vehicle vehicle
    VehicleColor vehicleColor
    VehicleGPS vehicleGPS
    VehicleVersion vehicleVersion
    VehicleMaintenance vehicleMaintenance
    Insurance insurance
    BigDecimal price

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [listPhotos: Photos]

    static constraints = {
        code unique: true
        chassis nullable: true
        vehicle nullable: true
        vehicleColor nullable: true
        insurance nullable: true
        vehicleVersion nullable: true
        vehicleGPS nullable: true
        year nullable: true
        vehicleMaintenance nullable: true
        price nullable: true
    }

    static mapping = {
        table "veh_branchoffice"

        vehicle fetch: 'join'
        vehicleMaintenance fetch: 'join'
        insurance fetch: 'join'
        vehicleColor fetch: 'join'
        vehicleGPS fetch: 'join'
        vehicleVersion fetch: 'join'

        listPhotos lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((VehicleBranchOffice) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    VehicleBranchOfficeAPI toAPI() {
        VehicleBranchOfficeAPI vehicleBranchOfficeAPI = new VehicleBranchOfficeAPI()
        vehicleBranchOfficeAPI.code = code
        vehicleBranchOfficeAPI.year = year
        if (vehicle) {
            vehicleBranchOfficeAPI.vehicle = vehicle.toAPI()
        }
        vehicleBranchOfficeAPI.chassis = chassis
        if (vehicleColor) {
            vehicleBranchOfficeAPI.vehicleColor = vehicleColor.toAPI()
        }
        if (vehicleGPS) {
            vehicleBranchOfficeAPI.vehicleGPS = vehicleGPS.toAPI()
        }
        if (vehicleVersion) {
            vehicleBranchOfficeAPI.vehicleVersion = vehicleVersion.toAPI()
        }
        vehicleBranchOfficeAPI.price = price
        return vehicleBranchOfficeAPI
    }
}
