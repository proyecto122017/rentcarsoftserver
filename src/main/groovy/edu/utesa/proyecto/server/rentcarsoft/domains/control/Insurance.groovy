package edu.utesa.proyecto.server.rentcarsoft.domains.control

import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.InsuranceAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Insurance implements APIConverter<InsuranceAPI> {

    int code
    String description
    Long coverage

    Date expirationDate

    boolean original = true

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        coverage nullable: true
        expirationDate nullable: true
    }

    static mapping = {
        table "insurance"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Insurance) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    InsuranceAPI toAPI() {
        InsuranceAPI insuranceAPI = new InsuranceAPI()
        insuranceAPI.code = code
        insuranceAPI.description = description
        insuranceAPI.coverage = coverage
        return insuranceAPI
    }
}
