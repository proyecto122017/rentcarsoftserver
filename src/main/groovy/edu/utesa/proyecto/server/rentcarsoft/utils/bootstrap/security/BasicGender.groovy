package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.security

import edu.utesa.proyecto.server.rentcarsoft.domains.security.Gender
import edu.utesa.proyecto.server.rentcarsoft.services.security.GenderService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicGender {

    @Autowired
    private GenderService genderService

    Gender hombre
    Gender mujer
    Gender otro

    BasicGender() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        hombre = create(1, "Hombre")
        mujer = create(2, "Mujer")
        otro = create(3, "Otro")
    }

    private Gender create(int code, String name) {
        Gender gender = genderService.byCode(code)
        if (!gender) {
            gender = new Gender()
            gender.code = code
            gender.description = name
            return genderService.bootStrap(gender)
        }
        return gender
    }
}
