package edu.utesa.proyecto.server.rentcarsoft.domains.control

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.RentalAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Rental implements APIConverter<RentalAPI> {

    int code
    String description
    Types rentalType

    Date pickup_date
    Date return_date

    VehicleCondition vehicleCondition

    boolean original = true

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [listVehicleBrandOffice: VehicleBranchOffice]

    static constraints = {
        code unique: true

    }

    static mapping = {
        table "rental"

        vehicleCondition fetch: "join"
        rentalType fetch: "join"
        listVehicleBrandOffice lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Rental) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    RentalAPI toAPI() {
        RentalAPI rentalAPI = new RentalAPI()
        rentalAPI.code = code
        rentalAPI.description = description
        if (rentalType) {
            rentalAPI.rentalType = rentalType.toAPI()
        }
        rentalAPI.pickup_date = pickup_date
        rentalAPI.return_date = return_date
        if (vehicleCondition) {
            rentalAPI.vehicleCondition = vehicleCondition.toAPI()
        }
        rentalAPI.vehicleBrandOfficeList = new ArrayList<>()
        if (listVehicleBrandOffice.size() > 0) {
            for (VehicleBranchOffice vehicleBranchOffice : listVehicleBrandOffice) {
                rentalAPI.vehicleBrandOfficeList.add(vehicleBranchOffice.toAPI())
            }
        }
        return rentalAPI
    }
}
