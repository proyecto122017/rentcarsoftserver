package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleModel
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleVersion
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleModelService")
@Transactional(rollbackFor = Exception.class)
class VehicleModelService {

    private VehicleModel createBase(VehicleModel vehicleModel, String userName) {
        if (vehicleModel != null && !vehicleModel.hasErrors()) {
            if (VehicleModel.exists(vehicleModel.id)) {
                vehicleModel.lastUpdated = new Date()
                vehicleModel.modifyBy = userName
                return vehicleModel.merge(flush: true, failOnError: true)
            } else {
                vehicleModel.createdBy = userName
                vehicleModel.dateCreated = new Date()
                vehicleModel = vehicleModel.save(flush: true, failOnError: true)
                return vehicleModel
            }
        }
        return null
    }

    VehicleModel bootStrap(VehicleModel vehicleModel) {
        return createBase(vehicleModel, Constants.ROOT)
    }

    VehicleModel create(VehicleModel vehicleModel, LoginManager loginManager) {
        return createBase(vehicleModel, loginManager.getUsername())
    }

    List<VehicleModel> list(boolean enabled, int start, int size) {
        return VehicleModel.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleModel> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleModel.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleModel> list(String name, int size) {
        return VehicleModel.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleModel> list(boolean enabled) {
        return VehicleModel.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleModel> list(boolean enabled, boolean admin) {
        return VehicleModel.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleModel refresh(Long id) {
        VehicleModel vehicleModel = VehicleModel.findById(id)
        return vehicleModel
    }

    int count(boolean enabled) {
        return VehicleModel.countByEnabled(enabled)
    }

    VehicleModel delete(VehicleModel vehicleModel, LoginManager loginManager) {
        if (!vehicleModel) {
            return null
        }
        vehicleModel = VehicleModel.findById(vehicleModel.id)
        vehicleModel.enabled = !vehicleModel.enabled
        return create(vehicleModel, loginManager)
    }

    VehicleModel byEmail(String vehicleModel) {
        return VehicleModel.findByEmailAndEnabled(vehicleModel, true)
    }

    VehicleModel byId(Long id) {
        return VehicleModel.findByIdAndEnabled(id, true)
    }

    VehicleModel byCode(int id) {
        return VehicleModel.findByCodeAndEnabled(id, true)
    }

    Long findMaxCode() {
        VehicleModel vehicleModel = VehicleModel.withCriteria {
            order("id", "desc")
        }.find() as VehicleModel
        if (!vehicleModel) {
            return 0
        }
        return vehicleModel.code
    }
}
