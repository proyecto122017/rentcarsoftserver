package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Person
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("personService")
@Transactional(rollbackFor = Exception.class)
class PersonService {

    private Person createBase(Person person, String userName) {
        if (person != null && !person.hasErrors()) {
            if (Person.exists(person.id)) {
                person.lastUpdated = new Date()
                person.modifyBy = userName
                return person.merge(flush: true, failOnError: true)
            } else {
                person.createdBy = userName
                person.dateCreated = new Date()
                person = person.save(flush: true, failOnError: true)
                return person
            }
        }
        return null
    }

    Person bootStrap(Person person) {
        return createBase(person, Constants.ROOT)
    }

    Person create(Person person, LoginManager loginManager) {
        return createBase(person, loginManager.getUsername())
    }

    List<Person> list(boolean enabled, int start, int size) {
        return Person.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Person> list(boolean enabled, boolean admin, int start, int size) {
        return Person.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Person> list(String name, int size) {
        return Person.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Person> list(boolean enabled) {
        return Person.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Person> list(boolean enabled, boolean admin) {
        return Person.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Person refresh(Long id) {
        Person person = Person.findById(id)
        return person
    }

    int count(boolean enabled) {
        return Person.countByEnabled(enabled)
    }

    Person delete(Person person, LoginManager loginManager) {
        if (!person) {
            return null
        }
        person = Person.findById(person.id)
        person.enabled = !person.enabled
        return create(person, loginManager)
    }

    Person byEmail(String person) {
        return Person.findByEmailAndEnabled(person, true)
    }

    Person byName(String name) {
        return Person.findByNameAndEnabled(name, true)
    }

    Person byId(Long id) {
        return Person.findByIdAndEnabled(id, true)
    }

    Long findMaxCode() {
        Person person = Person.withCriteria {
            order("id", "desc")
        }.find() as Person
        if (!person) {
            return 0
        }
        return person.code
    }
}
