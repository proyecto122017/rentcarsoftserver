package edu.utesa.proyecto.server.rentcarsoft.domains.security

import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.PermissionAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Permission implements APIConverter<PermissionAPI> {

    int code
    String name
    String description

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        description nullable: true
    }

    static mapping = {
        table "permission"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Permission) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    String toStringEntity() {
        return name
    }

    @Override
    PermissionAPI toAPI() {
        PermissionAPI permissionAPI = new PermissionAPI()
        permissionAPI.code = code
        permissionAPI.name = name
        permissionAPI.description = description
        return permissionAPI
    }
}
