package edu.utesa.proyecto.server.rentcarsoft.models.communication.administration

import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.OfferAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.RentalAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.ReservationAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.CoordinatesAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.GeneralInfoAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.TypesAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleBranchOfficeAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Desckop on 6/18/17.
 */
@CompileStatic
class BranchOfficeAPI {

    int code
    CoordinatesAPI coordinates
    GeneralInfoAPI generalInfo
    TypesAPI branchOfficeType
    List<OfferAPI> offerList
    List<NotificationsAPI> notificationsList
    List<VehicleBranchOfficeAPI> vehicleBrandOfficeList
    List<RentalAPI> rentalList
    List<ReservationAPI> reservationList
}
