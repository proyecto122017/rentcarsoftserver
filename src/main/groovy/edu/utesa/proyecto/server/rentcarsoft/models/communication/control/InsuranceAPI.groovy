package edu.utesa.proyecto.server.rentcarsoft.models.communication.control

import groovy.transform.CompileStatic

/**
 *
 * Created by Desckop on 6/18/17.
 */
@CompileStatic
class InsuranceAPI {

    int code
    String description
    Long coverage

}
