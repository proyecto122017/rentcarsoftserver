package edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle

import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class VehicleBranchOfficeAPI {

    int code
    String chassis
    String year
    VehicleAPI vehicle
    VehicleColorAPI vehicleColor
    VehicleGPSAPI vehicleGPS
    VehicleVersionAPI vehicleVersion
    BigDecimal price

}
