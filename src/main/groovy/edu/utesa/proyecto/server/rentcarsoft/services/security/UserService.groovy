package edu.utesa.proyecto.server.rentcarsoft.services.security

import edu.utesa.proyecto.server.rentcarsoft.domains.security.User
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("userService")
@Transactional(rollbackFor = Exception.class)
class UserService {

    private User createBase(User user, String userName) {
        if (user != null && !user.hasErrors()) {
            if (User.exists(user.id)) {
                user.lastUpdated = new Date()
                user.modifyBy = userName
                return user.merge(flush: true, failOnError: true)
            } else {
                user.createdBy = userName
                user.dateCreated = new Date()
                user = user.save(flush: true, failOnError: true)
                return user
            }
        }
        return null
    }

    User bootStrap(User user) {
        return createBase(user, Constants.ROOT)
    }

    User create(User user, LoginManager loginManager) {
        return createBase(user, loginManager.getUsername())
    }

    List<User> list(boolean enabled, int start, int size) {
        return User.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<User> list(boolean enabled, boolean admin, int start, int size) {
        return User.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<User> list(String name, int size) {
        return User.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<User> list(boolean enabled) {
        return User.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<User> list(boolean enabled, boolean admin) {
        return User.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    User refresh(Long id) {
        User user = User.findById(id)
        return user
    }

    int count(boolean enabled) {
        return User.countByEnabled(enabled)
    }

    int count(boolean enabled, boolean admin) {
        return User.countByEnabledAndAdmin(enabled, admin)
    }

    User delete(User user, LoginManager loginManager) {
        if (!user) {
            return null
        }
        user = User.findById(user.id)
        user.enabled = !user.enabled
        return create(user, loginManager)
    }

    User getRootUser() {
        return User.findByUsernameAndEnabled(Constants.ROOT_USERNAME, true)
    }

    User byUsername(String username) {
        return User.findByUsernameAndEnabled(username, true)
    }

    Boolean compareByUsername(String username) {
        User user = User.findByUsernameAndEnabled(username, true)
        if (user) {
            return true
        } else {
            return false
        }
    }

    User byName(String name) {
        return User.findByNameAndEnabled(name, true)
    }

    User byEmail(String email) {
        return User.findByEmailAndEnabled(email, true)
    }

    User byId(Long id) {
        return User.findByIdAndEnabled(id, true)
    }

    User changePassword(User user, LoginManager loginManager) {
        return create(user, loginManager)
    }

    Long findMaxCode() {
        User user = User.withCriteria {
            order("id", "desc")
        }.find() as User
        if (!user) {
            return 0
        }
        return user.code
    }


}
