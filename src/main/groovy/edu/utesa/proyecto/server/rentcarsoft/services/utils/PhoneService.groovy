package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Phone
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("phoneService")
@Transactional(rollbackFor = Exception.class)
class PhoneService {

    private Phone createBase(Phone phone, String userName) {
        if (phone != null && !phone.hasErrors()) {
            if (Phone.exists(phone.id)) {
                phone.lastUpdated = new Date()
                phone.modifyBy = userName
                return phone.merge(flush: true, failOnError: true)
            } else {
                phone.createdBy = userName
                phone.dateCreated = new Date()
                phone = phone.save(flush: true, failOnError: true)
                return phone
            }
        }
        return null
    }

    Phone bootStrap(Phone phone) {
        return createBase(phone, Constants.ROOT)
    }

    Phone create(Phone phone, LoginManager loginManager) {
        return createBase(phone, loginManager.getUsername())
    }

    List<Phone> list(boolean enabled, int start, int size) {
        return Phone.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Phone> list(boolean enabled, boolean admin, int start, int size) {
        return Phone.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Phone> list(String name, int size) {
        return Phone.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Phone> list(boolean enabled) {
        return Phone.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Phone> list(boolean enabled, boolean admin) {
        return Phone.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Phone refresh(Long id) {
        Phone phone = Phone.findById(id)
        return phone
    }

    int count(boolean enabled) {
        return Phone.countByEnabled(enabled)
    }

    Phone delete(Phone phone, LoginManager loginManager) {
        if (!phone) {
            return null
        }
        phone = Phone.findById(phone.id)
        phone.enabled = !phone.enabled
        return create(phone, loginManager)
    }

    Phone byEmail(String phone) {
        return Phone.findByEmailAndEnabled(phone, true)
    }

    Phone byId(Long id) {
        return Phone.findByIdAndEnabled(id, true)
    }

}
