package edu.utesa.proyecto.server.rentcarsoft.models.communication.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class IdentificationAPI {

    String identification
    TypesAPI identificationType

}
