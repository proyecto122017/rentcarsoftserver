package edu.utesa.proyecto.server.rentcarsoft.models.communication.administration

import groovy.transform.CompileStatic

/**
 *
 * Created by Desckop on 6/18/17.
 */
@CompileStatic
class NotificationsAPI {

    int code
    String description

}
