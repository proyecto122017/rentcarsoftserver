package edu.utesa.proyecto.server.rentcarsoft.models.interfaces;

/**
 * Created by ronald on 4/17/17.
 */
public interface APIConverter<T> {

    T toAPI();
}
