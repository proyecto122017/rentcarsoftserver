package edu.utesa.proyecto.server.rentcarsoft.domains.control

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.VehicleMaintenanceAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class VehicleMaintenance implements APIConverter<VehicleMaintenanceAPI> {

    int code
    String description
    Types vehicleMaintenanceType

    boolean original = true

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        vehicleMaintenanceType nullable: true
    }

    static mapping = {
        table "veh_maintenance"
        vehicleMaintenanceType fetch: 'join'
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((VehicleMaintenance) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    VehicleMaintenanceAPI toAPI() {
        VehicleMaintenanceAPI vehicleMaintenanceAPI = new VehicleMaintenanceAPI()
        vehicleMaintenanceAPI.code = code
        vehicleMaintenanceAPI.description = description
        if (vehicleMaintenanceType) {
            vehicleMaintenanceAPI.vehicleMaintenanceType = vehicleMaintenanceType.toAPI()
        }
        return vehicleMaintenanceAPI
    }
}
