package edu.utesa.proyecto.server.rentcarsoft.domains.location

import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.AddressAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Address implements APIConverter<AddressAPI> {

    int code
    String description
    Sector sector

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        description nullable: true
        sector nullable: true
    }

    static mapping = {
        table "address"

        sector fetch: "join"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Address) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    AddressAPI toAPI() {
        AddressAPI addressAPI = new AddressAPI()
        addressAPI.code = code
        addressAPI.description = description
        if (sector) {
            addressAPI.sector = sector.toAPI()
        }

        return addressAPI
    }
}
