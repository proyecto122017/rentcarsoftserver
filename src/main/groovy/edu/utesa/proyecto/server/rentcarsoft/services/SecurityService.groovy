package edu.utesa.proyecto.server.rentcarsoft.services

import edu.utesa.proyecto.server.rentcarsoft.domains.security.User
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.security.HashUtils
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("securityService")
@Transactional(rollbackFor = Exception.class)
class SecurityService {

    LoginManager authenticate(String username, String password) {
        password = HashUtils.encodeSHA256(password)
        LoginManager loginManager = new LoginManager()
        User user = authenticateUser(username, password)
        if (user) {
            loginManager.setUser(user)
            return loginManager
        }
        return null
    }

    private static User authenticateUser(String username, String password) {
        return User.findByUsernameAndPasswordAndEnabled(username, password, true)
    }
}
