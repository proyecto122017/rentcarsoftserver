package edu.utesa.proyecto.server.rentcarsoft.models.communication.control

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.domains.control.VehicleCondition
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.TypesAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleBranchOfficeAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Desckop on 6/18/17.
 */
@CompileStatic
class RentalAPI {

    int code
    String description
    TypesAPI rentalType
    Date pickup_date
    Date return_date
    VehicleConditionAPI vehicleCondition
    List<VehicleBranchOfficeAPI> vehicleBrandOfficeList
}
