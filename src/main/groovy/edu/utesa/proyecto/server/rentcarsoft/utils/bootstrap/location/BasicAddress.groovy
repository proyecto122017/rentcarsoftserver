package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Address
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Sector
import edu.utesa.proyecto.server.rentcarsoft.services.location.AddressService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicAddress {

    @Autowired
    private AddressService addressService

    Address address1
    Address address2
    Address address3

    BasicAddress() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicSector basicSector) {
        address1 = create(1, "Casa Ronald", basicSector.sabaneta)
        address2 = create(2, "Casa Sandy", basicSector.gurabo)
        address3 = create(3, "Casa Deli", basicSector.maimon)
    }

    private Address create(int code, String name, Sector sector) {
        Address address = addressService.byCode(code)
        if (!address) {
            address = new Address()
            address.code = code
            address.description = name
            address.sector = sector
            return addressService.bootStrap(address)
        }
        return address
    }
}
