package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBrand
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleBrandService")
@Transactional(rollbackFor = Exception.class)
class VehicleBrandService {

    private VehicleBrand createBase(VehicleBrand vehicleBrand, String userName) {
        if (vehicleBrand != null && !vehicleBrand.hasErrors()) {
            if (VehicleBrand.exists(vehicleBrand.id)) {
                vehicleBrand.lastUpdated = new Date()
                vehicleBrand.modifyBy = userName
                return vehicleBrand.merge(flush: true, failOnError: true)
            } else {
                vehicleBrand.createdBy = userName
                vehicleBrand.dateCreated = new Date()
                vehicleBrand = vehicleBrand.save(flush: true, failOnError: true)
                return vehicleBrand
            }
        }
        return null
    }

    VehicleBrand bootStrap(VehicleBrand vehicleBrand) {
        return createBase(vehicleBrand, Constants.ROOT)
    }

    VehicleBrand create(VehicleBrand vehicleBrand, LoginManager loginManager) {
        return createBase(vehicleBrand, loginManager.getUsername())
    }

    List<VehicleBrand> list(boolean enabled, int start, int size) {
        return VehicleBrand.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleBrand> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleBrand.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleBrand> list(String name, int size) {
        return VehicleBrand.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleBrand> list(boolean enabled) {
        return VehicleBrand.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleBrand> list(boolean enabled, boolean admin) {
        return VehicleBrand.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleBrand refresh(Long id) {
        VehicleBrand vehicleBrand = VehicleBrand.findById(id)
        return vehicleBrand
    }

    int count(boolean enabled) {
        return VehicleBrand.countByEnabled(enabled)
    }

    VehicleBrand delete(VehicleBrand vehicleBrand, LoginManager loginManager) {
        if (!vehicleBrand) {
            return null
        }
        vehicleBrand = VehicleBrand.findById(vehicleBrand.id)
        vehicleBrand.enabled = !vehicleBrand.enabled
        return create(vehicleBrand, loginManager)
    }

    VehicleBrand byEmail(String vehicleBrand) {
        return VehicleBrand.findByEmailAndEnabled(vehicleBrand, true)
    }

    VehicleBrand byId(Long id) {
        return VehicleBrand.findByIdAndEnabled(id, true)
    }

    VehicleBrand byCode(int id) {
        return VehicleBrand.findByCodeAndEnabled(id, true)
    }

    Long findMaxCode() {
        VehicleBrand vehicleBrand = VehicleBrand.withCriteria {
            order("id", "desc")
        }.find() as VehicleBrand
        if (!vehicleBrand) {
            return 1
        }
        return vehicleBrand.code
    }

}
