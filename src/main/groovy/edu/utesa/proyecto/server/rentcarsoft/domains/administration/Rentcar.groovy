package edu.utesa.proyecto.server.rentcarsoft.domains.administration

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.models.communication.administration.RentcarAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity
import org.grails.datastore.gorm.GormEntity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Rentcar implements APIConverter<RentcarAPI>, GormEntity<Rentcar> {

    int code
    GeneralInfo generalInfo

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [listBranchOffices: BranchOffice]

    static constraints = {
        code unique: true
        generalInfo nullable: true
    }

    static mapping = {
        table "rentcar"

        generalInfo fetch: "join"

        listBranchOffices lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Rentcar) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    RentcarAPI toAPI() {
        RentcarAPI rentcarAPI = new RentcarAPI()
        rentcarAPI.code = code
        if (generalInfo) {
            rentcarAPI.generalInfo = generalInfo.toAPI()
        }
        rentcarAPI.branchOfficeList = new ArrayList<>()
        if (listBranchOffices.size() > 0) {
            for (BranchOffice branchOffice : listBranchOffices) {
                rentcarAPI.branchOfficeList.add(branchOffice.toAPI())
            }
        }
        return rentcarAPI
    }
}
