package edu.utesa.proyecto.server.rentcarsoft.utils.security


import edu.utesa.proyecto.server.rentcarsoft.models.enums.files.CryptType
import groovy.transform.CompileStatic

import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import java.security.Key

/**
 * Class for keep save the data
 *
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
final class CryptUtils {

    /**
     * Encrypt Text data.
     *
     * @param cryptType
     * @param key
     * @param text
     * @return
     */
    static final String encrypt(CryptType cryptType, String key, String text) {
        return new String(Base64.getEncoder().encodeToString(doCrypto(Cipher.ENCRYPT_MODE, cryptType, key, text.getBytes())))
    }

    /**
     * Decrypt Text data.
     *
     * @param cryptType
     * @param key
     * @param text
     * @return
     */
    static final String decrypt(CryptType cryptType, String key, String text) {
        return new String(doCrypto(Cipher.DECRYPT_MODE, cryptType, key, Base64.getDecoder().decode(text)))
    }

    /**
     * Encrypt File data.
     *
     * @param cryptType
     * @param key
     * @param inputFile
     * @return Encrypt data on byte[]
     */
    static final byte[] encrypt(CryptType cryptType, String key, File inputFile) {
        return doCrypto(Cipher.ENCRYPT_MODE, cryptType, key, inputFile)
    }

    /**
     * Decrypt File data.
     *
     * @param cryptType
     * @param key
     * @param inputFile
     * @return Encrypt data on byte[]
     */
    static final byte[] decrypt(CryptType cryptType, String key, File inputFile) {
        return doCrypto(Cipher.DECRYPT_MODE, cryptType, key, inputFile)
    }

    /**
     * Do Process on File data.
     *
     * @param cipherMode
     * @param cryptType
     * @param key
     * @param inputFile
     * @return Encrypt data on byte[]
     */
    private static final byte[] doCrypto(int cipherMode, CryptType cryptType, String key, File inputFile) {
        try {
            Cipher cipher = prepareCipher(cipherMode, cryptType, key)
            byte[] inputBytes = new byte[(int) inputFile.length()]
            FileInputStream inputStream = new FileInputStream(inputFile)
            inputStream.read(inputBytes)
            return cipher.doFinal(inputBytes)
        } catch (Exception ex) {
            ex.printStackTrace()
            return null
        }
    }

    private static final byte[] doCrypto(int cipherMode, CryptType cryptType, String key, byte[] text) {
        try {
            Cipher cipher = prepareCipher(cipherMode, cryptType, key)
            return cipher.doFinal(text)
        } catch (Exception ex) {
            ex.printStackTrace()
            return null
        }
    }

    private static Cipher prepareCipher(int cipherMode, CryptType cryptType, String key) {
        // Prepare Key
        Key secretKey = new SecretKeySpec(key.getBytes(), cryptType.getCrypt())
        // Prepare Encrypt
        Cipher cipher = Cipher.getInstance(cryptType.getCrypt())
        cipher.init(cipherMode, secretKey)
        return cipher
    }
}
