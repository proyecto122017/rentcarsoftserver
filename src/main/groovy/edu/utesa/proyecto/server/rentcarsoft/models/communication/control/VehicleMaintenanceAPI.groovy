package edu.utesa.proyecto.server.rentcarsoft.models.communication.control

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.TypesAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class VehicleMaintenanceAPI {

    int code
    String description
    TypesAPI vehicleMaintenanceType

}
