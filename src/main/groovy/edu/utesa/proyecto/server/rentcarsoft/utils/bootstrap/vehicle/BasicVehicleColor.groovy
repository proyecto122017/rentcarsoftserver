package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleColor
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleColorService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.json.VehicleColorJson
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicVehicleColor {

    @Autowired
    private VehicleColorService vehicleColorService

    DataColor data

    BasicVehicleColor() {
        Constants.instance.autoWiredClass(this)
        data = Constants.instance.convert(Constants.instance.readJsonFile("vehicleColor"), DataColor.class)
    }

    void insert() {
        for (VehicleColorJson vehicleColorJson : data.data) {
            createVehicleColorFromExcel(vehicleColorJson)
        }
    }

    private VehicleColor create(int code, String name) {
        VehicleColor vehicleColor = vehicleColorService.byCode(code)
        if (!vehicleColor) {
            vehicleColor = new VehicleColor()
            vehicleColor.code = code
            vehicleColor.color = name
            return vehicleColorService.bootStrap(vehicleColor)
        }
        return vehicleColor
    }

    private VehicleColor createVehicleColorFromExcel(VehicleColorJson vehicleColorJson) {
        VehicleColor vehicleColor = vehicleColorService.byColor(vehicleColorJson.color)
        if (!vehicleColor) {
            vehicleColor = new VehicleColor()
            vehicleColor.code = vehicleColorService.findMaxCode() + 1 as Integer
            vehicleColor.color = vehicleColorJson.color
            return vehicleColorService.bootStrap(vehicleColor)
        }
        return vehicleColor
    }

    static class DataColor {
        List<VehicleColorJson> data
    }
}
