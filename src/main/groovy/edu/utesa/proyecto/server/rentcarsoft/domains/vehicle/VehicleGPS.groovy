package edu.utesa.proyecto.server.rentcarsoft.domains.vehicle

import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleGPSAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by Desckop on 9/6/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class VehicleGPS implements APIConverter<VehicleGPSAPI> {

    int code
    String serial
    String number
    BigDecimal balance

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        balance nullable: true
    }

    static mapping = {
        table "vehicle_gps"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((VehicleGPS) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    VehicleGPSAPI toAPI() {
        VehicleGPSAPI vehicleGPSAPI = new VehicleGPSAPI()
        vehicleGPSAPI.code = code
        vehicleGPSAPI.serial = serial
        vehicleGPSAPI.number = number
        vehicleGPSAPI.balance = balance
        return vehicleGPSAPI
    }
}
