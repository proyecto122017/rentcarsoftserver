package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.security

import edu.utesa.proyecto.server.rentcarsoft.domains.security.Occupation
import edu.utesa.proyecto.server.rentcarsoft.services.security.OccupationService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicOccupation {

    @Autowired
    private OccupationService occupationService

    Occupation doctor
    Occupation abogado
    Occupation desarrollador
    Occupation profesor
    Occupation licensiado
    Occupation secretaria
    Occupation comerciante

    BasicOccupation() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        doctor = create(1, "Doctor")
        abogado = create(2, "abogado")
        desarrollador = create(3, "desarrollador")
        profesor = create(4, "profesor")
        licensiado = create(5, "licensiado")
        secretaria = create(6, "secretaria")
        comerciante = create(7, "comerciante")
    }

    private Occupation create(int code, String name) {
        Occupation occupation = occupationService.byCode(code)
        if (!occupation) {
            occupation = new Occupation()
            occupation.code = code
            occupation.description = name
            return occupationService.bootStrap(occupation)
        }
        return occupation
    }
}
