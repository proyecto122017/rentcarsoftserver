package edu.utesa.proyecto.server.rentcarsoft.services.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Province
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("provinceService")
@Transactional(rollbackFor = Exception.class)
class ProvinceService {

    private Province createBase(Province province, String userName) {
        if (province != null && !province.hasErrors()) {
            if (Province.exists(province.id)) {
                province.lastUpdated = new Date()
                province.modifyBy = userName
                return province.merge(flush: true, failOnError: true)
            } else {
                province.createdBy = userName
                province.dateCreated = new Date()
                province = province.save(flush: true, failOnError: true)
                return province
            }
        }
        return null
    }

    Province bootStrap(Province province) {
        return createBase(province, Constants.ROOT)
    }

    Province create(Province province, LoginManager loginManager) {
        return createBase(province, loginManager.getUsername())
    }

    List<Province> list(boolean enabled, int start, int size) {
        return Province.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Province> list(boolean enabled, boolean admin, int start, int size) {
        return Province.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Province> list(String name, int size) {
        return Province.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Province> list(boolean enabled) {
        return Province.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Province> list(boolean enabled, boolean admin) {
        return Province.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Province refresh(Long id) {
        Province province = Province.findById(id)
        return province
    }

    int count(boolean enabled) {
        return Province.countByEnabled(enabled)
    }

    Province delete(Province province, LoginManager loginManager) {
        if (!province) {
            return null
        }
        province = Province.findById(province.id)
        province.enabled = !province.enabled
        return create(province, loginManager)
    }

    Province byId(Long id) {
        return Province.findByIdAndEnabled(id, true)
    }

    Province byCode(int id) {
        return Province.findByCodeAndEnabled(id, true)
    }

    Province changePassword(Province province, LoginManager loginManager) {
        return create(province, loginManager)
    }

    Long findMaxCode() {
        Province province = Province.withCriteria {
            order("id", "desc")
        }.find() as Province
        if (!province) {
            return 0
        }
        return province.code
    }

}
