package edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleModel
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class VehicleBrandAPI {

    int code
    String description
    VehicleModelAPI vehicleModel

}
