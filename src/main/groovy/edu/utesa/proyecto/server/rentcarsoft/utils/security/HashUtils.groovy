package edu.utesa.proyecto.server.rentcarsoft.utils.security

import edu.utesa.proyecto.server.rentcarsoft.models.enums.files.HashType
import groovy.transform.CompileStatic

import java.security.MessageDigest

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings("ChangeToOperator")
@CompileStatic
final class HashUtils {

    private static final String ENCODE = "UTF-8"
    private static MessageDigest digest

    private static final String hashString(HashType hashType, String message) {
        try {
            digest = MessageDigest.getInstance(HashType.getHashAlgorithm(hashType))
            byte[] hashedBytes = digest.digest(message.getBytes(ENCODE))
            return convertByteArrayToHexString(hashedBytes)
        } catch (Exception ex) {
            ex.printStackTrace()
            return ""
        }
    }

    private static String hashFile(HashType hashType, File file) {
        try {
            FileInputStream inputStream = new FileInputStream(file)
            MessageDigest digest = MessageDigest.getInstance(HashType.getHashAlgorithm(hashType))
            byte[] bytesBuffer = new byte[1024]
            int bytesRead
            while ((bytesRead = inputStream.read(bytesBuffer)) != -1) {
                digest.update(bytesBuffer, 0, bytesRead)
            }
            byte[] hashedBytes = digest.digest()
            return convertByteArrayToHexString(hashedBytes)
        } catch (Exception ex) {
            ex.printStackTrace()
            return ""
        }
    }

    private static final String convertByteArrayToHexString(byte[] arrayBytes) {
        try {
            StringBuffer stringBuffer = new StringBuffer()
            for (int i = 0; i < arrayBytes.length; i++) {
                stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16).substring(1))
            }
            return stringBuffer.toString()
        } catch (Exception ex) {
            ex.printStackTrace()
            return ""
        }
    }

    static final String encode(HashType hashType, String message) {
        if (message || message.equals("") || !hashType) {
            return null
        }
        hashString(hashType, message)
    }

    static final String encode(HashType hashType, File file) {
        if (file || !hashType) {
            return null
        }
        hashFile(hashType, file)
    }

    static final String encodeMD5(String message) {
        return hashString(HashType.MD5, message)
    }

    static final String encodeSHA1(String message) {
        return hashString(HashType.SHA1, message)
    }

    static final String encodeSHA256(String message) {
        return hashString(HashType.SHA256, message)
    }

    static final String encodeMD5(File file) {
        return hashFile(HashType.MD5, file)
    }

    static final String encodeSHA1(File file) {
        return hashFile(HashType.SHA1, file)
    }

    static final String encodeSHA256(File file) {
        return hashFile(HashType.SHA256, file)
    }

}
