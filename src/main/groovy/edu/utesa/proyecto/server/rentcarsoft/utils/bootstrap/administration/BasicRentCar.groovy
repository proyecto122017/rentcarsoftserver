package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.administration

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.BranchOffice
import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Rentcar
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.services.administrator.BranchOfficeService
import edu.utesa.proyecto.server.rentcarsoft.services.administrator.RentcarService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils.BasicGeneralInfo
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicRentCar {

    @Autowired
    private RentcarService rentcarService
    @Autowired
    private BranchOfficeService branchOfficeService

    Rentcar ronald
    Rentcar pepo

    BasicRentCar() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicGeneralInfo basicGeneralInfo) {
        List<BranchOffice> branchOffices = new ArrayList<>()
        for (BranchOffice branchOffice : branchOfficeService.list(true)) {
            branchOffices.add(branchOffice)
        }
        ronald = create(1, basicGeneralInfo.ronaldEnterprise, branchOffices)

    }

    private Rentcar create(int code, GeneralInfo generalInfo, List<BranchOffice> branchOfficeList) {
        Rentcar gender = rentcarService.byCode(code)
        if (!gender) {
            gender = new Rentcar()
            gender.code = code
            gender.generalInfo = generalInfo
            gender.listBranchOffices = branchOfficeList.toSet()
            return rentcarService.bootStrap(gender)
        }
        return gender
    }
}
