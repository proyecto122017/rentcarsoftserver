package edu.utesa.proyecto.server.rentcarsoft.models

import edu.utesa.proyecto.server.rentcarsoft.domains.security.User
import edu.utesa.proyecto.server.rentcarsoft.services.security.UserService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 1/16/2015.
 */
@CompileStatic
class LoginManager {
    @Autowired
    private UserService userService

    private User user

    LoginManager() {
        user = null
        Constants.instance.autoWiredClass(this)
    }

    User getUser() {
        return user
    }

    void setUser(User user) {
        this.user = user
    }

    Long getID() {
        return user.id
    }

    Long getIdUser() {
        return user.code
    }

    boolean isUser() {
        if (user) {
            return true
        }
        return false
    }

    String getPassword() {
        return user.password
    }

    String getUsername() {
        return user.username
    }

    boolean getAdmin() {
        return user.admin
    }

    Date getRegisterDate() {
        return user.registerDate
    }

    Date getIngressDate() {
        return user.ingressDate
    }

    void refresh(User user) {
        this.user = user
    }
}
