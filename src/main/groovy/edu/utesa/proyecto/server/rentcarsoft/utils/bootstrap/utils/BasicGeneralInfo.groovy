package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Address
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Identification
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Phone
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.services.utils.GeneralInfoService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location.BasicAddress
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicGeneralInfo {

    @Autowired
    private GeneralInfoService generalInfoService

    GeneralInfo ronaldEnterprise
    GeneralInfo sandyEnterprise

    BasicGeneralInfo() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicTypes basicTypes, BasicAddress basicAddress) {
        List<Phone> phoneRonaldArrayList = new ArrayList<>()
        phoneRonaldArrayList.add(new Phone(number: "809-344-2323"))
        List<Phone> phoneSandyArrayList = new ArrayList<>()
        phoneRonaldArrayList.add(new Phone(number: "809-344-2323"))
        List<Address> addressRonaldArrayList = new ArrayList<>()
        addressRonaldArrayList.add(basicAddress.address1)
        List<Address> addressSandyArrayList = new ArrayList<>()
        addressSandyArrayList.add(basicAddress.address2)
        ronaldEnterprise = create(1, "Ronald", basicTypes.typesUsuarioAdmin, new Identification(identification: "3333333", identificationType: basicTypes.typesIdentificationCedula),
                phoneRonaldArrayList, addressRonaldArrayList)
        sandyEnterprise = create(2, "Sandy", basicTypes.typesUsuarioNormal, new Identification(identification: "86765645545", identificationType: basicTypes.typesIdentificationCedula),
                phoneSandyArrayList, addressSandyArrayList)
    }

    private GeneralInfo create(int code, String name, Types types, Identification identification, List<Phone> phoneList, List<Address> addressList) {
        GeneralInfo generalInfo = generalInfoService.byName(name)
        if (generalInfo) {
            return generalInfo
        }
        generalInfo = new GeneralInfo()
        generalInfo.code = code
        generalInfo.name = name
        generalInfo.type = types
        generalInfo.identification = identification
        generalInfo.listPhones = phoneList.toSet()
        generalInfo.listAddress = addressList.toSet()
        return generalInfoService.bootStrap(generalInfo)
    }
}
