package edu.utesa.proyecto.server.rentcarsoft.domains.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Rental
import edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting.InvoiceDetailRentAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity


/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class InvoiceDetailRent implements APIConverter<InvoiceDetailRentAPI> {

    int code
    BigInteger invoiceNumber
    BigDecimal discount
    BigDecimal price
    BigDecimal saleTotal
    Rental rental

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true

    }

    static mapping = {
        table "invoices_rent_details"

        rental fetch: "join"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((InvoiceDetailRent) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    InvoiceDetailRentAPI toAPI() {
        InvoiceDetailRentAPI invoiceDetailRentAPI = new InvoiceDetailRentAPI()
        invoiceDetailRentAPI.code = code
        invoiceDetailRentAPI.invoiceNumber = invoiceNumber
        invoiceDetailRentAPI.discount = discount
        invoiceDetailRentAPI.price = price
        invoiceDetailRentAPI.saleTotal = saleTotal
        invoiceDetailRentAPI.rental = rental.toAPI()
        return invoiceDetailRentAPI
    }
}
