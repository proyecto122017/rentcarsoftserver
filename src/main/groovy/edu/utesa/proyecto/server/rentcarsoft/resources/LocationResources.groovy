package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.domains.location.*
import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.*
import edu.utesa.proyecto.server.rentcarsoft.services.location.*
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/location")
@CompileStatic
class LocationResources {

    static final SessionManager sessionManager = new SessionManager()

    @Autowired
    private AddressService addressService
    @Autowired
    private CoordinatesService coordinatesService
    @Autowired
    private CountryService countryService
    @Autowired
    private MunicipalityService municipalityService
    @Autowired
    private ProvinceService provinceService
    @Autowired
    private SectorService sectorService

    @GET
    static String isUP() {
        return "OK"
    }

    @POST
    @Path('/addresses')
    String listAddresses(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<AddressAPI> addressAPIS = new ArrayList<>()
        for (Address address : addressService.list(true)) {
            addressAPIS.add(address.toAPI())
        }
        return Constants.instance.stringify(addressAPIS)
    }

    @POST
    @Path('/create/address')
    String createAddress(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        AddressAPI addressAPI = Constants.instance.convert(request, AddressAPI.class)
        if (!addressAPI) {
            // No parsea el objeto que enviaron
        }
        Address address = new Address()
        address.code = addressService.findMaxCode() + 1 as Integer
        address.description = addressAPI.description ?: ""
        address.sector = addressAPI.sector as Sector
        addressService.bootStrap(address)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/coordinates')
    String listCoordinates(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<CoordinatesAPI> coordinatesAPIS = new ArrayList<>()
        for (Coordinates coordinates : coordinatesService.list(true)) {
            coordinatesAPIS.add(coordinates.toAPI())
        }
        return Constants.instance.stringify(coordinatesAPIS)
    }

    @POST
    @Path('/create/coordinate')
    String createCoordinates(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        CoordinatesAPI coordinatesAPI = Constants.instance.convert(request, CoordinatesAPI.class)
        if (!coordinatesAPI) {
            // No parsea el objeto que enviaron
        }
        Coordinates coordinates = new Coordinates()
        coordinates.code = coordinatesService.findMaxCode() + 1 as Integer
        coordinates.longitude = coordinatesAPI.longitude
        coordinates.latitude = coordinatesAPI.latitude
        coordinatesService.bootStrap(coordinates)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/countries')
    String listCountries(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<CountryAPI> countryAPIS = new ArrayList<>()
        for (Country country : countryService.list(true)) {
            countryAPIS.add(country.toAPI())
        }
        return Constants.instance.stringify(countryAPIS)
    }

    @POST
    @Path('/create/country')
    String createCountry(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        CountryAPI countryAPI = Constants.instance.convert(request, CountryAPI.class)
        if (!countryAPI) {
            // No parsea el objeto que enviaron
        }
        Country country = new Country()
        country.code = countryService.findMaxCode() + 1 as Integer
        country.description = countryAPI.description ?: ""
        country.province = countryAPI.province as Province
        countryService.bootStrap(country)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/municipalities')
    String listMunicipalities(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<MunicipalityAPI> municipalityAPIS = new ArrayList<>()
        for (Municipality municipality : municipalityService.list(true)) {
            municipalityAPIS.add(municipality.toAPI())
        }
        println "TEST:" + Constants.instance.stringify(municipalityAPIS)
        return Constants.instance.stringify(municipalityAPIS)
    }

    @POST
    @Path('/create/municipality')
    String createMunicipality(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        MunicipalityAPI municipalityAPI = Constants.instance.convert(request, MunicipalityAPI.class)
        if (!municipalityAPI) {
            // No parsea el objeto que enviaron
        }
        Municipality municipality = new Municipality()
        municipality.code = municipalityService.findMaxCode() + 1 as Integer
        municipality.description = municipalityAPI.description ?: ""
        municipality.sector = municipalityAPI.sector as Sector
        municipalityService.bootStrap(municipality)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/provinces')
    String listProvinces(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<ProvinceAPI> provinceAPIS = new ArrayList<>()
        for (Province province : provinceService.list(true)) {
            provinceAPIS.add(province.toAPI())
        }
        return Constants.instance.stringify(provinceAPIS)
    }

    @POST
    @Path('/create/province')
    String createProvince(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        ProvinceAPI provinceAPI = Constants.instance.convert(request, ProvinceAPI.class)
        if (!provinceAPI) {
            // No parsea el objeto que enviaron
        }
        Province province = new Province()
        province.code = provinceService.findMaxCode() + 1 as Integer
        province.description = provinceAPI.description ?: ""
        province.municipality = provinceAPI.municipality as Municipality
        provinceService.bootStrap(province)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/sectors')
    String listSectors(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<SectorAPI> sectorAPIS = new ArrayList<>()
        for (Sector sector : sectorService.list(true)) {
            sectorAPIS.add(sector.toAPI())
        }
        return Constants.instance.stringify(sectorAPIS)
    }

    @POST
    @Path('/create/sector')
    String createSector(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        SectorAPI sectorAPI = Constants.instance.convert(request, SectorAPI.class)
        if (!sectorAPI) {
            // No parsea el objeto que enviaron
        }
        Sector sector = new Sector()
        sector.code = sectorService.findMaxCode() + 1 as Integer
        sector.description = sectorAPI.description ?: ""
        sectorService.bootStrap(sector)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }
}
