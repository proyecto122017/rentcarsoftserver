package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.EDescription
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Entities
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.services.utils.TypesService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicTypes {

    @Autowired
    private TypesService typesService

    Types typesOficinaPrincipal
    Types typesOficinaSucursal
    Types typesFacturaContado
    Types typesFacturaEfectivo
    Types typesPagoEfectivo
    Types typesPagoTarjeta
    Types typesRentaNormal
    Types typesRentaLeasing
    Types typesMantenimientoNormal
    Types typesMantenimientoCorrect
    Types typesUsuarioAdmin
    Types typesUsuarioNormal
    Types typesUsuarioVIP
    Types typesVehiculoCamion
    Types typesVehiculoCamioneta
    Types typesVehiculojeepeta
    Types typesVehiculojeep
    Types typesVehiculocarro
    Types typesVehiculomotor
    Types typesIdentificationCedula
    Types typesIdentificationPassport

    BasicTypes() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicEntities basicEntities, BasicEDescription basicEDescription) {
        typesOficinaPrincipal = create(1, basicEntities.oficina, basicEDescription.principal)
        typesOficinaSucursal = create(2, basicEntities.oficina, basicEDescription.sucursal)
        typesFacturaContado = create(3, basicEntities.factura, basicEDescription.contado)
        typesFacturaEfectivo = create(4, basicEntities.factura, basicEDescription.efectivo)
        typesUsuarioVIP = create(5, basicEntities.usuario, basicEDescription.vip)
        typesUsuarioNormal = create(6, basicEntities.usuario, basicEDescription.normal)
        typesUsuarioAdmin = create(7, basicEntities.usuario, basicEDescription.admin)
        typesRentaNormal = create(8, basicEntities.renta, basicEDescription.normal)
        typesRentaLeasing = create(9, basicEntities.renta, basicEDescription.leasing)
        typesPagoEfectivo = create(10, basicEntities.pago, basicEDescription.efectivo)
        typesPagoTarjeta = create(11, basicEntities.pago, basicEDescription.tarjeta)
        typesVehiculoCamion = create(12, basicEntities.vehiculo, basicEDescription.camion)
        typesVehiculoCamioneta = create(13, basicEntities.vehiculo, basicEDescription.camioneta)
        typesVehiculocarro = create(14, basicEntities.vehiculo, basicEDescription.carro)
        typesVehiculojeep = create(15, basicEntities.vehiculo, basicEDescription.jeep)
        typesVehiculojeepeta = create(16, basicEntities.vehiculo, basicEDescription.jeepeta)
        typesVehiculomotor = create(17, basicEntities.vehiculo, basicEDescription.motor)
        typesMantenimientoNormal = create(18, basicEntities.mantenimiento, basicEDescription.correctivo)
        typesMantenimientoCorrect = create(19, basicEntities.mantenimiento, basicEDescription.normal)
        typesIdentificationCedula = create(20, basicEntities.identification, basicEDescription.cedula)
        typesIdentificationPassport = create(21, basicEntities.identification, basicEDescription.passport)
    }

    private Types create(Integer code, Entities entities, EDescription eDescription) {
        Types types = typesService.byCode(code)
        if (types) {
            return types
        }
        types = new Types()
        types.code = code
        types.entities = entities
        types.eDescription = eDescription
        return typesService.bootStrap(types)
    }
}
