package edu.utesa.proyecto.server.rentcarsoft.models.communication.security

import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class GenderAPI {

    int code
    String description

}
