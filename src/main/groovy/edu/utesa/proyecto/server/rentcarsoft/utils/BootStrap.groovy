package edu.utesa.proyecto.server.rentcarsoft.utils

import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.BasicUsers
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.administration.BasicBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.administration.BasicEnterprise
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.administration.BasicRentCar
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location.*
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.security.BasicGender
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.security.BasicOccupation
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils.*
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle.*

/**
 *  Created by ronald on 7/4/17.
 */
class BootStrap {

    private BasicAddress basicAddress = new BasicAddress()
    private BasicCountry basicCountry = new BasicCountry()
    private BasicMunicipality basicMunicipality = new BasicMunicipality()
    private BasicProvince basicProvince = new BasicProvince()
    private BasicSector basicSector = new BasicSector()
    private BasicGender basicGender = new BasicGender()
    private BasicOccupation basicOccupation = new BasicOccupation()
    private BasicEDescription basicEDescription = new BasicEDescription()
    private BasicEntities basicEntities = new BasicEntities()
    private BasicTypes basicTypes = new BasicTypes()
    private BasicVehicle basicVehicle = new BasicVehicle()
    private BasicVehicleBrand basicVehicleBrand = new BasicVehicleBrand()
    private BasicVehicleColor basicVehicleColor = new BasicVehicleColor()
    private BasicVehicleFeactures basicVehicleFeactures = new BasicVehicleFeactures()
    private BasicVehicleModel basicVehicleModel = new BasicVehicleModel()
    private BasicVehicleVersion basicVehicleVersion = new BasicVehicleVersion()
    private BasicCoordinates basicCoordinates = new BasicCoordinates()
    private BasicVehicleBranchOffice basicVehicleBranchOffice = new BasicVehicleBranchOffice()
    private BasicGeneralInfo basicGeneralInfo = new BasicGeneralInfo()
    private BasicPerson basicPerson = new BasicPerson()
    private BasicBranchOffice basicBranchOffice = new BasicBranchOffice()
    private BasicRentCar basicRentCar = new BasicRentCar()
    private BasicEnterprise basicEnterprise = new BasicEnterprise()
    private BasicUsers basicUsers = new BasicUsers()

    BootStrap() {
        Constants.instance.autoWiredClass(this)
    }

    void init() {

        basicSector.insert()
        basicMunicipality.insert(basicSector)
        basicProvince.insert(basicMunicipality)
        basicCountry.insert(basicProvince)
        basicAddress.insert(basicSector)
        basicGender.insert()
        basicOccupation.insert()
        basicEDescription.insert()
        basicEntities.insert()
        basicCoordinates.insert()
        basicTypes.insert(basicEntities, basicEDescription)
        basicVehicleColor.insert()
        basicVehicleFeactures.insert()
        basicVehicleModel.insert()
        basicVehicleBrand.insert()
        basicVehicleVersion.insert()
        basicVehicle.insert(basicTypes, basicVehicleFeactures)
        basicPerson.insert(basicTypes, basicGender, basicOccupation)
        basicGeneralInfo.insert(basicTypes, basicAddress)
        basicVehicleBranchOffice.insert(basicVehicle, basicVehicleVersion)
        basicBranchOffice.insert(basicGeneralInfo, basicCoordinates, basicTypes, basicVehicleBranchOffice)
        basicRentCar.insert(basicGeneralInfo)
        basicEnterprise.insert(basicGeneralInfo)
        basicUsers.insert()

        //hola soy german
    }
}
