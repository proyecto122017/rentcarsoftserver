package edu.utesa.proyecto.server.rentcarsoft.domains.utils

import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.TypesAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Types implements APIConverter<TypesAPI> {

    int code
    Entities entities
    EDescription eDescription

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        entities nullable: true
        eDescription nullable: true
    }

    static mapping = {
        table "types_relation"

        entities fetch: "join"
        eDescription fetch: "join"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Types) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    TypesAPI toAPI() {
        TypesAPI typesAPI = new TypesAPI()
        typesAPI.code = code
        if (entities) {
            typesAPI.entities = entities.toAPI()
        }
        if (eDescription) {
            typesAPI.eDescription = eDescription.toAPI()
        }
        return typesAPI
    }
}
