package edu.utesa.proyecto.server.rentcarsoft.utils.json

import groovy.transform.CompileStatic

/**
 *  Created by ronald on 6/12/17.
 */
@CompileStatic
class VehicleModelBrandJson {

    int id_marca
    int code
    String description

}
