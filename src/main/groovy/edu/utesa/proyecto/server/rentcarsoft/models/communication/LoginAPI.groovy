package edu.utesa.proyecto.server.rentcarsoft.models.communication

import groovy.transform.CompileStatic

@CompileStatic
class LoginAPI {

    String username
    String password
}
