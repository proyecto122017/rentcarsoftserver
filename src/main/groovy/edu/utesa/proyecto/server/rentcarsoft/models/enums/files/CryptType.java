package edu.utesa.proyecto.server.rentcarsoft.models.enums.files;

/**
 * Dice el modo de encriptaicón.
 * <p>
 * Created by ronald on 7/4/17.
 */
public enum CryptType {

    AES;

    public final String getCrypt() {
        switch (valueOf(name())) {
            case AES:
                return "AES";
        }
        return "";
    }
}
