package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Email
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("emailService")
@Transactional(rollbackFor = Exception.class)
class EmailService {

    private Email createBase(Email email, String userName) {
        if (email != null && !email.hasErrors()) {
            if (Email.exists(email.id)) {
                email.lastUpdated = new Date()
                email.modifyBy = userName
                return email.merge(flush: true, failOnError: true)
            } else {
                email.createdBy = userName
                email.dateCreated = new Date()
                email = email.save(flush: true, failOnError: true)
                return email
            }
        }
        return null
    }

    Email bootStrap(Email email) {
        return createBase(email, Constants.ROOT)
    }

    Email create(Email email, LoginManager loginManager) {
        return createBase(email, loginManager.getUsername())
    }

    List<Email> list(boolean enabled, int start, int size) {
        return Email.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Email> list(boolean enabled, boolean admin, int start, int size) {
        return Email.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Email> list(String name, int size) {
        return Email.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Email> list(boolean enabled) {
        return Email.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Email> list(boolean enabled, boolean admin) {
        return Email.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Email refresh(Long id) {
        Email email = Email.findById(id)
        return email
    }

    int count(boolean enabled) {
        return Email.countByEnabled(enabled)
    }

    Email delete(Email email, LoginManager loginManager) {
        if (!email) {
            return null
        }
        email = Email.findById(email.id)
        email.enabled = !email.enabled
        return create(email, loginManager)
    }

    Email byEmail(String email) {
        return Email.findByEmailAndEnabled(email, true)
    }

    Email byId(Long id) {
        return Email.findByIdAndEnabled(id, true)
    }

}
