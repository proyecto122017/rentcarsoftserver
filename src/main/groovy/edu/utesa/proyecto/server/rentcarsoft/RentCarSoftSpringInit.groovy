package edu.utesa.proyecto.server.rentcarsoft

import edu.utesa.proyecto.server.rentcarsoft.utils.BootStrap
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 *
 * Created by ronald on 5/31/17.
 */
@CompileStatic
//@EnableEurekaClient
@SpringBootApplication
class RentCarSoftSpringInit {
    private static BootStrap bootStrap

    static void main(String[] args) throws Exception {
        startSpring(args)
    }

    static void startSpring(String[] args) {
        Constants.instance.setApplicationContext(SpringApplication.run(RentCarSoftSpringInit.class, args))
        Constants.instance.autoWiredClass(this)
        bootStrap = new BootStrap()
        bootStrap.init()
    }

    static void stop() {
    }
}
