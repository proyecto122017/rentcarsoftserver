package edu.utesa.proyecto.server.rentcarsoft.services.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Country
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("countryService")
@Transactional(rollbackFor = Exception.class)
class CountryService {

    private Country createBase(Country country, String userName) {
        if (country != null && !country.hasErrors()) {
            if (Country.exists(country.id)) {
                country.lastUpdated = new Date()
                country.modifyBy = userName
                return country.merge(flush: true, failOnError: true)
            } else {
                country.createdBy = userName
                country.dateCreated = new Date()
                country = country.save(flush: true, failOnError: true)
                return country
            }
        }
        return null
    }

    Country bootStrap(Country country) {
        return createBase(country, Constants.ROOT)
    }

    Country create(Country country, LoginManager loginManager) {
        return createBase(country, loginManager.getUsername())
    }

    List<Country> list(boolean enabled, int start, int size) {
        return Country.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Country> list(boolean enabled, boolean admin, int start, int size) {
        return Country.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Country> list(String name, int size) {
        return Country.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Country> list(boolean enabled) {
        return Country.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Country> list(boolean enabled, boolean admin) {
        return Country.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Country refresh(Long id) {
        Country country = Country.findById(id)
        return country
    }

    int count(boolean enabled) {
        return Country.countByEnabled(enabled)
    }

    Country delete(Country country, LoginManager loginManager) {
        if (!country) {
            return null
        }
        country = Country.findById(country.id)
        country.enabled = !country.enabled
        return create(country, loginManager)
    }

    Country byId(Long id) {
        return Country.findByIdAndEnabled(id, true)
    }

    Country byCode(int id) {
        return Country.findByCodeAndEnabled(id, true)
    }

    Country changePassword(Country country, LoginManager loginManager) {
        return create(country, loginManager)
    }

    Long findMaxCode() {
        Country country = Country.withCriteria {
            order("id", "desc")
        }.find() as Country
        if (!country) {
            return 0
        }
        return country.code
    }

}
