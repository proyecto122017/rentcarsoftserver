package edu.utesa.proyecto.server.rentcarsoft.domains.utils

import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.IdentificationAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Identification implements APIConverter<IdentificationAPI> {

    String identification
    Types identificationType

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
    }

    static mapping = {
        table "identification"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Identification) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return identification
    }

    @Override
    IdentificationAPI toAPI() {
        IdentificationAPI identificationAPI = new IdentificationAPI()
        identificationAPI.identification = identification
        identificationAPI.identificationType = identificationType.toAPI()
        return identificationAPI
    }
}
