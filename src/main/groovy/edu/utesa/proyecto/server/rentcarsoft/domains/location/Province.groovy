package edu.utesa.proyecto.server.rentcarsoft.domains.location

import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.ProvinceAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by Desckop on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Province implements APIConverter<ProvinceAPI> {

    int code
    String description
    Municipality municipality

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        description nullable: true
        municipality nullable: true
    }

    static mapping = {
        table "province"
        municipality fetch: "join"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Province) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    ProvinceAPI toAPI() {
        ProvinceAPI provinceAPI = new ProvinceAPI()
        provinceAPI.code = code
        provinceAPI.description = description
        if (municipality) {
            provinceAPI.municipality = municipality.toAPI()
        }
        return provinceAPI

    }
}