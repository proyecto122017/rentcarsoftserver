package edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Rental
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.RentalAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by ronald on 4/17/17.
 */
@CompileStatic
class InvoiceDetailRentAPI {
    int code
    BigInteger invoiceNumber
    BigDecimal discount
    BigDecimal price
    BigDecimal saleTotal
    RentalAPI rental

}
