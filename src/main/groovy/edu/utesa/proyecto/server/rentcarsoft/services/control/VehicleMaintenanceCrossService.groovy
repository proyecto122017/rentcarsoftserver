package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.VehicleMaintenanceCross
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleMaintenanceCrossService")
@Transactional(rollbackFor = Exception.class)
class VehicleMaintenanceCrossService {

    private VehicleMaintenanceCross createBase(VehicleMaintenanceCross vehicleMaintenanceCross, String userName) {
        if (vehicleMaintenanceCross != null && !vehicleMaintenanceCross.hasErrors()) {
            if (VehicleMaintenanceCross.exists(vehicleMaintenanceCross.id)) {
                vehicleMaintenanceCross.lastUpdated = new Date()
                vehicleMaintenanceCross.modifyBy = userName
                return vehicleMaintenanceCross.merge(flush: true, failOnError: true)
            } else {
                vehicleMaintenanceCross.createdBy = userName
                vehicleMaintenanceCross.dateCreated = new Date()
                vehicleMaintenanceCross = vehicleMaintenanceCross.save(flush: true, failOnError: true)
                return vehicleMaintenanceCross
            }
        }
        return null
    }

    VehicleMaintenanceCross bootStrap(VehicleMaintenanceCross vehicleMaintenanceCross) {
        return createBase(vehicleMaintenanceCross, Constants.ROOT)
    }

    VehicleMaintenanceCross create(VehicleMaintenanceCross vehicleMaintenanceCross, LoginManager loginManager) {
        return createBase(vehicleMaintenanceCross, loginManager.getUsername())
    }

    List<VehicleMaintenanceCross> list(boolean enabled, int start, int size) {
        return VehicleMaintenanceCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleMaintenanceCross> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleMaintenanceCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleMaintenanceCross> list(String name, int size) {
        return VehicleMaintenanceCross.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleMaintenanceCross> list(boolean enabled) {
        return VehicleMaintenanceCross.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleMaintenanceCross> list(boolean enabled, boolean admin) {
        return VehicleMaintenanceCross.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleMaintenanceCross refresh(Long id) {
        VehicleMaintenanceCross vehicleMaintenanceCross = VehicleMaintenanceCross.findById(id)
        return vehicleMaintenanceCross
    }

    int count(boolean enabled) {
        return VehicleMaintenanceCross.countByEnabled(enabled)
    }

    VehicleMaintenanceCross delete(VehicleMaintenanceCross vehicleMaintenanceCross, LoginManager loginManager) {
        if (!vehicleMaintenanceCross) {
            return null
        }
        vehicleMaintenanceCross = VehicleMaintenanceCross.findById(vehicleMaintenanceCross.id)
        vehicleMaintenanceCross.enabled = !vehicleMaintenanceCross.enabled
        return create(vehicleMaintenanceCross, loginManager)
    }

    VehicleMaintenanceCross byId(Long id) {
        return VehicleMaintenanceCross.findByIdAndEnabled(id, true)
    }

    VehicleMaintenanceCross changePassword(VehicleMaintenanceCross vehicleMaintenanceCross, LoginManager loginManager) {
        return create(vehicleMaintenanceCross, loginManager)
    }

    Long findMaxCode() {
        VehicleMaintenanceCross vehicleMaintenanceCross = VehicleMaintenanceCross.withCriteria {
            order("id", "desc")
        }.find() as VehicleMaintenanceCross
        if (!vehicleMaintenanceCross) {
            return 0
        }
        return vehicleMaintenanceCross.code
    }

}
