package edu.utesa.proyecto.server.rentcarsoft.models.communication.security

import edu.utesa.proyecto.server.rentcarsoft.domains.security.Permission
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Person
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.PersonAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by ronald on 4/17/17.
 */
@CompileStatic
class UserAPI {

    Integer code
    String username
    String password // Esto es sha256
    String name
    PersonAPI person
    Date registerDate
    Date ingressDate
    List<PermissionAPI> permissionList

}
