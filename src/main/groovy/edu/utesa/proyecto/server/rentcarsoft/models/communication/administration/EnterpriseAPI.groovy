package edu.utesa.proyecto.server.rentcarsoft.models.communication.administration

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Rentcar
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.GeneralInfoAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Desckop on 6/18/17.
 */
@CompileStatic
class EnterpriseAPI {

    int code
    GeneralInfoAPI generalInfo
    List<RentcarAPI> rentcarList
}
