package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.ReservationCross
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("reservationCrossService")
@Transactional(rollbackFor = Exception.class)
class ReservationCrossService {

    private ReservationCross createBase(ReservationCross reservationCross, String userName) {
        if (reservationCross != null && !reservationCross.hasErrors()) {
            if (ReservationCross.exists(reservationCross.id)) {
                reservationCross.lastUpdated = new Date()
                reservationCross.modifyBy = userName
                return reservationCross.merge(flush: true, failOnError: true)
            } else {
                reservationCross.createdBy = userName
                reservationCross.dateCreated = new Date()
                reservationCross = reservationCross.save(flush: true, failOnError: true)
                return reservationCross
            }
        }
        return null
    }

    ReservationCross bootStrap(ReservationCross reservationCross) {
        return createBase(reservationCross, Constants.ROOT)
    }

    ReservationCross create(ReservationCross reservationCross, LoginManager loginManager) {
        return createBase(reservationCross, loginManager.getUsername())
    }

    List<ReservationCross> list(boolean enabled, int start, int size) {
        return ReservationCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<ReservationCross> list(boolean enabled, boolean admin, int start, int size) {
        return ReservationCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<ReservationCross> list(String name, int size) {
        return ReservationCross.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<ReservationCross> list(boolean enabled) {
        return ReservationCross.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<ReservationCross> list(boolean enabled, boolean admin) {
        return ReservationCross.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    ReservationCross refresh(Long id) {
        ReservationCross reservationCross = ReservationCross.findById(id)
        return reservationCross
    }

    int count(boolean enabled) {
        return ReservationCross.countByEnabled(enabled)
    }

    ReservationCross delete(ReservationCross reservationCross, LoginManager loginManager) {
        if (!reservationCross) {
            return null
        }
        reservationCross = ReservationCross.findById(reservationCross.id)
        reservationCross.enabled = !reservationCross.enabled
        return create(reservationCross, loginManager)
    }

    ReservationCross byId(Long id) {
        return ReservationCross.findByIdAndEnabled(id, true)
    }

    ReservationCross changePassword(ReservationCross reservationCross, LoginManager loginManager) {
        return create(reservationCross, loginManager)
    }

    Long findMaxCode() {
        ReservationCross reservationCross = ReservationCross.withCriteria {
            order("id", "desc")
        }.find() as ReservationCross
        if (!reservationCross) {
            return 0
        }
        return reservationCross.code
    }

}
