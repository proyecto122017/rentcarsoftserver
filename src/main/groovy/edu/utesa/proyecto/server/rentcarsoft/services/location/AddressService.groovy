package edu.utesa.proyecto.server.rentcarsoft.services.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Address
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("addressService")
@Transactional(rollbackFor = Exception.class)
class AddressService {

    private Address createBase(Address address, String userName) {
        if (address != null && !address.hasErrors()) {
            if (Address.exists(address.id)) {
                address.lastUpdated = new Date()
                address.modifyBy = userName
                return address.merge(flush: true, failOnError: true)
            } else {
                address.createdBy = userName
                address.dateCreated = new Date()
                address = address.save(flush: true, failOnError: true)
                return address
            }
        }
        return null
    }

    Address bootStrap(Address address) {
        return createBase(address, Constants.ROOT)
    }

    Address create(Address address, LoginManager loginManager) {
        return createBase(address, loginManager.getUsername())
    }

    List<Address> list(boolean enabled, int start, int size) {
        return Address.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Address> list(boolean enabled, boolean admin, int start, int size) {
        return Address.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Address> list(String name, int size) {
        return Address.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Address> list(boolean enabled) {
        return Address.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Address> list(boolean enabled, boolean admin) {
        return Address.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Address refresh(Long id) {
        Address address = Address.findById(id)
        return address
    }

    int count(boolean enabled) {
        return Address.countByEnabled(enabled)
    }

    Address delete(Address address, LoginManager loginManager) {
        if (!address) {
            return null
        }
        address = Address.findById(address.id)
        address.enabled = !address.enabled
        return create(address, loginManager)
    }

    Address byId(Long id) {
        return Address.findByIdAndEnabled(id, true)
    }

    Address byCode(int id) {
        return Address.findByCodeAndEnabled(id, true)
    }

    Address changePassword(Address address, LoginManager loginManager) {
        return create(address, loginManager)
    }

    Long findMaxCode() {
        Address address = Address.withCriteria {
            order("id", "desc")
        }.find() as Address
        if (!address) {
            return 0
        }
        return address.code
    }

}
