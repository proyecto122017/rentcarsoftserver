package edu.utesa.proyecto.server.rentcarsoft.domains.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.BranchOffice
import edu.utesa.proyecto.server.rentcarsoft.domains.security.User
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting.InvoiceHeaderAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class InvoiceHeader implements APIConverter<InvoiceHeaderAPI> {

    BranchOffice branchOffice

    int code
    BigInteger invoiceNumber
    Date date
    BigDecimal taxes
    BigDecimal subTotalAmount
    BigDecimal totalAmount
    Payment payment
    Types estatusType

    User user

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [invoiceDetailRentList: InvoiceDetailRent]

    static constraints = {
        code unique: true
    }

    static mapping = {
        table "invoices_header"

        branchOffice fetch: "join"
        payment fetch: "join"
        estatusType fetch: "join"
        invoiceDetailRentList lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((InvoiceHeader) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    InvoiceHeaderAPI toAPI() {
        InvoiceHeaderAPI invoiceAPI = new InvoiceHeaderAPI()
        invoiceAPI.code = code
        invoiceAPI.branchOffice = branchOffice
        invoiceAPI.invoiceNumber = invoiceNumber
        invoiceAPI.date = date
        invoiceAPI.taxes = taxes
        invoiceAPI.subTotalAmount = subTotalAmount
        invoiceAPI.totalAmount = totalAmount
        invoiceAPI.payment = payment
        invoiceAPI.estatusType = estatusType.toAPI()
        invoiceAPI.user = user.toAPI()
        invoiceAPI.invoiceDetailRentList = new ArrayList<>()
        for (InvoiceDetailRent detailRent : invoiceDetailRentList) {
            invoiceAPI.invoiceDetailRentList.add(detailRent.toAPI())
        }
        return invoiceAPI
    }
}
