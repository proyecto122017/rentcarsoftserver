package edu.utesa.proyecto.server.rentcarsoft.services.security

import edu.utesa.proyecto.server.rentcarsoft.domains.security.Gender
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("genderService")
@Transactional(rollbackFor = Exception.class)
class GenderService {

    private Gender createBase(Gender gender, String userName) {
        if (gender != null && !gender.hasErrors()) {
            if (Gender.exists(gender.id)) {
                gender.lastUpdated = new Date()
                gender.modifyBy = userName
                return gender.merge(flush: true, failOnError: true)
            } else {
                gender.createdBy = userName
                gender.dateCreated = new Date()
                gender = gender.save(flush: true, failOnError: true)
                return gender
            }
        }
        return null
    }

    Gender bootStrap(Gender gender) {
        return createBase(gender, Constants.ROOT)
    }

    Gender create(Gender gender, LoginManager loginManager) {
        return createBase(gender, loginManager.getUsername())
    }

    List<Gender> list(boolean enabled, int start, int size) {
        return Gender.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Gender> list(boolean enabled, boolean admin, int start, int size) {
        return Gender.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Gender> list(String name, int size) {
        return Gender.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Gender> list(boolean enabled) {
        return Gender.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Gender> likeGenres(String name) {
        return Gender.withCriteria {
            like "description", "%" + name + "%"
        }.findAll()
    }

    List<Gender> list(boolean enabled, boolean admin) {
        return Gender.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Gender refresh(Long id) {
        Gender gender = Gender.findById(id)
        return gender
    }

    int count(boolean enabled) {
        return Gender.countByEnabled(enabled)
    }

    Gender delete(Gender gender, LoginManager loginManager) {
        if (!gender) {
            return null
        }
        gender = Gender.findById(gender.id)
        gender.enabled = !gender.enabled
        return create(gender, loginManager)
    }

    Gender byId(Long id) {
        return Gender.findByIdAndEnabled(id, true)
    }

    Gender byCode(int id) {
        return Gender.findByCodeAndEnabled(id, true)
    }

    Gender changePassword(Gender gender, LoginManager loginManager) {
        return create(gender, loginManager)
    }

    Long findMaxCode() {
        Gender gender = Gender.withCriteria {
            order("id", "desc")
        }.find() as Gender
        if (!gender) {
            return 1
        }
        return gender.code
    }

}
