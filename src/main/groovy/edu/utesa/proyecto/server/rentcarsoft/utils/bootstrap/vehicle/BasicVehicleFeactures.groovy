package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleFeactures
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleFeacturesService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicVehicleFeactures {

    @Autowired
    private VehicleFeacturesService vehicleFeacturesService

    VehicleFeactures fourDoors
    VehicleFeactures gasoline
    VehicleFeactures gasoil
    VehicleFeactures ecoMode

    BasicVehicleFeactures() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        fourDoors = create(1, "4 doors")
        gasoline = create(2, "gasoline")
        gasoil = create(3, "gas oil")
        ecoMode = create(4, "eco mode")
    }

    private VehicleFeactures create(int code, String name) {
        VehicleFeactures vehicleFeactures = vehicleFeacturesService.byCode(code)
        if (!vehicleFeactures) {
            vehicleFeactures = new VehicleFeactures()
            vehicleFeactures.code = code
            vehicleFeactures.description = name
            return vehicleFeacturesService.bootStrap(vehicleFeactures)
        }
        return vehicleFeactures
    }
}
