package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.*
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Coordinates
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.administration.*
import edu.utesa.proyecto.server.rentcarsoft.services.administrator.*
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/administration")
@CompileStatic
class AdministratorResources {

    static final SessionManager sessionManager = new SessionManager()

    @Autowired
    private BranchOfficeService branchOfficeService
    @Autowired
    private ContactService contactService
    @Autowired
    private EnterpriseService enterpriseService
    @Autowired
    private NotificationService notificationService
    @Autowired
    private RentcarService rentcarService

    @GET
    static String isUP() {
        return "OK"
    }

    @POST
    @Path('/branchOffices')
    String listBranchOffices(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<BranchOfficeAPI> branchOfficeAPIS = new ArrayList<>()
        for (BranchOffice branchOffice : branchOfficeService.list(true)) {
            branchOfficeAPIS.add(branchOffice.toAPI())
        }
        return Constants.instance.stringify(branchOfficeAPIS)
    }

    @POST
    @Path('/create/branchOffice')
    String createBranchOffices(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        BranchOfficeAPI branchOfficeAPI = Constants.instance.convert(request, BranchOfficeAPI.class)
        if (!ContactAPI) {
            // No parsea el objeto que enviaron
        }
        BranchOffice branchOffice = new BranchOffice()
        branchOffice.code = branchOfficeService.findMaxCode() + 1 as Integer
        branchOffice.coordinates = branchOfficeAPI.coordinates as Coordinates
        branchOffice.generalInfo = branchOfficeAPI.generalInfo as GeneralInfo
        branchOffice.branchOfficeType = branchOfficeAPI.branchOfficeType as Types
        branchOfficeService.bootStrap(branchOffice)
        return "Salve la data"
    }

    @POST
    @Path('/create/contacts')
    String createContacts(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        ContactAPI contactAPI = Constants.instance.convert(request, ContactAPI.class)
        if (!ContactAPI) {
            // No parsea el objeto que enviaron
        }
        Contact contact = new Contact()
        contact.code = contactService.findMaxCode() + 1 as Integer
        contact.description = contactAPI.description ?: ""
        contactService.bootStrap(contact)

        return "Salve la data"
    }

    @POST
    @Path('/contacts')
    String listContacts(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<ContactAPI> contactAPIS = new ArrayList<>()
        for (Contact contact : contactService.list(true)) {
            contactAPIS.add(contact.toAPI())
        }
        return Constants.instance.stringify(contactAPIS)
    }

    @POST
    @Path('/enterprises')
    String listEnterprises(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<EnterpriseAPI> enterpriseAPIS = new ArrayList<>()
        for (Enterprise enterprise : enterpriseService.list()) {
            enterpriseAPIS.add(enterprise.toAPI())
        }
        return Constants.instance.stringify(enterpriseAPIS)
    }

    @POST
    @Path('/create/enterprises')
    String createEnterprise(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        EnterpriseAPI enterpriseAPI = Constants.instance.convert(request, EnterpriseAPI.class)
        if (!enterpriseAPI) {
            // No parsea el objeto que enviaron
        }
        Enterprise enterprise = new Enterprise()
        enterprise.code = enterpriseService.findMaxCode() + 1 as Integer
        enterprise.generalInfo = enterpriseAPI.generalInfo as GeneralInfo
        enterpriseService.bootStrap(enterprise)
        return "Salve la data"
    }

    @POST
    @Path('/create/notifications')
    String createNotifications(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        NotificationsAPI notificationsAPI = Constants.instance.convert(request, NotificationsAPI.class)
        if (!notificationsAPI) {
            // No parsea el objeto que enviaron
        }
        Notifications notifications = new Notifications()
        notifications.code = notificationService.findMaxCode() + 1 as Integer
        notifications.description = notificationsAPI.description ?: ""
        notificationService.bootStrap(notifications)
        return "Salve la data"
    }

    @POST
    @Path('/notifications')
    String listNotifications(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<NotificationsAPI> notificationsAPIS = new ArrayList<>()
        for (Notifications notifications : notificationService.list(true)) {
            notificationsAPIS.add(notifications.toAPI())
        }
        return Constants.instance.stringify(notificationsAPIS)
    }

    @POST
    @Path('/create/rentcar')
    String createRentcar(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        RentcarAPI rentcarAPI = Constants.instance.convert(request, RentcarAPI.class)
        if (!rentcarAPI) {
            // No parsea el objeto que enviaron
        }
        Rentcar rentcar = new Rentcar()
        rentcar.code = rentcarService.findMaxCode() + 1 as Integer
        rentcar.generalInfo = rentcarAPI.generalInfo as GeneralInfo
        rentcarService.bootStrap(rentcar)
        return "Salve la data"
    }

    @POST
    @Path('/rentcars')
    String listRentcars(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<RentcarAPI> rentcarAPIS = new ArrayList<>()
        for (Rentcar rentcar : rentcarService.list()) {
            rentcarAPIS.add(rentcar.toAPI())
        }
        return Constants.instance.stringify(rentcarAPIS)
    }

}
