package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Sector
import edu.utesa.proyecto.server.rentcarsoft.services.location.SectorService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicSector {

    @Autowired
    private SectorService sectorService

    Sector sabaneta
    Sector hatoMayor
    Sector dicayagua
    Sector gurabo
    Sector maimon
    Sector paloverde
    Sector dosa

    BasicSector() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        sabaneta = create(1, "sabaneta")
        hatoMayor = create(2, "hatoMayor")
        dicayagua = create(3, "dicayagua")
        gurabo = create(4, "gurabo")
        maimon = create(5, "maimon")
        paloverde = create(6, "paloverde")
        dosa = create(7, "dosa")
    }

    private Sector create(int code, String name) {
        Sector city = sectorService.byCode(code)
        if (!city) {
            city = new Sector()
            city.code = code
            city.description = name
            return sectorService.bootStrap(city)
        }
        return city
    }
}
