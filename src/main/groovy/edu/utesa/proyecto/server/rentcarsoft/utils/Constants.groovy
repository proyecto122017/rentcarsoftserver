package edu.utesa.proyecto.server.rentcarsoft.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import edu.utesa.proyecto.server.rentcarsoft.RentCarSoftSpringInit
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationListener
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.ContextClosedEvent

import java.nio.file.Files
import java.nio.file.Paths
import java.text.SimpleDateFormat

/**
 *  Created by ronald on 7/4/17.*/
@CompileStatic
@Singleton
class Constants {

    @Value('${info.app.version}')
    String VERSION

    public static final Boolean DEVELOP_MODE = true
    public static final String PARAM_DATA = "data"
    public static final String PARAM_SESSION = "session"
    public static final String LOCAL_URL = "http://localhost:8084/api/"
    public static final String SERVER_URL = "http:/37.187.142.47:8084/api/"
    public static final String TEST_URL = SERVER_URL
    public static final String EXTENSION = ".json"

    private static final String HOUR_FORMAT = "HH:mm:ss"

    public static final String DATE_FIND = "yyyy-MM-dd"

    private static final String DATE_HOUR_FIND = "yyyy-MM-dd HH:mm:ss"

    final String DATE_FORMAT = DATE_FIND + " " + HOUR_FORMAT

    public static final SimpleDateFormat hourFormat = new SimpleDateFormat(HOUR_FORMAT)

    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FIND)

    public static final SimpleDateFormat simpleDateFormatToFind = new SimpleDateFormat(DATE_HOUR_FIND)

    final String MIN_HOUR = " 00:00:00"

    final String MAX_HOUR = " 23:59:59"

    static final String ROOT = "System"

    static final String ROOT_USERNAME = "root"

    private final Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).setPrettyPrinting().create()

    final SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat(DATE_FORMAT)

    private final SimpleDateFormat simpleDateFormatQuery = new SimpleDateFormat(DATE_HOUR_FIND)

    static final SimpleDateFormat simpleDateFormatToFind = new SimpleDateFormat(DATE_HOUR_FIND)

    int KEY = 215001
    int KEYYEL = 123456

    public ConfigurableApplicationContext applicationContext

    void setApplicationContext(ConfigurableApplicationContext applicationContext) {
        this.applicationContext = applicationContext
        this.applicationContext.addApplicationListener(new ApplicationListener<ContextClosedEvent>() {
            @Override
            void onApplicationEvent(ContextClosedEvent event) {
                RentCarSoftSpringInit.stop()
            }
        })
        autoWiredClass(this)
    }

    void autoWiredClass(Object objectToWired) {
        applicationContext.getAutowireCapableBeanFactory().autowireBean(objectToWired)
    }

    String stringify(Object object) {
        return gson.toJson(object)
    }

    String readJsonFile(String fileName) {
        return new String(Files.readAllBytes(Paths.get(getClass().getResource("/" + fileName + EXTENSION).toURI())))
    }

    String stringifyCifrado(Object object) {
        return cifrar(stringify(object), KEY)
    }

    String stringifyDesCifrado(Object object) {
        return descifrar(stringify(object), KEY)
    }

    String stringifyCifradoYel(Object object) {
        return cifrar(stringify(object), KEYYEL)
    }

    String stringifyDesCifradoYel(Object object) {
        return descifrar(stringify(object), KEYYEL)
    }

    final Object validRequest(String data, Class validClass) {
        return convert(data, validClass)
    }

    final <T> T convert(String data, Class<T> validClass) {
        if (data) {
            try {
                return gson.fromJson(data, validClass)
            } catch (Exception ignored) {
                ignored.printStackTrace()
                println "Error de parseo: "
                println "Clase: " + validClass.toString()
                println "Data: " + data
                return null
            }
        }
        return null
    }


    final String showDate(Date date) {
        return simpleDateFormatDate.format(date)
    }

    final static String showDateFormated(Date date) {
        return simpleDateFormatToFind.format(date)
    }

    final Date parseLessThanEqualDateQuery(Date date) {
        return simpleDateFormatQuery.parse(simpleDateFormatDate.format(date) + MAX_HOUR)
    }

    final Date parseGreaterEqualDateQuery(Date date) {
        return simpleDateFormatQuery.parse(simpleDateFormatDate.format(date) + MIN_HOUR)
    }

    static String cifrar(String original, int clave) {
        StringBuilder cifrado = new StringBuilder(original.length())
        for (char c : original.toCharArray()) {
            int val = (c as int) + clave % 255
            cifrado.append(val as char)
        }
        return cifrado.toString()
    }

    static String descifrar(String original, int clave) {
        StringBuilder descifrado = new StringBuilder(original.length())
        for (char c : original.toCharArray()) {
            int val = (c as int) - clave % 255
            descifrado.append(val as char)
        }
        return descifrado.toString()
    }

//    String Encryption(String texto, String secretKey) {
//        String base64EncryptedString = ""
//        try {
//            MessageDigest md = MessageDigest.getInstance("MD5")
//            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"))
//            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24)
//            SecretKey key = new SecretKeySpec(keyBytes, "DESede")
//            Cipher cipher = Cipher.getInstance("DESede")
//            cipher.init(Cipher.ENCRYPT_MODE, key)
//            byte[] plainTextBytes = texto.getBytes("utf-8")
//            byte[] buf = cipher.doFinal(plainTextBytes)
//            byte[] base64Bytes = Base64.encodeBase64(buf)
//            base64EncryptedString = new String(base64Bytes)
//        } catch (Exception ex) {
//            System.out.println("Error no es posible seguir el proceso de encriptacion.\n" + ex)
//        }
//        return base64EncryptedString
//    }
//
//    String Desencryptions(String textoEncriptado, String secretKey) throws Exception {
//        String base64EncryptedString = ""
//        try {
//            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"))
//            MessageDigest md = MessageDigest.getInstance("MD5")
//            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"))
//            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24)
//            SecretKey key = new SecretKeySpec(keyBytes, "DESede")
//            Cipher decipher = Cipher.getInstance("DESede")
//            decipher.init(Cipher.DECRYPT_MODE, key)
//            byte[] plainText = decipher.doFinal(message)
//            base64EncryptedString = new String(plainText, "UTF-8")
//        } catch (Exception ex) {
//            System.out.println("Error no es posible seguir el proceso de desencriptacion.\n" + ex)
//        }
//        return base64EncryptedString
//    }
}
