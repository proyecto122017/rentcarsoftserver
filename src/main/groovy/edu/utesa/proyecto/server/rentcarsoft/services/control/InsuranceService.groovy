package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Insurance
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("insuranceService")
@Transactional(rollbackFor = Exception.class)
class InsuranceService {

    private Insurance createBase(Insurance insurance, String userName) {
        if (insurance != null && !insurance.hasErrors()) {
            if (Insurance.exists(insurance.id)) {
                insurance.lastUpdated = new Date()
                insurance.modifyBy = userName
                return insurance.merge(flush: true, failOnError: true)
            } else {
                insurance.createdBy = userName
                insurance.dateCreated = new Date()
                insurance = insurance.save(flush: true, failOnError: true)
                return insurance
            }
        }
        return null
    }

    Insurance bootStrap(Insurance insurance) {
        return createBase(insurance, Constants.ROOT)
    }

    Insurance create(Insurance insurance, LoginManager loginManager) {
        return createBase(insurance, loginManager.getUsername())
    }

    List<Insurance> list(boolean enabled, int start, int size) {
        return Insurance.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Insurance> list(boolean enabled, boolean admin, int start, int size) {
        return Insurance.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Insurance> list(String name, int size) {
        return Insurance.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Insurance> list(boolean enabled) {
        return Insurance.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Insurance> list(boolean enabled, boolean admin) {
        return Insurance.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Insurance refresh(Long id) {
        Insurance insurance = Insurance.findById(id)
        return insurance
    }

    int count(boolean enabled) {
        return Insurance.countByEnabled(enabled)
    }

    Insurance delete(Insurance insurance, LoginManager loginManager) {
        if (!insurance) {
            return null
        }
        insurance = Insurance.findById(insurance.id)
        insurance.enabled = !insurance.enabled
        return create(insurance, loginManager)
    }

    Insurance byId(Long id) {
        return Insurance.findByIdAndEnabled(id, true)
    }

    Insurance changePassword(Insurance insurance, LoginManager loginManager) {
        return create(insurance, loginManager)
    }

    Long findMaxCode() {
        Insurance insurance = Insurance.withCriteria {
            order("id", "desc")
        }.find() as Insurance
        if (!insurance) {
            return 0
        }
        return insurance.code
    }

}
