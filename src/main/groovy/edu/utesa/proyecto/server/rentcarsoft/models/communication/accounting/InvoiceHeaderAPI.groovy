package edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.InvoiceDetailRent
import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.domains.administration.BranchOffice
import edu.utesa.proyecto.server.rentcarsoft.domains.security.User
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.UserAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.TypesAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by ronald on 4/17/17.
 */
@CompileStatic
class InvoiceHeaderAPI {

    BranchOffice branchOffice
    int code
    BigInteger invoiceNumber
    Date date
    BigDecimal taxes
    BigDecimal subTotalAmount
    BigDecimal totalAmount
    Payment payment
    TypesAPI estatusType
    UserAPI user
    List<InvoiceDetailRentAPI> invoiceDetailRentList

}
