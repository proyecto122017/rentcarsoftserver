package edu.utesa.proyecto.server.rentcarsoft.utils.security

import groovy.transform.CompileStatic

@CompileStatic
class Session {

    String username

    String session
    String key

    Date date

    Session(String username, String session, String key) {
        this.username = username
        this.session = session
        this.key = key
        this.date = new Date()
    }
}
