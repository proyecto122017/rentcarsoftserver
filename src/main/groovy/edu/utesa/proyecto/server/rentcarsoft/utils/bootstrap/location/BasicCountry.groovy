package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Country
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Province
import edu.utesa.proyecto.server.rentcarsoft.services.location.CountryService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicCountry {

    @Autowired
    private CountryService countryService

    Country repDom

    BasicCountry() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicProvince basicProvince) {
        repDom = create(1, "Rep. Dom", basicProvince.santiago)
    }

    private Country create(int code, String name, Province province) {
        Country city = countryService.byCode(code)
        if (!city) {
            city = new Country()
            city.code = code
            city.description = name
            city.province = province
            return countryService.bootStrap(city)
        }
        return city
    }
}
