package edu.utesa.proyecto.server.rentcarsoft.utils

import groovy.transform.CompileStatic
import org.jsoup.Connection
import org.jsoup.Jsoup

/**
 *  Created by ronald on 7/4/17.
 */
@CompileStatic
@Singleton
class ComConstants {

    protected final String userAgent = 'Ronald Rules'
    static final String PARAM_FORM_RESOURCE = "data"
    static final String LOCAL_SERVER_URL = "http://localhost:8084/api"

    protected final int TIME_OUT = 20 * 1000

    Connection confConnection(Connection.Method method, Connection connection, String data) {
        connection.userAgent(userAgent)
        connection.timeout(TIME_OUT)
        connection.ignoreContentType(true)
        connection.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
        connection.method(method)
        connection.data(PARAM_FORM_RESOURCE, data)
        return connection
    }

    private String sendData(Connection.Method method, String url, String data) {
        try {
            Connection.Response queryConnection = confConnection(method, Jsoup.connect(url), data).execute()
            return Jsoup.parse(queryConnection.body()).text()
        } catch (Exception ignored) {
            return null
        }
    }

    protected final String sendGET(String url, String data) {
        return sendData(Connection.Method.GET, url, data)
    }
//
//    List<ProfesorAPI> apiProfesors() {
//        String data = sendGET(LOCAL_SERVER_URL + "/external/profesors", "klk")
//        String descripted = Constants.descifrar(data, Constants.instance.KEY)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        //TODO : ESTA ES LA VAINA QUE FUNCIONA DE VERDAD
//        List<ProfesorAPI> profesorAPIS = Constants.instance.convert(descripted, new ProfesorAPI[1].class) as List<ProfesorAPI>
//        println "server response: Converted " + Constants.instance.stringify(profesorAPIS)
//        return profesorAPIS
//    }
//
//    List<ProfesorAPI> apiProfesorsYelson() {
//        String data = sendGET(YELSON_SERVER_URL + "/sync/teachers", "")
//        String descripted = Constants.descifrar(data, Constants.instance.KEYYEL)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<ProfesorAPI> profesorAPIS = Constants.instance.convert(descripted, List.class) as List<ProfesorAPI>
//        println "server response: Converted " + Constants.instance.stringify(profesorAPIS)
//        return profesorAPIS
//    }
//
//    List<GradoAPI> apiGrados() {
//        String data = sendGET(YELSON_SERVER_URL + "/external/grados", "klk")
//        String descripted = Constants.descifrar(data, Constants.instance.KEY)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<GradoAPI> gradoAPIS = Constants.instance.convert(descripted, List.class) as List<GradoAPI>
//        println "server response: Converted " + Constants.instance.stringify(gradoAPIS)
//        return gradoAPIS
//    }
//
//    List<GradoAPI> apiGradosYelson() {
//        String data = sendGET(YELSON_SERVER_URL + "/sync/grades", "")
//        println YELSON_SERVER_URL + "/sync/grades"
//        String descripted = Constants.descifrar(data, Constants.instance.KEYYEL)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<GradoAPI> gradoAPIS = Constants.instance.convert(descripted, List.class) as List<GradoAPI>
//        println "server response: Converted " + Constants.instance.stringify(gradoAPIS)
//        return gradoAPIS
//    }
//
//    List<MateriaAPI> apiMaterias() {
//        String data = sendGET(YELSON_SERVER_URL + "/external/materias", "")
//        String descripted = Constants.descifrar(data, Constants.instance.KEY)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<MateriaAPI> materiaAPIS = Constants.instance.convert(descripted, List.class) as List<MateriaAPI>
//        println "server response: Converted " + Constants.instance.stringify(materiaAPIS)
//        return materiaAPIS
//    }
//
//    List<MateriaAPI> apiMateriasYelson() {
//        String data = sendGET(YELSON_SERVER_URL + "/sync/subjects", "")
//        String descripted = Constants.descifrar(data, Constants.instance.KEYYEL)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<MateriaAPI> materiaAPIS = Constants.instance.convert(descripted, List.class) as List<MateriaAPI>
//        println "server response: Converted " + Constants.instance.stringify(materiaAPIS)
//        return materiaAPIS
//    }

}
