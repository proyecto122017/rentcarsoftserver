package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.security.Gender
import edu.utesa.proyecto.server.rentcarsoft.domains.security.Occupation
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Identification
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Person
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.services.utils.PersonService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.security.BasicGender
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.security.BasicOccupation
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicPerson {

    @Autowired
    private PersonService personService

    Person ronald
    Person sandy
    Person deli

    BasicPerson() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicTypes basicTypes, BasicGender basicGender, BasicOccupation basicOccupation) {
        ronald = create(1, "Ronald", basicTypes.typesUsuarioAdmin, basicGender.hombre, basicOccupation.desarrollador, new Date(), new Identification(identification: "3333333", identificationType: basicTypes.typesIdentificationCedula))
        sandy = create(2, "Sandy", basicTypes.typesUsuarioNormal, basicGender.hombre, basicOccupation.licensiado, new Date(), new Identification(identification: "3445345", identificationType: basicTypes.typesIdentificationCedula))
        deli = create(3, "deli", basicTypes.typesUsuarioVIP, basicGender.mujer, basicOccupation.doctor, new Date(), new Identification(identification: "3453454", identificationType: basicTypes.typesIdentificationCedula))
    }

    private Person create(int code, String name, Types types, Gender gender, Occupation occupation,
                          Date birthDate, Identification identification) {
        Person person = personService.byName(name)
        if (person) {
            return person
        }
        person = new Person()
        person.code = code
        person.name = name
        person.type = types
        person.gender = gender
        person.occupation = occupation
        person.birthDate = birthDate
        person.identification = identification
        return personService.bootStrap(person)
    }
}
