package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleColor
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleColorService")
@Transactional(rollbackFor = Exception.class)
class VehicleColorService {

    private VehicleColor createBase(VehicleColor vehicleColor, String userName) {
        if (vehicleColor != null && !vehicleColor.hasErrors()) {
            if (VehicleColor.exists(vehicleColor.id)) {
                vehicleColor.lastUpdated = new Date()
                vehicleColor.modifyBy = userName
                return vehicleColor.merge(flush: true, failOnError: true)
            } else {
                vehicleColor.createdBy = userName
                vehicleColor.dateCreated = new Date()
                vehicleColor = vehicleColor.save(flush: true, failOnError: true)
                return vehicleColor
            }
        }
        return null
    }

    VehicleColor bootStrap(VehicleColor vehicleColor) {
        return createBase(vehicleColor, Constants.ROOT)
    }

    VehicleColor create(VehicleColor vehicleColor, LoginManager loginManager) {
        return createBase(vehicleColor, loginManager.getUsername())
    }

    List<VehicleColor> list(boolean enabled, int start, int size) {
        return VehicleColor.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleColor> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleColor.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleColor> list(String name, int size) {
        return VehicleColor.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleColor> list(boolean enabled) {
        return VehicleColor.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleColor> list(boolean enabled, boolean admin) {
        return VehicleColor.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleColor refresh(Long id) {
        VehicleColor vehicleColor = VehicleColor.findById(id)
        return vehicleColor
    }

    int count(boolean enabled) {
        return VehicleColor.countByEnabled(enabled)
    }

    VehicleColor delete(VehicleColor vehicleColor, LoginManager loginManager) {
        if (!vehicleColor) {
            return null
        }
        vehicleColor = VehicleColor.findById(vehicleColor.id)
        vehicleColor.enabled = !vehicleColor.enabled
        return create(vehicleColor, loginManager)
    }

    VehicleColor byColor(String color) {
        return VehicleColor.findByColorAndEnabled(color, true)
    }

    VehicleColor byId(Long id) {
        return VehicleColor.findByIdAndEnabled(id, true)
    }

    VehicleColor byCode(int id) {
        return VehicleColor.findByCodeAndEnabled(id, true)
    }

    Long findMaxCode() {
        VehicleColor vehicleColor = VehicleColor.withCriteria {
            order("id", "desc")
        }.find() as VehicleColor
        if (!vehicleColor) {
            return 1
        }
        return vehicleColor.code
    }

}
