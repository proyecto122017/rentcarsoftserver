package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.Vehicle
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleService")
@Transactional(rollbackFor = Exception.class)
class VehicleService {

    private Vehicle createBase(Vehicle vehicle, String userName) {
        if (vehicle != null && !vehicle.hasErrors()) {
            if (Vehicle.exists(vehicle.id)) {
                vehicle.lastUpdated = new Date()
                vehicle.modifyBy = userName
                return vehicle.merge(flush: true, failOnError: true)
            } else {
                vehicle.createdBy = userName
                vehicle.dateCreated = new Date()
                vehicle = vehicle.save(flush: true, failOnError: true)
                return vehicle
            }
        }
        return null
    }

    Vehicle bootStrap(Vehicle vehicle) {
        return createBase(vehicle, Constants.ROOT)
    }

    Vehicle create(Vehicle vehicle, LoginManager loginManager) {
        return createBase(vehicle, loginManager.getUsername())
    }

    List<Vehicle> list(boolean enabled, int start, int size) {
        return Vehicle.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Vehicle> list(boolean enabled, boolean admin, int start, int size) {
        return Vehicle.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Vehicle> list(String name, int size) {
        return Vehicle.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Vehicle> list(boolean enabled) {
        return Vehicle.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Vehicle> list(boolean enabled, boolean admin) {
        return Vehicle.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Vehicle refresh(Long id) {
        Vehicle vehicle = Vehicle.findById(id)
        return vehicle
    }

    int count(boolean enabled) {
        return Vehicle.countByEnabled(enabled)
    }

    int count(boolean enabled, boolean admin) {
        return Vehicle.countByEnabledAndAdmin(enabled, admin)
    }

    Vehicle delete(Vehicle vehicle, LoginManager loginManager) {
        if (!vehicle) {
            return null
        }
        vehicle = Vehicle.findById(vehicle.id)
        vehicle.enabled = !vehicle.enabled
        return create(vehicle, loginManager)
    }

    Vehicle byVehiclename(String vehiclename) {
        return Vehicle.findByVehiclenameAndEnabled(vehiclename, true)
    }

    Vehicle byName(String name) {
        return Vehicle.findByNameAndEnabled(name, true)
    }

    Vehicle byId(Long id) {
        return Vehicle.findByIdAndEnabled(id, true)
    }

    Vehicle byCode(int id) {
        return Vehicle.findByCodeAndEnabled(id, true)
    }

    Vehicle changePassword(Vehicle vehicle, LoginManager loginManager) {
        return create(vehicle, loginManager)
    }

    Long findMaxCode() {
        Vehicle vehicle = Vehicle.withCriteria {
            order("id", "desc")
        }.find() as Vehicle
        if (!vehicle) {
            return 0
        }
        return vehicle.code
    }

}
