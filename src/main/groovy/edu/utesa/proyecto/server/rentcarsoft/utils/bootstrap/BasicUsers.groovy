package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap

import edu.utesa.proyecto.server.rentcarsoft.domains.security.User
import edu.utesa.proyecto.server.rentcarsoft.services.security.UserService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.HashUtils
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicUsers {

    @Autowired
    private UserService userService

    User userRoot
    User userTest
    User userGeneric

    BasicUsers() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        userRoot = create(true, 1, "root", "ronald03", "Test", "",
                "000-000-0000", "000-000-0000", null, null, null, new Date(), new Date(), new Date(), "", "")
            userGeneric = create(false, 2, "generic", "generic", "Generic", "",
                "000-000-0000", "000-000-0000", null, null, null, new Date(), new Date(), new Date(), "", "")
        userTest = create(false, 3, "test", "test", "Test", "",
                "000-000-0000", "000-000-0000", null, null, null, new Date(), new Date(), new Date(), "", "")
    }

    private User create(boolean admin, int idUser, String username, String password, String name,
                        String address, String phone, String cellphone, String email, String email_sec,
                        String identification, Date birthDate, Date registerDate,
                        Date ingressDate, String photo, String contact) {
        User user = userService.byUsername(username)
        if (!user) {
            user = new User()
            user.admin = admin
            user.code = idUser
            user.name = name
            user.username = username
            user.password = HashUtils.encodeSHA256(password)
            user.registerDate = registerDate
            user.ingressDate = ingressDate
            return userService.bootStrap(user)
        }
        return user
    }
}
