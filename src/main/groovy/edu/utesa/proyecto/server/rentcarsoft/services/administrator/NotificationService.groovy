package edu.utesa.proyecto.server.rentcarsoft.services.administrator

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Notifications
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("notificationService")
@Transactional(rollbackFor = Exception.class)
class NotificationService {

    private Notifications createBase(Notifications notifications, String userName) {
        if (notifications != null && !notifications.hasErrors()) {
            if (Notifications.exists(notifications.id)) {
                notifications.lastUpdated = new Date()
                notifications.modifyBy = userName
                return notifications.merge(flush: true, failOnError: true)
            } else {
                notifications.createdBy = userName
                notifications.dateCreated = new Date()
                notifications = notifications.save(flush: true, failOnError: true)
                return notifications
            }
        }
        return null
    }

    Notifications bootStrap(Notifications notifications) {
        return createBase(notifications, Constants.ROOT)
    }

    Notifications create(Notifications notifications, LoginManager loginManager) {
        return createBase(notifications, loginManager.getUsername())
    }

    List<Notifications> list(boolean enabled, int start, int size) {
        return Notifications.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Notifications> list(boolean enabled, boolean admin, int start, int size) {
        return Notifications.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Notifications> list(String name, int size) {
        return Notifications.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Notifications> list(boolean enabled) {
        return Notifications.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Notifications> list(boolean enabled, boolean admin) {
        return Notifications.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Notifications refresh(Long id) {
        Notifications notifications = Notifications.findById(id)
        return notifications
    }

    int count(boolean enabled) {
        return Notifications.countByEnabled(enabled)
    }

    Notifications delete(Notifications notifications, LoginManager loginManager) {
        if (!notifications) {
            return null
        }
        notifications = Notifications.findById(notifications.id)
        notifications.enabled = !notifications.enabled
        return create(notifications, loginManager)
    }

    Notifications byId(Long id) {
        return Notifications.findByIdAndEnabled(id, true)
    }

    Notifications changePassword(Notifications notifications, LoginManager loginManager) {
        return create(notifications, loginManager)
    }

    Long findMaxCode() {
        Notifications notifications = Notifications.withCriteria {
            order("id", "desc")
        }.find() as Notifications
        if (!notifications) {
            return 0
        }
        return notifications.code
    }

}
