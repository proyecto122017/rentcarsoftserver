package edu.utesa.proyecto.server.rentcarsoft.domains.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Contact
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Address
import edu.utesa.proyecto.server.rentcarsoft.domains.security.Gender
import edu.utesa.proyecto.server.rentcarsoft.domains.security.Occupation
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.PersonAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by Desckop on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Person implements APIConverter<PersonAPI> {

    int code
    String name
    String photo
    Date birthDate
    Types type
    Identification identification
    Gender gender
    Occupation occupation

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        type nullable: true
        identification nullable: true
        photo nullable: true
        birthDate nullable: true
        gender nullable: true
        occupation nullable: true
    }

    static hasMany = [listContacts: Contact, listEmails: Email, listPhones: Phone,
                      listAddress : Address]

    static mapping = {
        table "persons"

        type fetch: "join"
        occupation fetch: "join"
        gender fetch: "join"
        identification fetch: "join"

        listContacts lazy: false
        listEmails lazy: false
        listPhones lazy: false
        listAddress lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Person) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    PersonAPI toAPI() {
        PersonAPI personAPI = new PersonAPI()
        personAPI.code = code
        personAPI.name = name
        personAPI.type = type.toAPI()
        personAPI.photo = photo
        if (gender) {
            personAPI.gender = gender.toAPI()
        }
        if (occupation) {
            personAPI.occupation = occupation.toAPI()
        }
        personAPI.birthDate = birthDate
        if (identification) {
            personAPI.identification = identification.toAPI()
        }
        personAPI.emailList = new ArrayList<>()
        if (listEmails.size() > 0) {
            for (Email email : listEmails) {
                personAPI.emailList.add(email.toAPI())
            }
        }
        personAPI.contactList = new ArrayList<>()
        if (listContacts.size() > 0) {
            for (Contact contact : listContacts) {
                personAPI.contactList.add(contact.toAPI())
            }
        }
        personAPI.addressList = new ArrayList<>()
        if (listAddress.size() > 0) {
            for (Address address : listAddress) {
                personAPI.addressList.add(address.toAPI())
            }
        }
        personAPI.phoneList = new ArrayList<>()
        if (listPhones.size() > 0) {
            for (Phone phone : listPhones) {
                personAPI.phoneList.add(phone.toAPI())
            }
        }
        return personAPI
    }
}
