package edu.utesa.proyecto.server.rentcarsoft.utils.json

import groovy.transform.CompileStatic

/**
 *  Created by ronald on 6/12/17.
 */
@CompileStatic
class VehicleColorJson {

    String color

    @Override
    public String toString() {
        return "VehicleColorJson{" +
                "description='" + color + '\'' +
                '}';
    }
}
