package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("typesService")
@Transactional(rollbackFor = Exception.class)
class TypesService {

    private Types createBase(Types types, String userName) {
        if (types != null && !types.hasErrors()) {
            if (Types.exists(types.id)) {
                types.lastUpdated = new Date()
                types.modifyBy = userName
                return types.merge(flush: true, failOnError: true)
            } else {
                types.createdBy = userName
                types.dateCreated = new Date()
                types = types.save(flush: true, failOnError: true)
                return types
            }
        }
        return null
    }

    Types bootStrap(Types types) {
        return createBase(types, Constants.ROOT)
    }

    Types create(Types types, LoginManager loginManager) {
        return createBase(types, loginManager.getUsername())
    }

    List<Types> list(boolean enabled, int start, int size) {
        return Types.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Types> list(boolean enabled, boolean admin, int start, int size) {
        return Types.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Types> list(String name, int size) {
        return Types.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Types> list(boolean enabled) {
        return Types.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Types> list(boolean enabled, boolean admin) {
        return Types.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Types refresh(Long id) {
        Types types = Types.findById(id)
        return types
    }

    int count(boolean enabled) {
        return Types.countByEnabled(enabled)
    }

    Types delete(Types types, LoginManager loginManager) {
        if (!types) {
            return null
        }
        types = Types.findById(types.id)
        types.enabled = !types.enabled
        return create(types, loginManager)
    }

    Types byEmail(String types) {
        return Types.findByEmailAndEnabled(types, true)
    }

    Types byId(Long id) {
        return Types.findByIdAndEnabled(id, true)
    }

    Types byCode(Integer code) {
        return Types.findByCodeAndEnabled(code, true)
    }

    Long findMaxCode() {
        Types types = Types.withCriteria {
            order("id", "desc")
        }.find() as Types
        if (!types) {
            return 0
        }
        return types.code
    }

}
