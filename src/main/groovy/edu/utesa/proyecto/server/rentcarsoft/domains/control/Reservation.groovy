package edu.utesa.proyecto.server.rentcarsoft.domains.control

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.ReservationAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Reservation implements APIConverter<ReservationAPI> {

    int code
    String description

    Date pickup_date
    Date return_date

    Payment payment

    boolean original = true

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [listVehicleBrandOffice: VehicleBranchOffice]

    static constraints = {
        code unique: true

        description nullable: true
        payment nullable: true
    }

    static mapping = {
        table "reservation"

        payment fetch: "join"
        listVehicleBrandOffice lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Reservation) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    ReservationAPI toAPI() {
        ReservationAPI reservationAPI = new ReservationAPI()
        reservationAPI.code = code
        reservationAPI.description = description
        reservationAPI.pickup_date = pickup_date
        reservationAPI.return_date = return_date
        reservationAPI.payment = payment.toAPI()
        reservationAPI.vehicleBrandOfficeList = new ArrayList<>()
        for (VehicleBranchOffice vehicleBranchOffice : listVehicleBrandOffice) {
            reservationAPI.vehicleBrandOfficeList.add(vehicleBranchOffice.toAPI())
        }
        return reservationAPI
    }
}
