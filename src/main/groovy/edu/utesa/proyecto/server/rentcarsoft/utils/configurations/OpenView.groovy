package edu.utesa.proyecto.server.rentcarsoft.utils.configurations

import groovy.transform.CompileStatic
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.orm.hibernate5.support.OpenSessionInViewFilter

import javax.servlet.Filter

/**
 *  Created by ronald on 7/6/17.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Configuration
@CompileStatic
class OpenView {

    @Bean
    FilterRegistrationBean registerOpenSessionInViewFilterBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean()
        registrationBean.setFilter(openView())
        registrationBean.addUrlPatterns("/*")
        registrationBean.setOrder(5)
        return registrationBean
    }

    @Bean(name = "openSessionInView")
    Filter openView() {
        return new OpenSessionInViewFilter()
    }
}
