package edu.utesa.proyecto.server.rentcarsoft.domains.location

import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.SectorAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by Desckop on 8/6/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Sector implements APIConverter<SectorAPI> {

    int code
    String description

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        description nullable: true
    }

    static mapping = {
        table "sector"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Sector) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    SectorAPI toAPI() {
        SectorAPI sectorAPI = new SectorAPI()
        sectorAPI.code = code
        sectorAPI.description = description
        return sectorAPI
    }
}
