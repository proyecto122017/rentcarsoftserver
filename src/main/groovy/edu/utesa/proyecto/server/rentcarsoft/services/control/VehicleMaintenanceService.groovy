package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.VehicleMaintenance
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleMaintenanceService")
@Transactional(rollbackFor = Exception.class)
class VehicleMaintenanceService {

    private VehicleMaintenance createBase(VehicleMaintenance vehicleMaintenance, String userName) {
        if (vehicleMaintenance != null && !vehicleMaintenance.hasErrors()) {
            if (VehicleMaintenance.exists(vehicleMaintenance.id)) {
                vehicleMaintenance.lastUpdated = new Date()
                vehicleMaintenance.modifyBy = userName
                return vehicleMaintenance.merge(flush: true, failOnError: true)
            } else {
                vehicleMaintenance.createdBy = userName
                vehicleMaintenance.dateCreated = new Date()
                vehicleMaintenance = vehicleMaintenance.save(flush: true, failOnError: true)
                return vehicleMaintenance
            }
        }
        return null
    }

    VehicleMaintenance bootStrap(VehicleMaintenance vehicleMaintenance) {
        return createBase(vehicleMaintenance, Constants.ROOT)
    }

    VehicleMaintenance create(VehicleMaintenance vehicleMaintenance, LoginManager loginManager) {
        return createBase(vehicleMaintenance, loginManager.getUsername())
    }

    List<VehicleMaintenance> list(boolean enabled, int start, int size) {
        return VehicleMaintenance.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleMaintenance> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleMaintenance.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleMaintenance> list(String name, int size) {
        return VehicleMaintenance.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleMaintenance> list(boolean enabled) {
        return VehicleMaintenance.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleMaintenance> list(boolean enabled, boolean admin) {
        return VehicleMaintenance.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleMaintenance refresh(Long id) {
        VehicleMaintenance vehicleMaintenance = VehicleMaintenance.findById(id)
        return vehicleMaintenance
    }

    int count(boolean enabled) {
        return VehicleMaintenance.countByEnabled(enabled)
    }

    VehicleMaintenance delete(VehicleMaintenance vehicleMaintenance, LoginManager loginManager) {
        if (!vehicleMaintenance) {
            return null
        }
        vehicleMaintenance = VehicleMaintenance.findById(vehicleMaintenance.id)
        vehicleMaintenance.enabled = !vehicleMaintenance.enabled
        return create(vehicleMaintenance, loginManager)
    }

    VehicleMaintenance byId(Long id) {
        return VehicleMaintenance.findByIdAndEnabled(id, true)
    }

    VehicleMaintenance changePassword(VehicleMaintenance vehicleMaintenance, LoginManager loginManager) {
        return create(vehicleMaintenance, loginManager)
    }

    Long findMaxCode() {
        VehicleMaintenance vehicleMaintenance = VehicleMaintenance.withCriteria {
            order("id", "desc")
        }.find() as VehicleMaintenance
        if (!vehicleMaintenance) {
            return 0
        }
        return vehicleMaintenance.code
    }

}
