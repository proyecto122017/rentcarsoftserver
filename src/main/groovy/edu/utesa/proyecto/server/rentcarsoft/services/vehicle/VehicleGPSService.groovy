package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleGPS
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleGPSService")
@Transactional(rollbackFor = Exception.class)
class VehicleGPSService {

    private VehicleGPS createBase(VehicleGPS vehicleGPS, String userName) {
        if (vehicleGPS != null && !vehicleGPS.hasErrors()) {
            if (VehicleGPS.exists(vehicleGPS.id)) {
                vehicleGPS.lastUpdated = new Date()
                vehicleGPS.modifyBy = userName
                return vehicleGPS.merge(flush: true, failOnError: true)
            } else {
                vehicleGPS.createdBy = userName
                vehicleGPS.dateCreated = new Date()
                vehicleGPS = vehicleGPS.save(flush: true, failOnError: true)
                return vehicleGPS
            }
        }
        return null
    }

    VehicleGPS bootStrap(VehicleGPS vehicleGPS) {
        return createBase(vehicleGPS, Constants.ROOT)
    }

    VehicleGPS create(VehicleGPS vehicleGPS, LoginManager loginManager) {
        return createBase(vehicleGPS, loginManager.getUsername())
    }

    List<VehicleGPS> list(boolean enabled, int start, int size) {
        return VehicleGPS.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleGPS> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleGPS.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleGPS> list(String name, int size) {
        return VehicleGPS.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleGPS> list(boolean enabled) {
        return VehicleGPS.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleGPS> list(boolean enabled, boolean admin) {
        return VehicleGPS.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleGPS refresh(Long id) {
        VehicleGPS vehicleGPS = VehicleGPS.findById(id)
        return vehicleGPS
    }

    int count(boolean enabled) {
        return VehicleGPS.countByEnabled(enabled)
    }

    VehicleGPS delete(VehicleGPS vehicleGPS, LoginManager loginManager) {
        if (!vehicleGPS) {
            return null
        }
        vehicleGPS = VehicleGPS.findById(vehicleGPS.id)
        vehicleGPS.enabled = !vehicleGPS.enabled
        return create(vehicleGPS, loginManager)
    }

    VehicleGPS byEmail(String vehicleGPS) {
        return VehicleGPS.findByEmailAndEnabled(vehicleGPS, true)
    }

    VehicleGPS byId(Long id) {
        return VehicleGPS.findByIdAndEnabled(id, true)
    }

    Long findMaxCode() {
        VehicleGPS vehicleGPS = VehicleGPS.withCriteria {
            order("id", "desc")
        }.find() as VehicleGPS
        if (!vehicleGPS) {
            return 0
        }
        return vehicleGPS.code
    }

}
