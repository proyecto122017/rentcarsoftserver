package edu.utesa.proyecto.server.rentcarsoft.services.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Sector
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("sectorService")
@Transactional(rollbackFor = Exception.class)
class SectorService {

    private Sector createBase(Sector sector, String userName) {
        if (sector != null && !sector.hasErrors()) {
            if (Sector.exists(sector.id)) {
                sector.lastUpdated = new Date()
                sector.modifyBy = userName
                return sector.merge(flush: true, failOnError: true)
            } else {
                sector.createdBy = userName
                sector.dateCreated = new Date()
                sector = sector.save(flush: true, failOnError: true)
                return sector
            }
        }
        return null
    }

    Sector bootStrap(Sector sector) {
        return createBase(sector, Constants.ROOT)
    }

    Sector create(Sector sector, LoginManager loginManager) {
        return createBase(sector, loginManager.getUsername())
    }

    List<Sector> list(boolean enabled, int start, int size) {
        return Sector.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Sector> list(boolean enabled, boolean admin, int start, int size) {
        return Sector.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Sector> list(String name, int size) {
        return Sector.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Sector> list(boolean enabled) {
        return Sector.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Sector> list(boolean enabled, boolean admin) {
        return Sector.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Sector refresh(Long id) {
        Sector sector = Sector.findById(id)
        return sector
    }

    int count(boolean enabled) {
        return Sector.countByEnabled(enabled)
    }

    Sector delete(Sector sector, LoginManager loginManager) {
        if (!sector) {
            return null
        }
        sector = Sector.findById(sector.id)
        sector.enabled = !sector.enabled
        return create(sector, loginManager)
    }

    Sector byId(Long id) {
        return Sector.findByIdAndEnabled(id, true)
    }

    Sector byCode(int id) {
        return Sector.findByCodeAndEnabled(id, true)
    }

    Sector changePassword(Sector sector, LoginManager loginManager) {
        return create(sector, loginManager)
    }

    Long findMaxCode() {
        Sector sector = Sector.withCriteria {
            order("id", "desc")
        }.find() as Sector
        if (!sector) {
            return 0
        }
        return sector.code
    }

}
