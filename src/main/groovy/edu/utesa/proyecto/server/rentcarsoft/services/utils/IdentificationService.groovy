package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Identification
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("identificationService")
@Transactional(rollbackFor = Exception.class)
class IdentificationService {

    private Identification createBase(Identification identification, String userName) {
        if (identification != null && !identification.hasErrors()) {
            if (Identification.exists(identification.id)) {
                identification.lastUpdated = new Date()
                identification.modifyBy = userName
                return identification.merge(flush: true, failOnError: true)
            } else {
                identification.createdBy = userName
                identification.dateCreated = new Date()
                identification = identification.save(flush: true, failOnError: true)
                return identification
            }
        }
        return null
    }

    Identification bootStrap(Identification identification) {
        return createBase(identification, Constants.ROOT)
    }

    Identification create(Identification identification, LoginManager loginManager) {
        return createBase(identification, loginManager.getUsername())
    }

    List<Identification> list(boolean enabled, int start, int size) {
        return Identification.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Identification> list(boolean enabled, boolean admin, int start, int size) {
        return Identification.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Identification> list(String name, int size) {
        return Identification.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Identification> list(boolean enabled) {
        return Identification.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Identification> list(boolean enabled, boolean admin) {
        return Identification.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Identification refresh(Long id) {
        Identification identification = Identification.findById(id)
        return identification
    }

    int count(boolean enabled) {
        return Identification.countByEnabled(enabled)
    }

    Identification delete(Identification identification, LoginManager loginManager) {
        if (!identification) {
            return null
        }
        identification = Identification.findById(identification.id)
        identification.enabled = !identification.enabled
        return create(identification, loginManager)
    }

    Identification byEmail(String identification) {
        return Identification.findByEmailAndEnabled(identification, true)
    }

    Identification byId(Long id) {
        return Identification.findByIdAndEnabled(id, true)
    }

}
