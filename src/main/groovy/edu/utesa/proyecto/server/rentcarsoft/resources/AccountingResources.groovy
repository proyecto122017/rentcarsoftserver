package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.InvoiceHeader
import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting.InvoiceHeaderAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting.PaymentAPI
import edu.utesa.proyecto.server.rentcarsoft.services.accounting.InvoiceDetailRentService
import edu.utesa.proyecto.server.rentcarsoft.services.accounting.InvoiceHeaderService
import edu.utesa.proyecto.server.rentcarsoft.services.accounting.PaymentService
import edu.utesa.proyecto.server.rentcarsoft.services.utils.TypesService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/accounting")
@CompileStatic
class AccountingResources {

    static final SessionManager sessionManager = new SessionManager()

    @Autowired
    private InvoiceHeaderService invoiceHeaderService
    @Autowired
    private InvoiceDetailRentService invoiceDetailRentService
    @Autowired
    private PaymentService paymentService
    @Autowired
    private TypesService typesService

    @GET
    static String isUP() {
        return "OK"
    }

    @POST
    @Path('/invoices')
    String listInvoiceHeaders(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<InvoiceHeaderAPI> invoiceHeaderAPIS = new ArrayList<>()
        for (InvoiceHeader invoiceHeader : invoiceHeaderService.list(true)) {
            invoiceHeaderAPIS.add(invoiceHeader.toAPI())
        }
        return Constants.instance.stringify(invoiceHeaderAPIS)
    }

    @POST
    @Path('/create/payment')
    String createPayment(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        PaymentAPI paymentAPI = Constants.instance.convert(request, PaymentAPI.class)
        if (!paymentAPI) {
            // No parsea el objeto que enviaron
        }
        Payment payment = new Payment()
        payment.code =  paymentService.findMaxCode() + 1 as Integer
        payment.paymentDate = paymentAPI.paymentDate
        payment.paymentType = typesService.byCode(paymentAPI.code)
        payment.amount = paymentAPI.amount
        paymentService.bootStrap(payment)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/payments')
    String listPayments(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<PaymentAPI> paymentAPIS = new ArrayList<>()
        for (Payment payment : paymentService.list(true)) {
            paymentAPIS.add(payment.toAPI())
        }
        return Constants.instance.stringify(paymentAPIS)
    }
}
