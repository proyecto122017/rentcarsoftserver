package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBrand
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleModel
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleBrandService
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleModelService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.json.VehicleBrandJson
import edu.utesa.proyecto.server.rentcarsoft.utils.json.VehicleModelBrandJson
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicVehicleBrand {

    @Autowired
    private VehicleBrandService vehicleBrandService
    @Autowired
    private VehicleModelService vehicleModelService

    Data data
    DataModel datamodel

    BasicVehicleBrand() {
        Constants.instance.autoWiredClass(this)
        data = Constants.instance.convert(Constants.instance.readJsonFile("vehicleBrand"), Data.class)
        datamodel = Constants.instance.convert(Constants.instance.readJsonFile("vehicleModel"), DataModel.class)
    }

    void insert() {
        for (VehicleModelBrandJson brandJson : datamodel.data) {
            for (VehicleBrandJson vehicleBrandJson : data.data) {
                if (vehicleBrandJson.code.equals(brandJson.id_marca)) {
                    createVehicleBrandFromExcel(vehicleBrandJson, brandJson.code)
                }
            }
        }

    }

    void insertModel() {
        for (VehicleModelBrandJson vehicleBrandJson : datamodel.data) {
            VehicleBrand vehicleBrand = vehicleBrandService.byCode(vehicleBrandJson.id_marca)
            if (vehicleBrand) {
                vehicleBrand.code = vehicleBrand.code
                vehicleBrand.description = vehicleBrand.description
                vehicleBrand.vehicleModel = vehicleModelService.byCode(vehicleBrandJson.code)
                vehicleBrandService.bootStrap(vehicleBrand)
            }
        }
    }

    private VehicleBrand create(int code, String name, VehicleModel vehicleModel) {
        VehicleBrand vehicleBrand = vehicleBrandService.byCode(code)
        if (!vehicleBrand) {
            vehicleBrand = new VehicleBrand()
            vehicleBrand.code = code
            vehicleBrand.description = name
            vehicleBrand.vehicleModel = vehicleModel
            return vehicleBrandService.bootStrap(vehicleBrand)
        }
        return vehicleBrand
    }

    private VehicleBrand createVehicleBrandFromExcel(VehicleBrandJson vehicleBrandJson, int vehicleModel) {
        VehicleBrand vehicleBrand = vehicleBrandService.byCode(vehicleBrandJson.code)
        if (!vehicleBrand) {
            vehicleBrand = new VehicleBrand()
            vehicleBrand.code = vehicleBrandJson.code
            vehicleBrand.description = vehicleBrandJson.description
            vehicleBrand.vehicleModel = vehicleModelService.byCode(vehicleModel)
            return vehicleBrandService.bootStrap(vehicleBrand)
        }
        return vehicleBrand
    }

    static class Data {
        List<VehicleBrandJson> data
    }

    static class DataModel {
        List<VehicleModelBrandJson> data
    }
}
