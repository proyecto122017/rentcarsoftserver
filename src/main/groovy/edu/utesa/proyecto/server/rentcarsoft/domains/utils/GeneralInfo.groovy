package edu.utesa.proyecto.server.rentcarsoft.domains.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Contact
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Address
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.GeneralInfoAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by Desckop on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class GeneralInfo implements APIConverter<GeneralInfoAPI> {

    int code
    String name
    Types type
    Identification identification

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        type nullable: true
        identification nullable: true
    }

    static hasMany = [listContacts: Contact, listEmails: Email, listPhones: Phone,
                      listAddress : Address]

    static mapping = {
        table "general_info"

        type fetch: "join"
        identification fetch: "join"

        listContacts lazy: false
        listEmails lazy: false
        listPhones lazy: false
        listAddress lazy: false
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((GeneralInfo) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    GeneralInfoAPI toAPI() {
        GeneralInfoAPI generalInfoAPI = new GeneralInfoAPI()
        generalInfoAPI.code = code
        generalInfoAPI.name = name
        if (type) {
            generalInfoAPI.type = type.toAPI()
        }
        if (identification) {
            generalInfoAPI.identification = identification.toAPI()
        }
        generalInfoAPI.emailList = new ArrayList<>()
        if (listEmails.size() > 0) {
            for (Email email : listEmails) {
                generalInfoAPI.emailList.add(email.toAPI())
            }
        }
        generalInfoAPI.contactList = new ArrayList<>()
        if (listContacts.size() > 0) {
            for (Contact contact : listContacts) {
                generalInfoAPI.contactList.add(contact.toAPI())
            }
        }
        generalInfoAPI.addressList = new ArrayList<>()
        if (listAddress.size() > 0) {
            for (Address address : listAddress) {
                generalInfoAPI.addressList.add(address.toAPI())
            }
        }
        generalInfoAPI.phoneList = new ArrayList<>()
        if (listPhones.size() > 0) {
            for (Phone phone : listPhones) {
                generalInfoAPI.phoneList.add(phone.toAPI())
            }
        }
        return generalInfoAPI
    }
}
