package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.administration

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.BranchOffice
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Coordinates
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.services.administrator.BranchOfficeService
import edu.utesa.proyecto.server.rentcarsoft.services.utils.TypesService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location.BasicCoordinates
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils.BasicGeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils.BasicTypes
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle.BasicVehicleBranchOffice
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicBranchOffice {

    @Autowired
    private BranchOfficeService branchOfficeService
    @Autowired
    private TypesService typesService

    BranchOffice principal
    BranchOffice sucursal

    BasicBranchOffice() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicGeneralInfo basicGeneralInfo, BasicCoordinates basicCoordinates, BasicTypes basicTypes, BasicVehicleBranchOffice basicVehicleBranchOffice) {
        List<VehicleBranchOffice> vehicleBranchOfficeListPrincipal = new ArrayList<>()
        vehicleBranchOfficeListPrincipal.add(basicVehicleBranchOffice.vehicleBranchOffice)
        vehicleBranchOfficeListPrincipal.add(basicVehicleBranchOffice.vehicleBranchOffice2)
        List<VehicleBranchOffice> vehicleBranchOfficeListSucursal = new ArrayList<>()
        vehicleBranchOfficeListSucursal.add(basicVehicleBranchOffice.vehicleBranchOffice1)
        vehicleBranchOfficeListSucursal.add(basicVehicleBranchOffice.vehicleBranchOffice3)

        principal = create(1, basicCoordinates.casa, basicGeneralInfo.ronaldEnterprise, basicTypes.typesOficinaPrincipal, vehicleBranchOfficeListPrincipal)
        sucursal = create(2, basicCoordinates.trabajo, basicGeneralInfo.sandyEnterprise, basicTypes.typesOficinaSucursal, vehicleBranchOfficeListSucursal)
    }

    private BranchOffice create(int code, Coordinates coordinates, GeneralInfo generalInfo, Types branchOfficeType,
                                List<VehicleBranchOffice> vehicleBranchOfficeList) {
        BranchOffice branchOffice = branchOfficeService.byCode(code)
        if (!branchOffice) {
            branchOffice = new BranchOffice()
            branchOffice.code = code
            branchOffice.coordinates = coordinates
            branchOffice.generalInfo = generalInfo
            branchOffice.branchOfficeType = branchOfficeType
            branchOffice.listVehicleBrandOffice = vehicleBranchOfficeList.toSet()
            return branchOfficeService.bootStrap(branchOffice)
        }
        return branchOffice
    }
}
