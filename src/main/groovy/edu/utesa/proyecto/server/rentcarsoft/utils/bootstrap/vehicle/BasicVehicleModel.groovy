package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleModel
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleModelService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.json.VehicleModelJson
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicVehicleModel {

    @Autowired
    private VehicleModelService vehicleModelService
    Data data

    BasicVehicleModel() {
        Constants.instance.autoWiredClass(this)
        data = Constants.instance.convert(Constants.instance.readJsonFile("vehicleModel"), Data.class)
    }

    void insert() {
        for (VehicleModelJson vehicleBrandJson : data.data) {
            createVehicleBrandFromExcel(vehicleBrandJson, null)
        }
    }

    private VehicleModel create(int code, String name) {
        VehicleModel vehicleModel = vehicleModelService.byCode(code)
        if (!vehicleModel) {
            vehicleModel = new VehicleModel()
            vehicleModel.code = code
            vehicleModel.description = name
            return vehicleModelService.bootStrap(vehicleModel)
        }
        return vehicleModel
    }

    private VehicleModel createVehicleBrandFromExcel(VehicleModelJson vehicleBrandJson, VehicleModel vehicleModel) {
        VehicleModel vehicleBrand = vehicleModelService.byCode(vehicleBrandJson.code)
        if (!vehicleBrand) {
            vehicleBrand = new VehicleModel()
            vehicleBrand.code = vehicleBrandJson.code
            vehicleBrand.description = vehicleBrandJson.description
            return vehicleModelService.bootStrap(vehicleBrand)
        }
        return vehicleBrand
    }

    static class Data {
        List<VehicleModelJson> data
    }
}
