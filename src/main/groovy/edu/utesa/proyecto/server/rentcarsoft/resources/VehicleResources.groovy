package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.*
import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.*
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.*
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/vehicle")
@CompileStatic
class VehicleResources {

    static final SessionManager sessionManager = new SessionManager()

    @Autowired
    private VehicleService vehicleService
    @Autowired
    private VehicleBranchOfficeService vehicleBranchOfficeService
    @Autowired
    private VehicleBrandService vehicleBrandService
    @Autowired
    private VehicleColorService vehicleColorService
    @Autowired
    private VehicleFeacturesService vehicleFeacturesService
    @Autowired
    private VehicleGPSService vehicleGPSService
    @Autowired
    private VehicleModelService vehicleModelService
    @Autowired
    private VehicleVersionService vehicleVersionService

    @POST
    static String isUP() {
        return "OK"
    }

    @POST
    @Path('/create/vehicle')
    String createVehicle(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleAPI vehicleAPI = Constants.instance.convert(request, VehicleAPI.class)
        if (!vehicleAPI) {
            // No parsea el objeto que enviaron
        }
        Vehicle vehicle = new Vehicle()
        vehicle.code = vehicleService.findMaxCode() + 1 as Integer
        vehicle.vehicleFeactures = vehicleAPI.vehicleFeactures as VehicleFeactures
        vehicle.vehicleBrand = vehicleAPI.vehicleBrand as VehicleBrand
        vehicle.vehicleType = vehicleAPI.vehicleType as Types
        vehicleService.bootStrap(vehicle)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/listVehicles')
    String listVehicles(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleAPI> vehicleAPIS = new ArrayList<>()
        for (Vehicle vehicle : vehicleService.list(true)) {
            println "klk"
            println "klk"
            vehicleAPIS.add(vehicle.toAPI())
        }
        return Constants.instance.stringify(vehicleAPIS)
    }

    @POST
    @Path('/vehicleBranchOffices')
    String listVehicleBranchOffices(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleBranchOfficeAPI> vehicleBranchOfficeAPIS = new ArrayList<>()
        for (VehicleBranchOffice vehicleBranchOffice : vehicleBranchOfficeService.list(true)) {
            vehicleBranchOfficeAPIS.add(vehicleBranchOffice.toAPI())
        }
        return Constants.instance.stringify(vehicleBranchOfficeAPIS)
    }

    @POST
    @Path('/create/vehicleBranchOffice')
    String createVehicleBranchOffice(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleBranchOfficeAPI vehicleBranchOfficeAPI = Constants.instance.convert(request, VehicleBranchOfficeAPI.class)
        if (!vehicleBranchOfficeAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleBranchOffice vehicleBranchOffice = new VehicleBranchOffice()
        vehicleBranchOffice.code = vehicleBranchOfficeService.findMaxCode() + 1 as Integer
        vehicleBranchOffice.chassis = vehicleBranchOfficeAPI.chassis
        vehicleBranchOffice.year = vehicleBranchOfficeAPI.year
        vehicleBranchOffice.vehicle = vehicleBranchOfficeAPI.vehicle as Vehicle
        vehicleBranchOffice.vehicleColor = vehicleBranchOfficeAPI.vehicleColor as VehicleColor
        vehicleBranchOffice.vehicleGPS = vehicleBranchOfficeAPI.vehicleGPS as VehicleGPS
        vehicleBranchOffice.vehicleVersion = vehicleBranchOfficeAPI.vehicleVersion as VehicleVersion
        vehicleBranchOffice.price = vehicleBranchOfficeAPI.price
        vehicleBranchOfficeService.bootStrap(vehicleBranchOffice)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/vehicleBrands')
    String listVehicleBrands(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleBrandAPI> vehicleBrandAPIS = new ArrayList<>()
        for (VehicleBrand vehicleBrand : vehicleBrandService.list(true)) {
            vehicleBrandAPIS.add(vehicleBrand.toAPI())
        }
        return Constants.instance.stringify(vehicleBrandAPIS)
    }

    @POST
    @Path('/create/vehicleBrand')
    String createVehicleBrand(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleBrandAPI vehicleBrandAPI = Constants.instance.convert(request, VehicleBrandAPI.class)
        if (!vehicleBrandAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleBrand vehicleBrand = new VehicleBrand()
        vehicleBrand.code = vehicleBrandService.findMaxCode() + 1 as Integer
        vehicleBrand.description = vehicleBrandAPI.description
        vehicleBrand.vehicleModel = vehicleBrandAPI.vehicleModel as VehicleModel
        vehicleBrandService.bootStrap(vehicleBrand)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/vehicleColors')
    String listVehicleColors(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleColorAPI> vehicleColorAPIS = new ArrayList<>()
        for (VehicleColor vehicleColor : vehicleColorService.list(true)) {
            vehicleColorAPIS.add(vehicleColor.toAPI())
        }
        return Constants.instance.stringify(vehicleColorAPIS)
    }

    @POST
    @Path('/create/vehicleColor')
    String createVehicleColor(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleColorAPI vehicleColorAPI = Constants.instance.convert(request, VehicleColorAPI.class)
        if (!vehicleColorAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleColor vehicleColor = new VehicleColor()
        vehicleColor.code = vehicleColorService.findMaxCode() + 1 as Integer
        vehicleColor.color = vehicleColorAPI.description
        vehicleColorService.bootStrap(vehicleColor)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/vehicleFeactures')
    String listVehicleFeacturess(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleFeacturesAPI> vehicleFeacturesAPIS = new ArrayList<>()
        for (VehicleFeactures vehicleFeactures : vehicleFeacturesService.list(true)) {
            vehicleFeacturesAPIS.add(vehicleFeactures.toAPI())
        }
        return Constants.instance.stringify(vehicleFeacturesAPIS)
    }

    @POST
    @Path('/create/vehicleFeacture')
    String createVehicleFeactures(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleFeacturesAPI vehicleFeacturesAPI = Constants.instance.convert(request, VehicleFeacturesAPI.class)
        if (!vehicleFeacturesAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleFeactures vehicleFeactures = new VehicleFeactures()
        vehicleFeactures.code = vehicleFeacturesService.findMaxCode() + 1 as Integer
        vehicleFeactures.description = vehicleFeacturesAPI.description
        vehicleFeacturesService.bootStrap(vehicleFeactures)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/vehicleGPSs')
    String listVehicleGPSs(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleGPSAPI> vehicleGPSAPIS = new ArrayList<>()
        for (VehicleGPS vehicleGPS : vehicleGPSService.list(true)) {
            vehicleGPSAPIS.add(vehicleGPS.toAPI())
        }
        return Constants.instance.stringify(vehicleGPSAPIS)
    }

    @POST
    @Path('/create/vehicleGPS')
    String createVehicleGPS(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleGPSAPI vehicleGPSAPI = Constants.instance.convert(request, VehicleGPSAPI.class)
        if (!vehicleGPSAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleGPS vehicleGPS = new VehicleGPS()
        vehicleGPS.code = vehicleGPSService.findMaxCode() + 1 as Integer
        vehicleGPS.number = vehicleGPSAPI.number
        vehicleGPS.serial = vehicleGPSAPI.serial
        vehicleGPS.balance = vehicleGPSAPI.balance
        vehicleGPSService.bootStrap(vehicleGPS)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/vehicleModels')
    String listVehicleModels(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleModelAPI> vehicleModelAPIS = new ArrayList<>()
        for (VehicleModel vehicleModel : vehicleModelService.list(true)) {
            vehicleModelAPIS.add(vehicleModel.toAPI())
        }
        String data = Constants.instance.stringify(vehicleModelAPIS)
        return data
    }

    @POST
    @Path('/create/vehicleModel')
    String createVehicleModel(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleModelAPI vehicleModelAPI = Constants.instance.convert(request, VehicleModelAPI.class)
        if (!vehicleModelAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleModel vehicleModel = new VehicleModel()
        vehicleModel.code = vehicleModelService.findMaxCode() + 1 as Integer
        vehicleModel.description = vehicleModelAPI.description
        vehicleModelService.bootStrap(vehicleModel)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/vehicleVersions')
    String listVehicleVersion(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleVersionAPI> vehicleVersionAPIS = new ArrayList<>()
        for (VehicleVersion vehicleVersion : vehicleVersionService.list(true)) {
            vehicleVersionAPIS.add(vehicleVersion.toAPI())
        }
        return Constants.instance.stringify(vehicleVersionAPIS)
    }

    @POST
    @Path('/create/vehicleVersion')
    String createVehicleVersion(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleVersionAPI vehicleVersionAPI = Constants.instance.convert(request, VehicleVersionAPI.class)
        if (!vehicleVersionAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleVersion vehicleVersion = new VehicleVersion()
        vehicleVersion.code = vehicleVersionService.findMaxCode() + 1 as Integer
        vehicleVersion.description = vehicleVersionAPI.description
        vehicleVersionService.bootStrap(vehicleVersion)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }
}
