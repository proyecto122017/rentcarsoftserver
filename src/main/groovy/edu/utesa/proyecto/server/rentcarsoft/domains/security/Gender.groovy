package edu.utesa.proyecto.server.rentcarsoft.domains.security

import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.GenderAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Gender implements APIConverter<GenderAPI> {

    int code
    String description

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
    }

    static mapping = {
        table "gender"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Gender) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    String toString() {
        return description
    }

    @Override
    GenderAPI toAPI() {
        GenderAPI genderAPI = new GenderAPI()
        genderAPI.code = code
        genderAPI.description = description
        return genderAPI
    }
}
