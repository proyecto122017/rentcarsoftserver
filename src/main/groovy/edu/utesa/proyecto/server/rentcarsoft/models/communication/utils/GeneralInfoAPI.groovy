package edu.utesa.proyecto.server.rentcarsoft.models.communication.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Contact
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Address
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Email
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Identification
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Phone
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.administration.ContactAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.AddressAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class GeneralInfoAPI {

    int code
    String name
    TypesAPI type
    IdentificationAPI identification
    List<ContactAPI> contactList
    List<EmailAPI> emailList
    List<PhoneAPI> phoneList
    List<AddressAPI> addressList

}
