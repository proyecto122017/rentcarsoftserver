package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Municipality
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Province
import edu.utesa.proyecto.server.rentcarsoft.services.location.ProvinceService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicProvince {

    @Autowired
    private ProvinceService provinceService

    Province santiago
    Province lavega
    Province puertoPlata
    Province hermanasMirabal

    BasicProvince() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicMunicipality basicMunicipality) {
        santiago = create(1, "Santiago de los 30 Caballerso", basicMunicipality.santiago)
        lavega = create(2, "La Vega", basicMunicipality.santiago)
        puertoPlata = create(3, "Puerto Plata", basicMunicipality.santiago)
        hermanasMirabal = create(4, "Hermanas Mirabal", basicMunicipality.santiago)
    }

    private Province create(int code, String name, Municipality municipality) {
        Province city = provinceService.byCode(code)
        if (!city) {
            city = new Province()
            city.code = code
            city.description = name
            city.municipality = municipality
            return provinceService.bootStrap(city)
        }
        return city
    }
}
