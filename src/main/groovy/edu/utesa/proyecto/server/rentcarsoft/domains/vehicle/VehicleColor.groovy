package edu.utesa.proyecto.server.rentcarsoft.domains.vehicle

import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleColorAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class VehicleColor implements APIConverter<VehicleColorAPI> {

    int code
    String color

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
    }

    static mapping = {
        table "vehicle_color"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((VehicleColor) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    VehicleColorAPI toAPI() {
        VehicleColorAPI vehicleColorAPI = new VehicleColorAPI()
        vehicleColorAPI.code = code
        vehicleColorAPI.description = color
        return vehicleColorAPI
    }
}
