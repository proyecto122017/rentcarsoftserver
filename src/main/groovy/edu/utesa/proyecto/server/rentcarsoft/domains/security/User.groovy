package edu.utesa.proyecto.server.rentcarsoft.domains.security

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Person
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.UserAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class User implements APIConverter<UserAPI> {

    boolean admin = false // Dice si es el dueño de la cuenta.

    int code
    String username
    String password // Esto es sha256
    String name

    Date registerDate
    Date ingressDate
    Person person

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [listPermissions: Permission]

    static constraints = {
        code unique: true
        username unique: true, nullable: false

        person nullable: true
        registerDate nullable: true
        ingressDate nullable: true

    }

    static mapping = {
        table "users"

        person fetch: "join"
        listPermissions lazy: false

        username column: 'username', index: 'user_user_pass_idx'
        password column: 'password', index: 'user_user_pass_idx'

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((User) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }


    @Override
    UserAPI toAPI() {
        UserAPI userAPI = new UserAPI()
        userAPI.code = code
        userAPI.username = username
        userAPI.password = password
        userAPI.registerDate = registerDate
        userAPI.ingressDate = ingressDate
        if (person) {
            userAPI.person = person.toAPI()
        }
        userAPI.permissionList = new ArrayList<>()
        if (listPermissions.size() > 0) {
            for (Permission permission : listPermissions) {
                userAPI.permissionList.add(permission.toAPI())
            }
        }
        return userAPI
    }

    @Override
    String toString() {
        return username
    }

    String toStringEntity() {
        return name
    }
}
