package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Municipality
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Sector
import edu.utesa.proyecto.server.rentcarsoft.services.location.MunicipalityService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicMunicipality {

    @Autowired
    private MunicipalityService municipalityService

    Municipality santiago
    Municipality punal
    Municipality lavega
    Municipality maimon
    Municipality salcedo

    BasicMunicipality() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicSector basicSector) {
        santiago = create(1, "santiago", basicSector.gurabo)
        punal = create(2, "punal", basicSector.sabaneta)
        lavega = create(3, "lavega", basicSector.dosa)
        maimon = create(4, "maimon", basicSector.maimon)
        salcedo = create(5, "salcedo", basicSector.paloverde)
    }

    private Municipality create(int code, String name, Sector sector) {
        Municipality city = municipalityService.byCode(code)
        if (!city) {
            city = new Municipality()
            city.code = code
            city.description = name
            city.sector = sector
            return municipalityService.bootStrap(city)
        }
        return city
    }
}
