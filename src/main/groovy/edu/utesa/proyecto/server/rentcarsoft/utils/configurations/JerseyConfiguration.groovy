package edu.utesa.proyecto.server.rentcarsoft.utils.configurations

import groovy.transform.CompileStatic
import org.glassfish.jersey.server.ResourceConfig
import org.springframework.stereotype.Component

import javax.ws.rs.ApplicationPath

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings("GroovyUnusedDeclaration")
@Component
@ApplicationPath("/api")
@CompileStatic
class JerseyConfiguration extends ResourceConfig {

    JerseyConfiguration() {
        packages("edu.utesa.proyecto.server.rentcarsoft.resources")
    }
}
