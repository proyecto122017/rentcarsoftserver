package edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBrand
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleFeactures
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.TypesAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class VehicleAPI {

    int code
    TypesAPI vehicleType
    VehicleBrandAPI vehicleBrand
    VehicleFeacturesAPI vehicleFeactures

}
