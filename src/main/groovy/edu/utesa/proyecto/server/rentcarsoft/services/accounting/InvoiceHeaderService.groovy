package edu.utesa.proyecto.server.rentcarsoft.services.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.InvoiceHeader
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("invoiceHeaderService")
@Transactional(rollbackFor = Exception.class)
class InvoiceHeaderService {

    private InvoiceHeader createBase(InvoiceHeader invoiceHeader, String userName) {
        if (invoiceHeader != null && !invoiceHeader.hasErrors()) {
            if (InvoiceHeader.exists(invoiceHeader.id)) {
                invoiceHeader.lastUpdated = new Date()
                invoiceHeader.modifyBy = userName
                return invoiceHeader.merge(flush: true, failOnError: true)
            } else {
                invoiceHeader.createdBy = userName
                invoiceHeader.dateCreated = new Date()
                invoiceHeader = invoiceHeader.save(flush: true, failOnError: true)
                return invoiceHeader
            }
        }
        return null
    }

    InvoiceHeader bootStrap(InvoiceHeader invoiceHeader) {
        return createBase(invoiceHeader, Constants.ROOT)
    }

    InvoiceHeader create(InvoiceHeader invoiceHeader, LoginManager loginManager) {
        return createBase(invoiceHeader, loginManager.getUsername())
    }

    List<InvoiceHeader> list(boolean enabled, int start, int size) {
        return InvoiceHeader.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<InvoiceHeader> list(boolean enabled, boolean admin, int start, int size) {
        return InvoiceHeader.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<InvoiceHeader> list(String name, int size) {
        return InvoiceHeader.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<InvoiceHeader> list(boolean enabled) {
        return InvoiceHeader.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<InvoiceHeader> list(boolean enabled, boolean admin) {
        return InvoiceHeader.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    InvoiceHeader refresh(Long id) {
        InvoiceHeader invoiceHeader = InvoiceHeader.findById(id)
        return invoiceHeader
    }

    int count(boolean enabled) {
        return InvoiceHeader.countByEnabled(enabled)
    }

    InvoiceHeader delete(InvoiceHeader invoiceHeader, LoginManager loginManager) {
        if (!invoiceHeader) {
            return null
        }
        invoiceHeader = InvoiceHeader.findById(invoiceHeader.id)
        invoiceHeader.enabled = !invoiceHeader.enabled
        return create(invoiceHeader, loginManager)
    }

    InvoiceHeader byId(Long id) {
        return InvoiceHeader.findByIdAndEnabled(id, true)
    }

    Long findMaxCode() {
        InvoiceHeader invoiceHeader = InvoiceHeader.withCriteria {
            order("id", "desc")
        }.find() as InvoiceHeader
        if (!invoiceHeader) {
            return 0
        }
        return invoiceHeader.code
    }

}
