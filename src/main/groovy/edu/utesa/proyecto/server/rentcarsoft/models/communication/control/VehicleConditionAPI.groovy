package edu.utesa.proyecto.server.rentcarsoft.models.communication.control

import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class VehicleConditionAPI {

    int code
    String description

}
