package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Rental
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("rentalService")
@Transactional(rollbackFor = Exception.class)
class RentalService {

    private Rental createBase(Rental rental, String userName) {
        if (rental != null && !rental.hasErrors()) {
            if (Rental.exists(rental.id)) {
                rental.lastUpdated = new Date()
                rental.modifyBy = userName
                return rental.merge(flush: true, failOnError: true)
            } else {
                rental.createdBy = userName
                rental.dateCreated = new Date()
                rental = rental.save(flush: true, failOnError: true)
                return rental
            }
        }
        return null
    }

    Rental bootStrap(Rental rental) {
        return createBase(rental, Constants.ROOT)
    }

    Rental create(Rental rental, LoginManager loginManager) {
        return createBase(rental, loginManager.getUsername())
    }

    List<Rental> list(boolean enabled, int start, int size) {
        return Rental.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Rental> list(boolean enabled, boolean admin, int start, int size) {
        return Rental.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Rental> list(String name, int size) {
        return Rental.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Rental> list(boolean enabled) {
        return Rental.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Rental> list(boolean enabled, boolean admin) {
        return Rental.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Rental refresh(Long id) {
        Rental rental = Rental.findById(id)
        return rental
    }

    int count(boolean enabled) {
        return Rental.countByEnabled(enabled)
    }

    Rental delete(Rental rental, LoginManager loginManager) {
        if (!rental) {
            return null
        }
        rental = Rental.findById(rental.id)
        rental.enabled = !rental.enabled
        return create(rental, loginManager)
    }

    Rental byId(Long id) {
        return Rental.findByIdAndEnabled(id, true)
    }

    Rental changePassword(Rental rental, LoginManager loginManager) {
        return create(rental, loginManager)
    }

    Long findMaxCode() {
        Rental rental = Rental.withCriteria {
            order("id", "desc")
        }.find() as Rental
        if (!rental) {
            return 0
        }
        return rental.code
    }

}
