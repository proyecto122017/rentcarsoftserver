package edu.utesa.proyecto.server.rentcarsoft.models.communication.utils

import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class PhotosAPI {

    String description
    String photo

}
