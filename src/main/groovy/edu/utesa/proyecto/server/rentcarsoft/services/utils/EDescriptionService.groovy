package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.EDescription
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("eDescriptionService")
@Transactional(rollbackFor = Exception.class)
class EDescriptionService {

    private EDescription createBase(EDescription eDescription, String userName) {
        if (eDescription != null && !eDescription.hasErrors()) {
            if (EDescription.exists(eDescription.id)) {
                eDescription.lastUpdated = new Date()
                eDescription.modifyBy = userName
                return eDescription.merge(flush: true, failOnError: true)
            } else {
                eDescription.createdBy = userName
                eDescription.dateCreated = new Date()
                eDescription = eDescription.save(flush: true, failOnError: true)
                return eDescription
            }
        }
        return null
    }

    EDescription bootStrap(EDescription eDescription) {
        return createBase(eDescription, Constants.ROOT)
    }

    EDescription create(EDescription eDescription, LoginManager loginManager) {
        return createBase(eDescription, loginManager.getUsername())
    }

    List<EDescription> list(boolean enabled, int start, int size) {
        return EDescription.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<EDescription> list(boolean enabled, boolean admin, int start, int size) {
        return EDescription.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<EDescription> list(String name, int size) {
        return EDescription.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<EDescription> list(boolean enabled) {
        return EDescription.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<EDescription> list(boolean enabled, boolean admin) {
        return EDescription.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    EDescription refresh(Long id) {
        EDescription eDescription = EDescription.findById(id)
        return eDescription
    }

    int count(boolean enabled) {
        return EDescription.countByEnabled(enabled)
    }

    EDescription delete(EDescription eDescription, LoginManager loginManager) {
        if (!eDescription) {
            return null
        }
        eDescription = EDescription.findById(eDescription.id)
        eDescription.enabled = !eDescription.enabled
        return create(eDescription, loginManager)
    }

    EDescription byId(Long id) {
        return EDescription.findByIdAndEnabled(id, true)
    }

    EDescription byDescription(String name) {
        return EDescription.findByDescriptionAndEnabled(name, true)
    }

    EDescription changePassword(EDescription eDescription, LoginManager loginManager) {
        return create(eDescription, loginManager)
    }

    Long findMaxCode() {
        EDescription eDescription = EDescription.withCriteria {
            order("id", "desc")
        }.find() as EDescription
        if (!eDescription) {
            return 0
        }
        return eDescription.code
    }

}
