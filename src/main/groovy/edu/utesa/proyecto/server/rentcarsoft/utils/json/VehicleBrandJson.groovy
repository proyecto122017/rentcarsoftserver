package edu.utesa.proyecto.server.rentcarsoft.utils.json

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleModel
import groovy.transform.CompileStatic

/**
 *  Created by ronald on 6/12/17.
 */
@CompileStatic
class VehicleBrandJson {

    int code
    String description
    VehicleModel vehicleModel
}
