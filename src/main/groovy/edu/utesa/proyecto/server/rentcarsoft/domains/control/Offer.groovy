package edu.utesa.proyecto.server.rentcarsoft.domains.control

import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.OfferAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Offer implements APIConverter<OfferAPI> {

    int code
    String description

    boolean original = true

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true

    }

    static mapping = {
        table "offers"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Offer) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    OfferAPI toAPI() {
        OfferAPI offerAPI = new OfferAPI()
        offerAPI.code = code
        offerAPI.description = description
        return offerAPI
    }
}
