package edu.utesa.proyecto.server.rentcarsoft.domains.location

import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.CoordinatesAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Coordinates implements APIConverter<CoordinatesAPI> {

    int code

    BigDecimal latitude
    BigDecimal longitude

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        latitude nullable: true
        longitude nullable: true
    }

    static mapping = {
        table "coordinates"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Coordinates) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    CoordinatesAPI toAPI() {
        CoordinatesAPI coordinatesAPI = new CoordinatesAPI()
        coordinatesAPI.code = code
        coordinatesAPI.latitude = latitude
        coordinatesAPI.longitude = longitude
        return coordinatesAPI
    }
}
