package edu.utesa.proyecto.server.rentcarsoft.domains.vehicle

import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleBrandAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class VehicleBrand implements APIConverter<VehicleBrandAPI> {

    int code
    String description
    VehicleModel vehicleModel

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        description nullable: true
        vehicleModel nullable: true
    }

    static mapping = {
        table "vehicle_brands"
        vehicleModel fetch: 'join'
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((VehicleBrand) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    VehicleBrandAPI toAPI() {
        VehicleBrandAPI vehicleBrandAPI = new VehicleBrandAPI()
        vehicleBrandAPI.code = code
        vehicleBrandAPI.description = description
        if (vehicleModel) {
            vehicleBrandAPI.vehicleModel = vehicleModel.toAPI()
        }
        return vehicleBrandAPI
    }
}
