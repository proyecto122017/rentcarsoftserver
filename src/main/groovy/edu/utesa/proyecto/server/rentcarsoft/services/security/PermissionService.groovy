package edu.utesa.proyecto.server.rentcarsoft.services.security

import edu.utesa.proyecto.server.rentcarsoft.domains.security.Permission
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("permissionService")
@Transactional(rollbackFor = Exception.class)
class PermissionService {

    private Permission createBase(Permission permission, String userName) {
        if (permission != null && !permission.hasErrors()) {
            if (Permission.exists(permission.id)) {
                permission.lastUpdated = new Date()
                permission.modifyBy = userName
                return permission.merge(flush: true, failOnError: true)
            } else {
                permission.createdBy = userName
                permission.dateCreated = new Date()
                permission = permission.save(flush: true, failOnError: true)
                return permission
            }
        }
        return null
    }

    Permission bootStrap(Permission permission) {
        return createBase(permission, Constants.ROOT)
    }

    Permission create(Permission permission, LoginManager loginManager) {
        return createBase(permission, loginManager.getUsername())
    }

    List<Permission> list(boolean enabled, int start, int size) {
        return Permission.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Permission> list(boolean enabled, boolean admin, int start, int size) {
        return Permission.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Permission> list(String name, int size) {
        return Permission.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Permission> list(boolean enabled) {
        return Permission.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Permission> list(boolean enabled, boolean admin) {
        return Permission.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Permission refresh(Long id) {
        Permission permission = Permission.findById(id)
        return permission
    }

    int count(boolean enabled) {
        return Permission.countByEnabled(enabled)
    }

    Permission delete(Permission permission, LoginManager loginManager) {
        if (!permission) {
            return null
        }
        permission = Permission.findById(permission.id)
        permission.enabled = !permission.enabled
        return create(permission, loginManager)
    }

    Permission byId(Long id) {
        return Permission.findByIdAndEnabled(id, true)
    }

    Permission changePassword(Permission permission, LoginManager loginManager) {
        return create(permission, loginManager)
    }

    Long findMaxCode() {
        Permission permission = Permission.withCriteria {
            order("id", "desc")
        }.find() as Permission
        if (!permission) {
            return 0
        }
        return permission.code
    }

}
