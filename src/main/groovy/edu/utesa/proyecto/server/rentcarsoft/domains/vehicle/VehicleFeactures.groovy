package edu.utesa.proyecto.server.rentcarsoft.domains.vehicle

import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleFeacturesAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by Desckop on 9/6/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class VehicleFeactures implements APIConverter<VehicleFeacturesAPI> {

    int code
    String description

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
    }

    static mapping = {
        table "vehicle_features"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((VehicleFeactures) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    VehicleFeacturesAPI toAPI() {
        VehicleFeacturesAPI vehicleFeacturesAPI = new VehicleFeacturesAPI()
        vehicleFeacturesAPI.code = code
        vehicleFeacturesAPI.description = description
        return vehicleFeacturesAPI
    }
}
