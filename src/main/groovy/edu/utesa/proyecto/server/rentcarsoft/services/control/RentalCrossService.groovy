package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.RentalCross
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("rentalCrossService")
@Transactional(rollbackFor = Exception.class)
class RentalCrossService {

    private RentalCross createBase(RentalCross rentalCross, String userName) {
        if (rentalCross != null && !rentalCross.hasErrors()) {
            if (RentalCross.exists(rentalCross.id)) {
                rentalCross.lastUpdated = new Date()
                rentalCross.modifyBy = userName
                return rentalCross.merge(flush: true, failOnError: true)
            } else {
                rentalCross.createdBy = userName
                rentalCross.dateCreated = new Date()
                rentalCross = rentalCross.save(flush: true, failOnError: true)
                return rentalCross
            }
        }
        return null
    }

    RentalCross bootStrap(RentalCross rentalCross) {
        return createBase(rentalCross, Constants.ROOT)
    }

    RentalCross create(RentalCross rentalCross, LoginManager loginManager) {
        return createBase(rentalCross, loginManager.getUsername())
    }

    List<RentalCross> list(boolean enabled, int start, int size) {
        return RentalCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<RentalCross> list(boolean enabled, boolean admin, int start, int size) {
        return RentalCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<RentalCross> list(String name, int size) {
        return RentalCross.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<RentalCross> list(boolean enabled) {
        return RentalCross.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<RentalCross> list(boolean enabled, boolean admin) {
        return RentalCross.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    RentalCross refresh(Long id) {
        RentalCross rentalCross = RentalCross.findById(id)
        return rentalCross
    }

    int count(boolean enabled) {
        return RentalCross.countByEnabled(enabled)
    }

    RentalCross delete(RentalCross rentalCross, LoginManager loginManager) {
        if (!rentalCross) {
            return null
        }
        rentalCross = RentalCross.findById(rentalCross.id)
        rentalCross.enabled = !rentalCross.enabled
        return create(rentalCross, loginManager)
    }

    RentalCross byId(Long id) {
        return RentalCross.findByIdAndEnabled(id, true)
    }

    RentalCross changePassword(RentalCross rentalCross, LoginManager loginManager) {
        return create(rentalCross, loginManager)
    }

    Long findMaxCode() {
        RentalCross rentalCross = RentalCross.withCriteria {
            order("id", "desc")
        }.find() as RentalCross
        if (!rentalCross) {
            return 0
        }
        return rentalCross.code
    }

}
