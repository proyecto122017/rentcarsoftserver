package edu.utesa.proyecto.server.rentcarsoft.domains.utils

import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.EDescriptionAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class EDescription implements APIConverter<EDescriptionAPI> {

    String description

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        description nullable: true
    }

    static mapping = {
        table "ent_description"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((EDescription) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }


    @Override
    String toString() {
        return description
    }

    @Override
    EDescriptionAPI toAPI() {
        EDescriptionAPI eDescriptionAPI = new EDescriptionAPI()
        eDescriptionAPI.description = description
        return eDescriptionAPI
    }
}
