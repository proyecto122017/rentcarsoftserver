package edu.utesa.proyecto.server.rentcarsoft.services.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.InvoiceDetailRent
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("invoiceDetailRentService")
@Transactional(rollbackFor = Exception.class)
class InvoiceDetailRentService {

    private InvoiceDetailRent createBase(InvoiceDetailRent invoiceDetailRent, String userName) {
        if (invoiceDetailRent != null && !invoiceDetailRent.hasErrors()) {
            if (InvoiceDetailRent.exists(invoiceDetailRent.id)) {
                invoiceDetailRent.lastUpdated = new Date()
                invoiceDetailRent.modifyBy = userName
                return invoiceDetailRent.merge(flush: true, failOnError: true)
            } else {
                invoiceDetailRent.dateCreated = new Date()
                invoiceDetailRent.createdBy = userName
                invoiceDetailRent = invoiceDetailRent.save(flush: true, failOnError: true)
                return invoiceDetailRent
            }
        }
        return null
    }

    InvoiceDetailRent bootStrap(InvoiceDetailRent invoiceDetailRent) {
        return createBase(invoiceDetailRent, Constants.ROOT)
    }

    InvoiceDetailRent create(InvoiceDetailRent invoiceDetailRent, LoginManager loginManager) {
        return createBase(invoiceDetailRent, loginManager.getUsername())
    }

    List<InvoiceDetailRent> list(boolean enabled, int start, int size) {
        return InvoiceDetailRent.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<InvoiceDetailRent> list(boolean enabled, boolean admin, int start, int size) {
        return InvoiceDetailRent.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<InvoiceDetailRent> list(String name, int size) {
        return InvoiceDetailRent.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<InvoiceDetailRent> list(boolean enabled) {
        return InvoiceDetailRent.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<InvoiceDetailRent> list(boolean enabled, boolean admin) {
        return InvoiceDetailRent.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    InvoiceDetailRent refresh(Long id) {
        InvoiceDetailRent invoiceDetailRent = InvoiceDetailRent.findById(id)
        return invoiceDetailRent
    }

    int count(boolean enabled) {
        return InvoiceDetailRent.countByEnabled(enabled)
    }

    InvoiceDetailRent delete(InvoiceDetailRent invoiceDetailRent, LoginManager loginManager) {
        if (!invoiceDetailRent) {
            return null
        }
        invoiceDetailRent = InvoiceDetailRent.findById(invoiceDetailRent.id)
        invoiceDetailRent.enabled = !invoiceDetailRent.enabled
        return create(invoiceDetailRent, loginManager)
    }

    InvoiceDetailRent byId(Long id) {
        return InvoiceDetailRent.findByIdAndEnabled(id, true)
    }

    Long findMaxCode() {
        InvoiceDetailRent invoiceDetailRent = InvoiceDetailRent.withCriteria {
            order("id", "desc")
        }.find() as InvoiceDetailRent
        if (!invoiceDetailRent) {
            return 0
        }
        return invoiceDetailRent.code
    }

}
