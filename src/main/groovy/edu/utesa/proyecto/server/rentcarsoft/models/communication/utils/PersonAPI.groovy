package edu.utesa.proyecto.server.rentcarsoft.models.communication.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Contact
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Address
import edu.utesa.proyecto.server.rentcarsoft.domains.security.Gender
import edu.utesa.proyecto.server.rentcarsoft.domains.security.Occupation
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Email
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Identification
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Phone
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.administration.ContactAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.AddressAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.GenderAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.OccupationAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class PersonAPI {

    int code
    String name
    String photo
    Date birthDate
    TypesAPI type
    IdentificationAPI identification
    GenderAPI gender
    OccupationAPI occupation
    List<ContactAPI> contactList
    List<EmailAPI> emailList
    List<PhoneAPI> phoneList
    List<AddressAPI> addressList
}
