package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Coordinates
import edu.utesa.proyecto.server.rentcarsoft.services.location.CoordinatesService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicCoordinates {

    @Autowired
    private CoordinatesService coordinatesService

    Coordinates trabajo
    Coordinates casa

    BasicCoordinates() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        trabajo = create(1, 19.434933, -70.692093)
        casa = create(2, 19.424427, -70.657091)
    }

    private Coordinates create(int code, BigDecimal longitude, BigDecimal latitude) {
        Coordinates coordinates = coordinatesService.byCode(code)
        if (!coordinates) {
            coordinates = new Coordinates()
            coordinates.code = code
            coordinates.longitude = longitude
            coordinates.latitude = latitude
            return coordinatesService.bootStrap(coordinates)
        }
        return coordinates
    }
}
