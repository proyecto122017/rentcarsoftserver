package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleVersion
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleVersionService")
@Transactional(rollbackFor = Exception.class)
class VehicleVersionService {

    private VehicleVersion createBase(VehicleVersion vehicleVersion, String userName) {
        if (vehicleVersion != null && !vehicleVersion.hasErrors()) {
            if (VehicleVersion.exists(vehicleVersion.id)) {
                vehicleVersion.lastUpdated = new Date()
                vehicleVersion.modifyBy = userName
                return vehicleVersion.merge(flush: true, failOnError: true)
            } else {
                vehicleVersion.createdBy = userName
                vehicleVersion.dateCreated = new Date()
                vehicleVersion = vehicleVersion.save(flush: true, failOnError: true)
                return vehicleVersion
            }
        }
        return null
    }

    VehicleVersion bootStrap(VehicleVersion vehicleVersion) {
        return createBase(vehicleVersion, Constants.ROOT)
    }

    VehicleVersion create(VehicleVersion vehicleVersion, LoginManager loginManager) {
        return createBase(vehicleVersion, loginManager.getUsername())
    }

    List<VehicleVersion> list(boolean enabled, int start, int size) {
        return VehicleVersion.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleVersion> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleVersion.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleVersion> list(String name, int size) {
        return VehicleVersion.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleVersion> list(boolean enabled) {
        return VehicleVersion.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleVersion> list(boolean enabled, boolean admin) {
        return VehicleVersion.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleVersion refresh(Long id) {
        VehicleVersion vehicleVersion = VehicleVersion.findById(id)
        return vehicleVersion
    }

    int count(boolean enabled) {
        return VehicleVersion.countByEnabled(enabled)
    }

    VehicleVersion delete(VehicleVersion vehicleVersion, LoginManager loginManager) {
        if (!vehicleVersion) {
            return null
        }
        vehicleVersion = VehicleVersion.findById(vehicleVersion.id)
        vehicleVersion.enabled = !vehicleVersion.enabled
        return create(vehicleVersion, loginManager)
    }

    VehicleVersion byEmail(String vehicleVersion) {
        return VehicleVersion.findByEmailAndEnabled(vehicleVersion, true)
    }

    VehicleVersion byId(Long id) {
        return VehicleVersion.findByIdAndEnabled(id, true)
    }

    VehicleVersion byCode(int id) {
        return VehicleVersion.findByCodeAndEnabled(id, true)
    }

    Long findMaxCode() {
        VehicleVersion vehicleVersion = VehicleVersion.withCriteria {
            order("id", "desc")
        }.find() as VehicleVersion
        if (!vehicleVersion) {
            return 0
        }
        return vehicleVersion.code
    }

}
