package edu.utesa.proyecto.server.rentcarsoft.utils.security

import groovy.transform.CompileStatic

@CompileStatic
class SessionManager {

    private final Map<String, Session> sessions = new HashMap<>()

    boolean isLoginByUserName(String username) {
        for (Map.Entry<String, Session> entry : sessions.entrySet()) {
            if (entry.value.username.equals(username)) {
                entry.value.date = new Date()
                return true
            }
        }
        return false
    }

    boolean isLogin(String idSession) {
        Session session = sessions.get(idSession)
        if (session) {
            session.date = new Date()
            return true
        }
        return false
    }

    Session login(String username) {
        for (Map.Entry<String, Session> entry : sessions.entrySet()) {
            if (entry.value.username.equals(username)) {
                entry.value.key = UUID.randomUUID().toString()
                entry.value.session = UUID.randomUUID().toString()
                entry.value.date = new Date()
                return entry.value
            }
        }
        String idSession = UUID.randomUUID().toString()
        synchronized (sessions) {
            String key = UUID.randomUUID().toString()
            sessions.put(idSession, new Session(username, idSession, key))
        }
        return sessions.get(idSession)
    }

    Session findSession(String idSession) {
        Session session = sessions.get(idSession)
        if (session) {
            session.date = new Date()
        }
        return session
    }

    private boolean expire(Session session) {
        // Comparo las fechas
        return false
    }
}
