package edu.utesa.proyecto.server.rentcarsoft.domains.location

import edu.utesa.proyecto.server.rentcarsoft.models.communication.location.MunicipalityAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by Desckop on 8/6/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Municipality implements APIConverter<MunicipalityAPI> {

    int code
    String description
    Sector sector

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        description nullable: true
        sector nullable: true

    }

    static mapping = {
        table "municipality"
        sector fetch: "join"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Municipality) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    MunicipalityAPI toAPI() {
        MunicipalityAPI municipalityAPI = new MunicipalityAPI()
        municipalityAPI.code = code
        municipalityAPI.description = description
        if (sector) {
            municipalityAPI.sector = sector.toAPI()
        }
        return municipalityAPI
    }
}
