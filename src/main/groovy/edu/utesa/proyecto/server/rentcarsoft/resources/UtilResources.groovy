package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.*
import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.*
import edu.utesa.proyecto.server.rentcarsoft.services.utils.*
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/util")
@CompileStatic
class UtilResources {

    static final SessionManager sessionManager = new SessionManager()

    @Autowired
    private EDescriptionService eDescriptionService
    @Autowired
    private EmailService emailService
    @Autowired
    private EntitiesService entitiesService
    @Autowired
    private GeneralInfoService generalInfoService
    @Autowired
    private IdentificationService identificationService
    @Autowired
    private PersonService personService
    @Autowired
    private PhoneService phoneService
    @Autowired
    private PhotosService photosService
    @Autowired
    private TypesService typesService

    @POST
    static String isUP() {
        return "OK"
    }

    @POST
    @Path('/eDescriptions')
    String listEDescriptions(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<EDescriptionAPI> eDescriptionAPIS = new ArrayList<>()
        for (EDescription eDescription : eDescriptionService.list(true)) {
            eDescriptionAPIS.add(eDescription.toAPI())
        }
        return Constants.instance.stringify(eDescriptionAPIS)
    }

    @POST
    @Path('/create/eDescription')
    String createEDescription(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        EDescriptionAPI eDescriptionAPI = Constants.instance.convert(request, EDescriptionAPI.class)
        if (!eDescriptionAPI) {
            // No parsea el objeto que enviaron
        }
        EDescription eDescription = new EDescription()
        eDescription.description = eDescriptionAPI.description ?: ""
        eDescriptionService.bootStrap(eDescription)
        // Valido que los campos no sean null en mi caso
        // Creo en la db el objeto con las reglas que yo diga
        return "Salve la data"
    }

    @POST
    @Path('/emails')
    String listEmails(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<EmailAPI> emailAPIS = new ArrayList<>()
        for (Email email : emailService.list(true)) {
            emailAPIS.add(email.toAPI())
        }
        return Constants.instance.stringify(emailAPIS)
    }

    @POST
    @Path('/entities')
    String listEntitiess(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<EntitiesAPI> entitiesAPIS = new ArrayList<>()
        for (Entities entities : entitiesService.list(true)) {
            entitiesAPIS.add(entities.toAPI())
        }
        return Constants.instance.stringify(entitiesAPIS)
    }

    @POST
    @Path('/create/entities')
    String createEntities(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        EntitiesAPI entitiesAPI = Constants.instance.convert(request, EntitiesAPI.class)
        if (!entitiesAPI) {
            //aqui debo de validar los null
        }
        Entities entities = new Entities()
        entities.code = entitiesService.findMaxCode() + 1 as Integer
        entities.description = entitiesAPI.description ?: ""
        entitiesService.bootStrap(entities)
        return "Salve la data"
    }

    @POST
    @Path('/generalInfos')
    String listGeneralInfos(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<GeneralInfoAPI> generalInfoAPIS = new ArrayList<>()
        for (GeneralInfo generalInfo : generalInfoService.list(true)) {
            generalInfoAPIS.add(generalInfo.toAPI())
        }
        return Constants.instance.stringify(generalInfoAPIS)
    }

    @POST
    @Path('/create/generalInfos')
    String createGeneralInfo(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        GeneralInfoAPI generalInfosAPI = Constants.instance.convert(request, GeneralInfoAPI.class)
        if (!generalInfosAPI) {
            // aqui valido que no sea null
        }
        GeneralInfo generalInfos = new GeneralInfo()
        generalInfos.code = generalInfoService.findMaxCode() + 1 as Integer
        generalInfos.name = generalInfosAPI.name ?: ""
        generalInfoService.bootStrap(generalInfos)
        return "Salve la data"
    }

    @POST
    @Path('/identifications')
    String listIdentifications(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<IdentificationAPI> identificationAPIS = new ArrayList<>()
        for (Identification identification : identificationService.list(true)) {
            identificationAPIS.add(identification.toAPI())
        }
        return Constants.instance.stringify(identificationAPIS)
    }

    @POST
    @Path('/persons')
    String listPersons(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<PersonAPI> personAPIS = new ArrayList<>()
        for (Person person : personService.list(true)) {
            personAPIS.add(person.toAPI())
        }
        return Constants.instance.stringify(personAPIS)
    }

    @POST
    @Path('/create/persons')
    String createPerson(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        PersonAPI personAPI = Constants.instance.convert(request, PersonAPI.class)
        if (!personAPI) {
            // aqui valido que no sea null
        }
        Person person = new Person()
        person.code = personService.findMaxCode() + 1 as Integer
        person.name = personAPI.name ?: ""
        personService.bootStrap(person)
        return "Salve la data"
    }


    @POST
    @Path('/phones')
    String listPhones(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<PhoneAPI> phoneAPIS = new ArrayList<>()
        for (Phone phone : phoneService.list(true)) {
            phoneAPIS.add(phone.toAPI())
        }
        return Constants.instance.stringify(phoneAPIS)
    }

    @POST
    @Path('/create/phones')
    String createPhone(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        PhoneAPI phoneAPI = Constants.instance.convert(request, PhoneAPI.class)
        if (!phoneAPI) {
            //valido
        }
        Phone phone = new Phone()
        phone.number = phoneAPI.number ?: ""
        phoneService.bootStrap(phone)
        return "Salve la data"
    }

    @POST
    @Path('/photos')
    String listPhotos(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<PhotosAPI> photosAPIS = new ArrayList<>()
        for (Photos photos : photosService.list(true)) {
            photosAPIS.add(photos.toAPI())
        }
        return Constants.instance.stringify(photosAPIS)
    }

    @POST
    @Path('/create/types')
    String createTypes(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        TypesAPI typesAPI = Constants.instance.convert(request, TypesAPI.class)
        if (!typesAPI) {
            //valido
        }
        Types types = new Types()
        types.code = typesService.findMaxCode() + 1 as Integer
        types.eDescription = typesAPI.eDescription as EDescription
        types.entities = typesAPI.entities as Entities
        typesService.bootStrap(types)
        return "Salve la data"
    }


    @POST
    @Path('/types')
    String listTypess(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<TypesAPI> typesAPIS = new ArrayList<>()
        for (Types types : typesService.list(true)) {
            typesAPIS.add(types.toAPI())
        }
        return Constants.instance.stringify(typesAPIS)
    }
}
