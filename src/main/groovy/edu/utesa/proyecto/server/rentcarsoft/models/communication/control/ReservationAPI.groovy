package edu.utesa.proyecto.server.rentcarsoft.models.communication.control

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting.PaymentAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleBranchOfficeAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by Desckop on 6/18/17.
 */
@CompileStatic
class ReservationAPI {

    int code
    String description
    Date pickup_date
    Date return_date
    PaymentAPI payment
    List<VehicleBranchOfficeAPI> vehicleBrandOfficeList
}
