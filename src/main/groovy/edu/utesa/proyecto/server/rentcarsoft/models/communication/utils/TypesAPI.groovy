package edu.utesa.proyecto.server.rentcarsoft.models.communication.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.EDescription
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Entities
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class TypesAPI {

    int code
    EntitiesAPI entities
    EDescriptionAPI eDescription

}
