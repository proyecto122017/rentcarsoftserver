package edu.utesa.proyecto.server.rentcarsoft.models.communication.location

import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class CoordinatesAPI {

    int code

    BigDecimal latitude
    BigDecimal longitude

}
