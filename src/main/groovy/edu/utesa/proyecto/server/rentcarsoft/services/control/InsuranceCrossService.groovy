package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.InsuranceCross
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("insuranceCrossService")
@Transactional(rollbackFor = Exception.class)
class InsuranceCrossService {

    private InsuranceCross createBase(InsuranceCross insuranceCross, String userName) {
        if (insuranceCross != null && !insuranceCross.hasErrors()) {
            if (InsuranceCross.exists(insuranceCross.id)) {
                insuranceCross.lastUpdated = new Date()
                insuranceCross.modifyBy = userName
                return insuranceCross.merge(flush: true, failOnError: true)
            } else {
                insuranceCross.createdBy = userName
                insuranceCross.dateCreated = new Date()
                insuranceCross = insuranceCross.save(flush: true, failOnError: true)
                return insuranceCross
            }
        }
        return null
    }

    InsuranceCross bootStrap(InsuranceCross insuranceCross) {
        return createBase(insuranceCross, Constants.ROOT)
    }

    InsuranceCross create(InsuranceCross insuranceCross, LoginManager loginManager) {
        return createBase(insuranceCross, loginManager.getUsername())
    }

    List<InsuranceCross> list(boolean enabled, int start, int size) {
        return InsuranceCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<InsuranceCross> list(boolean enabled, boolean admin, int start, int size) {
        return InsuranceCross.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<InsuranceCross> list(String name, int size) {
        return InsuranceCross.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<InsuranceCross> list(boolean enabled) {
        return InsuranceCross.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<InsuranceCross> list(boolean enabled, boolean admin) {
        return InsuranceCross.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    InsuranceCross refresh(Long id) {
        InsuranceCross insuranceCross = InsuranceCross.findById(id)
        return insuranceCross
    }

    int count(boolean enabled) {
        return InsuranceCross.countByEnabled(enabled)
    }

    InsuranceCross delete(InsuranceCross insuranceCross, LoginManager loginManager) {
        if (!insuranceCross) {
            return null
        }
        insuranceCross = InsuranceCross.findById(insuranceCross.id)
        insuranceCross.enabled = !insuranceCross.enabled
        return create(insuranceCross, loginManager)
    }

    InsuranceCross byId(Long id) {
        return InsuranceCross.findByIdAndEnabled(id, true)
    }

    InsuranceCross changePassword(InsuranceCross insuranceCross, LoginManager loginManager) {
        return create(insuranceCross, loginManager)
    }

    Long findMaxCode() {
        InsuranceCross insuranceCross = InsuranceCross.withCriteria {
            order("id", "desc")
        }.find() as InsuranceCross
        if (!insuranceCross) {
            return 0
        }
        return insuranceCross.code
    }

}
