package edu.utesa.proyecto.server.rentcarsoft.domains.administration

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Offer
import edu.utesa.proyecto.server.rentcarsoft.domains.control.Rental
import edu.utesa.proyecto.server.rentcarsoft.domains.control.Reservation
import edu.utesa.proyecto.server.rentcarsoft.domains.location.Coordinates
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.models.communication.administration.BranchOfficeAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class BranchOffice implements APIConverter<BranchOfficeAPI> {

    int code
    Coordinates coordinates
    GeneralInfo generalInfo
    Types branchOfficeType

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static hasMany = [listOffers            : Offer, listNotifications: Notifications,
                      listVehicleBrandOffice: VehicleBranchOffice,
                      listRentals           : Rental, listReservations: Reservation]

    static constraints = {
        code unique: true
        coordinates nullable: true
        generalInfo nullable: true
        branchOfficeType nullable: true
    }

    static mapping = {
        table "branch_office"

        coordinates fetch: "join"

        listOffers lazy: false
        listNotifications lazy: false
        listVehicleBrandOffice lazy: false
        listRentals lazy: false
        listReservations lazy: false

        listVehicleBrandOffice cascade: "all"
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((BranchOffice) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    BranchOfficeAPI toAPI() {
        BranchOfficeAPI branchOfficeAPI = new BranchOfficeAPI()
        branchOfficeAPI.code = code
        if (coordinates) {
            branchOfficeAPI.coordinates = coordinates.toAPI()
        }
        if (generalInfo) {
            branchOfficeAPI.generalInfo = generalInfo.toAPI()
        }
        branchOfficeAPI.offerList = new ArrayList<>()
        for (Offer offer : listOffers) {
            branchOfficeAPI.offerList.add(offer.toAPI())
        }
        branchOfficeAPI.notificationsList = new ArrayList<>()
        for (Notifications notifications : listNotifications) {
            branchOfficeAPI.notificationsList.add(notifications.toAPI())
        }
        branchOfficeAPI.vehicleBrandOfficeList = new ArrayList<>()
        for (VehicleBranchOffice vehicleBranchOffice : listVehicleBrandOffice) {
            branchOfficeAPI.vehicleBrandOfficeList.add(vehicleBranchOffice.toAPI())
        }
        branchOfficeAPI.rentalList = new ArrayList<>()
        for (Rental rental : listRentals) {
            branchOfficeAPI.rentalList.add(rental.toAPI())
        }
        branchOfficeAPI.reservationList = new ArrayList<>()
        for (Reservation reservation : listReservations) {
            branchOfficeAPI.reservationList.add(reservation.toAPI())
        }
        return branchOfficeAPI
    }
}
