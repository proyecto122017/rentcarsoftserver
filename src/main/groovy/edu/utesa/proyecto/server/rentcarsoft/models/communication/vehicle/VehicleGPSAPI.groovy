package edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle

import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class VehicleGPSAPI {

    int code
    String serial
    String number
    BigDecimal balance

}
