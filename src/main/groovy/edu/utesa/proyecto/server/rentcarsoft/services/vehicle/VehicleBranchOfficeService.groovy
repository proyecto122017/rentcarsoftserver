package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleBranchOfficeService")
@Transactional(rollbackFor = Exception.class)
class VehicleBranchOfficeService {

    private VehicleBranchOffice createBase(VehicleBranchOffice vehicleBranchOffice, String userName) {
        if (vehicleBranchOffice != null && !vehicleBranchOffice.hasErrors()) {
            if (VehicleBranchOffice.exists(vehicleBranchOffice.id)) {
                vehicleBranchOffice.lastUpdated = new Date()
                vehicleBranchOffice.modifyBy = userName
                return vehicleBranchOffice.merge(flush: true, failOnError: true)
            } else {
                vehicleBranchOffice.createdBy = userName
                vehicleBranchOffice.dateCreated = new Date()
                vehicleBranchOffice = vehicleBranchOffice.save(flush: true, failOnError: true)
                return vehicleBranchOffice
            }
        }
        return null
    }

    VehicleBranchOffice bootStrap(VehicleBranchOffice vehicleBranchOffice) {
        return createBase(vehicleBranchOffice, Constants.ROOT)
    }

    VehicleBranchOffice create(VehicleBranchOffice vehicleBranchOffice, LoginManager loginManager) {
        return createBase(vehicleBranchOffice, loginManager.getUsername())
    }

    List<VehicleBranchOffice> list(boolean enabled, int start, int size) {
        return VehicleBranchOffice.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleBranchOffice> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleBranchOffice.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleBranchOffice> list(String name, int size) {
        return VehicleBranchOffice.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleBranchOffice> list(boolean enabled) {
        return VehicleBranchOffice.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleBranchOffice> list(boolean enabled, boolean admin) {
        return VehicleBranchOffice.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleBranchOffice refresh(Long id) {
        VehicleBranchOffice vehicleBranchOffice = VehicleBranchOffice.findById(id)
        return vehicleBranchOffice
    }

    int count(boolean enabled) {
        return VehicleBranchOffice.countByEnabled(enabled)
    }

    VehicleBranchOffice delete(VehicleBranchOffice vehicleBranchOffice, LoginManager loginManager) {
        if (!vehicleBranchOffice) {
            return null
        }
        vehicleBranchOffice = VehicleBranchOffice.findById(vehicleBranchOffice.id)
        vehicleBranchOffice.enabled = !vehicleBranchOffice.enabled
        return create(vehicleBranchOffice, loginManager)
    }

    VehicleBranchOffice byEmail(String vehicleBranchOffice) {
        return VehicleBranchOffice.findByEmailAndEnabled(vehicleBranchOffice, true)
    }

    VehicleBranchOffice byId(Long id) {
        return VehicleBranchOffice.findByIdAndEnabled(id, true)
    }

    VehicleBranchOffice byCode(Integer code) {
        return VehicleBranchOffice.findByCodeAndEnabled(code, true)
    }

    Long findMaxCode() {
        VehicleBranchOffice vehicleBranchOffice = VehicleBranchOffice.withCriteria {
            order("id", "desc")
        }.find() as VehicleBranchOffice
        if (!vehicleBranchOffice) {
            return 0
        }
        return vehicleBranchOffice.code
    }

}
