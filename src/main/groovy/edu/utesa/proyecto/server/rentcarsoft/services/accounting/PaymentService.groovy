package edu.utesa.proyecto.server.rentcarsoft.services.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("paymentService")
@Transactional(rollbackFor = Exception.class)
class PaymentService {

    private Payment createBase(Payment payment, String userName) {
        if (payment != null && !payment.hasErrors()) {
            if (Payment.exists(payment.id)) {
                payment.lastUpdated = new Date()
                payment.modifyBy = userName
                return payment.merge(flush: true, failOnError: true)
            } else {
                payment.createdBy = userName
                payment.dateCreated = new Date()
                payment = payment.save(flush: true, failOnError: true)
                return payment
            }
        }
        return null
    }

    Payment bootStrap(Payment payment) {
        return createBase(payment, Constants.ROOT)
    }

    Payment create(Payment payment, LoginManager loginManager) {
        return createBase(payment, loginManager.getUsername())
    }

    List<Payment> list(boolean enabled, int start, int size) {
        return Payment.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Payment> list(boolean enabled, boolean admin, int start, int size) {
        return Payment.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Payment> list(String name, int size) {
        return Payment.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Payment> list(boolean enabled) {
        return Payment.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Payment> list(boolean enabled, boolean admin) {
        return Payment.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Payment refresh(Long id) {
        Payment payment = Payment.findById(id)
        return payment
    }

    int count(boolean enabled) {
        return Payment.countByEnabled(enabled)
    }

    Payment delete(Payment payment, LoginManager loginManager) {
        if (!payment) {
            return null
        }
        payment = Payment.findById(payment.id)
        payment.enabled = !payment.enabled
        return create(payment, loginManager)
    }

    Payment byId(Long id) {
        return Payment.findByIdAndEnabled(id, true)
    }

    Long findMaxCode() {
        Payment payment = Payment.withCriteria {
            order("id", "desc")
        }.find() as Payment
        if (!payment) {
            return 0
        }
        return payment.code
    }

}
