package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.EDescription
import edu.utesa.proyecto.server.rentcarsoft.services.utils.EDescriptionService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicEDescription {

    @Autowired
    private EDescriptionService eDescriptionService

    EDescription tipo
    EDescription vip
    EDescription admin
    EDescription normal
    EDescription efectivo
    EDescription tarjeta
    EDescription principal
    EDescription sucursal
    EDescription credito
    EDescription contado
    EDescription correctivo
    EDescription leasing
    EDescription camioneta
    EDescription carro
    EDescription jeepeta
    EDescription jeep
    EDescription camion
    EDescription motor
    EDescription cedula
    EDescription passport

    BasicEDescription() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        admin = create("Administrador")
        normal = create("Normal")
        efectivo = create("Efectivo")
        tarjeta = create("Tarjeta")
        principal = create("Principal")
        sucursal = create("Sucursal")
        credito = create("Creduito")
        contado = create("Contado")
        correctivo = create("Correctivo")
        leasing = create("Leasing")
        camioneta = create("Camioneta")
        carro = create("Carro")
        jeepeta = create("Jeepeta")
        jeep = create("Jeep")
        camion = create("Camion")
        motor = create("Motor")
        vip = create("VIP")
        cedula = create("cedula")
        passport = create("passport")
    }

    private EDescription create(String name) {
        EDescription eDescription = eDescriptionService.byDescription(name)
        if (eDescription) {
            return eDescription
        }
        eDescription = new EDescription()
        eDescription.description = name
        return eDescriptionService.bootStrap(eDescription)
    }
}
