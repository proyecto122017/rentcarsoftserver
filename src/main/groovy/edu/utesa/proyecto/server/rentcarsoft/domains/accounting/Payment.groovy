package edu.utesa.proyecto.server.rentcarsoft.domains.accounting

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting.PaymentAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Payment implements APIConverter<PaymentAPI> {

    int code
    Types paymentType
    Date paymentDate
    BigDecimal amount

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        paymentType nullable: true

    }

    static mapping = {
        table "payment"

        paymentType fetch: "join"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Payment) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    PaymentAPI toAPI() {
        PaymentAPI paymentAPI = new PaymentAPI()
        paymentAPI.code = code
        paymentAPI.paymentType = paymentType.toAPI()
        paymentAPI.amount = amount
        paymentAPI.paymentDate = paymentDate
        return paymentAPI
    }
}
