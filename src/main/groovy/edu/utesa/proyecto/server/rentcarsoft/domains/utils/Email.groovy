package edu.utesa.proyecto.server.rentcarsoft.domains.utils

import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.EmailAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Email implements APIConverter<EmailAPI> {

    String email

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
    }

    static mapping = {
        table "emails"

    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Email) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }


    @Override
    String toString() {
        return email
    }

    @Override
    EmailAPI toAPI() {
        EmailAPI emailAPI = new EmailAPI()
        emailAPI.email = email
        return emailAPI
    }
}
