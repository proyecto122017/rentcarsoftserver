package edu.utesa.proyecto.server.rentcarsoft.domains.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle.VehicleAPI
import edu.utesa.proyecto.server.rentcarsoft.models.interfaces.APIConverter
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import grails.gorm.annotation.Entity

/**
 *  Created by ronald on 7/4/17.
 */
@SuppressWarnings(["GroovyUnusedDeclaration", "ChangeToOperator"])
@Entity
class Vehicle implements APIConverter<VehicleAPI> {

    int code
    Types vehicleType
    VehicleBrand vehicleBrand
    VehicleFeactures vehicleFeactures

    boolean enabled = true
    String createdBy = Constants.ROOT
    String modifyBy = Constants.ROOT
    Date dateCreated
    Date lastUpdated

    static constraints = {
        code unique: true
        vehicleType nullable: true
        vehicleBrand nullable: true
        vehicleFeactures nullable: true
    }

    static mapping = {
        table "vehicle"

        vehicleType fetch: 'join'
        vehicleBrand fetch: 'join'
        vehicleFeactures fetch: 'join'
    }

    @Override
    boolean equals(Object o) {
        if (!o) {
            return false
        } else if (this.is(o)) {
            return true
        } else if (!this.getClass().equals(o.getClass())) {
            return false
        }
        return this.id == ((Vehicle) o).id
    }

    @Override
    int hashCode() {
        if (id) {
            return id.hashCode()
        }
        return super.hashCode()
    }

    @Override
    VehicleAPI toAPI() {
        VehicleAPI vehicleAPI = new VehicleAPI()
        vehicleAPI.code = code
        if (vehicleType) {
            vehicleAPI.vehicleType = vehicleType.toAPI()
        }
        if (vehicleBrand) {
            vehicleAPI.vehicleBrand = vehicleBrand.toAPI()
        }
        if (vehicleFeactures) {
            vehicleAPI.vehicleFeactures = vehicleFeactures.toAPI()
        }
        return vehicleAPI
    }
}
