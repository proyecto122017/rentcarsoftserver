package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.Vehicle
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleBranchOffice
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleGPS
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleVersion
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleBranchOfficeService
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleColorService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicVehicleBranchOffice {

    @Autowired
    private VehicleBranchOfficeService vehicleBranchOfficeService
    @Autowired
    private VehicleColorService vehicleColorService

    VehicleBranchOffice vehicleBranchOffice
    VehicleBranchOffice vehicleBranchOffice1
    VehicleBranchOffice vehicleBranchOffice2
    VehicleBranchOffice vehicleBranchOffice3

    BasicVehicleBranchOffice() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicVehicle basicVehicle, BasicVehicleVersion basicVehicleVersion) {
        vehicleBranchOffice = create(1, "2011", basicVehicle.vehicle1, "23123412341235", 6, null, basicVehicleVersion.coupe, new BigDecimal(1500))
        vehicleBranchOffice1 = create(2, "2013", basicVehicle.vehicle2, "345234234", 3, null, basicVehicleVersion.coupe, new BigDecimal(1500))
        vehicleBranchOffice2 = create(3, "2015", basicVehicle.vehicle3, "231234123345234541235", 34, null, basicVehicleVersion.coupe, new BigDecimal(1500))
        vehicleBranchOffice3 = create(4, "2016", basicVehicle.vehicle4, "345656343", 15, null, basicVehicleVersion.coupe, new BigDecimal(1500))
    }

    private VehicleBranchOffice create(int code, String year, Vehicle vehicle, String chassis, int color, VehicleGPS vehicleGPS, VehicleVersion vehicleVersion, BigDecimal price) {
        VehicleBranchOffice vehicleBranchOffice = vehicleBranchOfficeService.byCode(code)
        if (!vehicleBranchOffice) {
            vehicleBranchOffice = new VehicleBranchOffice()
            vehicleBranchOffice.code = code
            vehicleBranchOffice.year = year
            vehicleBranchOffice.vehicle = vehicle
            vehicleBranchOffice.chassis = chassis
            vehicleBranchOffice.vehicleColor = vehicleColorService.byCode(color)
            vehicleBranchOffice.vehicleGPS = vehicleGPS
            vehicleBranchOffice.vehicleVersion = vehicleVersion
            vehicleBranchOffice.price = price
            return vehicleBranchOfficeService.bootStrap(vehicleBranchOffice)
        }
        return vehicleBranchOffice
    }
}
