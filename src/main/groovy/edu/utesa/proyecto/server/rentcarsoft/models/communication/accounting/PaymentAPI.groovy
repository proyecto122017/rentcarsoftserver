package edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting

import edu.utesa.proyecto.server.rentcarsoft.models.communication.utils.TypesAPI
import groovy.transform.CompileStatic

/**
 *
 * Created by ronald on 4/17/17.
 */
@CompileStatic
class PaymentAPI {

    int code
    TypesAPI paymentType
    BigDecimal amount
    Date paymentDate

}
