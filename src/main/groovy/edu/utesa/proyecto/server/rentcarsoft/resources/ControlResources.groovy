package edu.utesa.proyecto.server.rentcarsoft.resources

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.domains.control.*
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.*
import edu.utesa.proyecto.server.rentcarsoft.services.control.*
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.security.SessionManager
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

/**
 *
 *  Created by ronald on 4/17/17.
 */
@Component
@Path("/control")
@CompileStatic
class ControlResources {

    static final SessionManager sessionManager = new SessionManager()

    @Autowired
    private InsuranceService insuranceService
    @Autowired
    private OfferService offerService
    @Autowired
    private RentalService rentalService
    @Autowired
    private ReservationService reservationService
    @Autowired
    private VehicleMaintenanceService vehicleMaintenanceService
    @Autowired
    private VehicleConditionService vehicleConditionService

    @GET
    static String isUP() {
        return "OK"
    }

    @POST
    @Path('/insurances')
    String listInsurances(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<InsuranceAPI> insuranceAPIS = new ArrayList<>()
        for (Insurance insurance : insuranceService.list(true)) {
            insuranceAPIS.add(insurance.toAPI())
        }
        return Constants.instance.stringify(insuranceAPIS)
    }

    @POST
    @Path('/create/insurances')
    String createInsurance(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        InsuranceAPI insuranceAPI = Constants.instance.convert(request, InsuranceAPI.class)
        if (!insuranceAPI) {
            // No parsea el objeto que enviaron
        }
        Insurance insurance = new Insurance()
        insurance.code = insuranceService.findMaxCode() + 1 as Integer
        insurance.description = insuranceAPI.description ?: ""
        insurance.coverage = insuranceAPI.coverage
        insuranceService.bootStrap(insurance)
        return "Salve la data"
    }

    @POST
    @Path('/rentals')
    String listRentals(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<RentalAPI> rentalAPIS = new ArrayList<>()
        for (Rental rental : rentalService.list(true)) {
            rentalAPIS.add(rental.toAPI())
        }
        return Constants.instance.stringify(rentalAPIS)
    }

    @POST
    @Path('/create/rentals')
    String createRental(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        RentalAPI rentalAPI = Constants.instance.convert(request, RentalAPI.class)
        if (!rentalAPI) {
            // No parsea el objeto que enviaron
        }
        Rental rental = new Rental()
        rental.code = rentalService.findMaxCode() + 1 as Integer
        rental.rentalType = rentalAPI.rentalType as Types
        rental.description = rentalAPI.description ?: ""
        rental.pickup_date = rentalAPI.pickup_date
        rental.return_date = rentalAPI.return_date
        rental.vehicleCondition = rentalAPI.vehicleCondition as VehicleCondition
        rentalService.bootStrap(rental)
        return "Salve la data"
    }

    @POST
    @Path('/reservations')
    String listReservations(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<ReservationAPI> reservationAPIS = new ArrayList<>()
        for (Reservation reservation : reservationService.list(true)) {
            reservationAPIS.add(reservation.toAPI())
        }
        return Constants.instance.stringify(reservationAPIS)
    }

    @POST
    @Path('/create/reservations')
    String createReservation(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        ReservationAPI reservationAPI = Constants.instance.convert(request, ReservationAPI.class)
        if (!reservationAPI) {
            //valido los nulls
        }
        Reservation reservation = new Reservation()
        reservation.code = reservationService.findMaxCode() + 1 as Integer
        reservation.description = reservationAPI.description ?: ""
        reservation.pickup_date = reservationAPI.pickup_date
        reservation.return_date = reservationAPI.return_date
        reservation.payment = reservationAPI.payment as Payment
        reservationService.bootStrap(reservation)
        return "Salve la data"
    }

    @POST
    @Path('/offers')
    String listOffers(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<OfferAPI> offerAPIS = new ArrayList<>()
        for (Offer offer : offerService.list(true)) {
            offerAPIS.add(offer.toAPI())
        }
        return Constants.instance.stringify(offerAPIS)
    }

    @POST
    @Path('/create/offers')
    String createOffer(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        OfferAPI offerAPI = Constants.instance.convert(request, OfferAPI.class)
        if (!offerAPI) {
            // No parsea el objeto que enviaron
        }
        Offer offer = new Offer()
        offer.code = offerService.findMaxCode() + 1 as Integer
        offer.description = offerAPI.description ?: ""
        offerService.bootStrap(offer)
        return "Salve la data"
    }

    @POST
    @Path('/vehicleMaintenances')
    String listVehicleMaintenances(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleMaintenanceAPI> vehicleMaintenanceAPIS = new ArrayList<>()
        for (VehicleMaintenance vehicleMaintenance : vehicleMaintenanceService.list(true)) {
            vehicleMaintenanceAPIS.add(vehicleMaintenance.toAPI())
        }
        return Constants.instance.stringify(vehicleMaintenanceAPIS)
    }

    @POST
    @Path('/create/vehicleMaintenance')
    String createVehicleMaintenance(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleMaintenanceAPI vehicleMaintenanceAPI = Constants.instance.convert(request, VehicleMaintenanceAPI.class)
        if (!vehicleMaintenanceAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleMaintenance vehicleMaintenance = new VehicleMaintenance()
        vehicleMaintenance.code = vehicleMaintenanceService.findMaxCode() + 1 as Integer
        vehicleMaintenance.description = vehicleMaintenanceAPI.description ?: ""
        vehicleMaintenance.vehicleMaintenanceType = vehicleMaintenanceAPI.vehicleMaintenanceType as Types
        vehicleMaintenanceService.bootStrap(vehicleMaintenance)
        return "Salve la data"
    }

    @POST
    @Path('/vehicleConditions')
    String listVehicleConditions(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        List<VehicleConditionAPI> vehicleConditionAPIS = new ArrayList<>()
        for (VehicleCondition vehicleCondition : vehicleConditionService.list(true)) {
            vehicleConditionAPIS.add(vehicleCondition.toAPI())
        }
        return Constants.instance.stringify(vehicleConditionAPIS)
    }

    @POST
    @Path('/create/vehicleCondition')
    String createVehicleCondition(
            @FormParam(Constants.PARAM_SESSION) String session, @FormParam(Constants.PARAM_DATA) String request) {
        println "Me enviaron: " + request
//        if (!sessionManager.isLogin(session)) {
//            return ""
//        }
        VehicleConditionAPI vehicleConditionAPI = Constants.instance.convert(request, VehicleConditionAPI.class)
        if (!vehicleConditionAPI) {
            // No parsea el objeto que enviaron
        }
        VehicleCondition vehicleCondition = new VehicleCondition()
        vehicleCondition.code = vehicleConditionService.findMaxCode() + 1 as Integer
        vehicleCondition.description = vehicleConditionAPI.description ?: ""
        vehicleConditionService.bootStrap(vehicleCondition)
        return "Salve la data"
    }
}
