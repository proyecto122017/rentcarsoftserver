package edu.utesa.proyecto.server.rentcarsoft.services.administrator

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.BranchOffice
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("branchOfficeService")
@Transactional(rollbackFor = Exception.class)
class BranchOfficeService {

    private BranchOffice createBase(BranchOffice branchOffice, String userName) {
        if (branchOffice != null && !branchOffice.hasErrors()) {
            if (BranchOffice.exists(branchOffice.id)) {
                branchOffice.lastUpdated = new Date()
                branchOffice.modifyBy = userName
                return branchOffice.merge(flush: true, failOnError: true)
            } else {
                branchOffice.createdBy = userName
                branchOffice.dateCreated = new Date()
                branchOffice = branchOffice.merge(flush: true, failOnError: true)
                return branchOffice
            }
        }
        return null
    }

    BranchOffice bootStrap(BranchOffice branchOffice) {
        return createBase(branchOffice, Constants.ROOT)
    }

    BranchOffice create(BranchOffice branchOffice, LoginManager loginManager) {
        return createBase(branchOffice, loginManager.getUsername())
    }

    List<BranchOffice> list(boolean enabled, int start, int size) {
        return BranchOffice.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<BranchOffice> list(boolean enabled, boolean admin, int start, int size) {
        return BranchOffice.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<BranchOffice> list(String name, int size) {
        return BranchOffice.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<BranchOffice> list(boolean enabled) {
        return BranchOffice.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<BranchOffice> list(boolean enabled, boolean admin) {
        return BranchOffice.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    BranchOffice refresh(Long id) {
        BranchOffice branchOffice = BranchOffice.findById(id)
        return branchOffice
    }

    int count(boolean enabled) {
        return BranchOffice.countByEnabled(enabled)
    }

    BranchOffice delete(BranchOffice branchOffice, LoginManager loginManager) {
        if (!branchOffice) {
            return null
        }
        branchOffice = BranchOffice.findById(branchOffice.id)
        branchOffice.enabled = !branchOffice.enabled
        return create(branchOffice, loginManager)
    }

    BranchOffice byId(Long id) {
        return BranchOffice.findByIdAndEnabled(id, true)
    }

    BranchOffice byCode(Integer code) {
        return BranchOffice.findByCodeAndEnabled(code, true)
    }

    BranchOffice changePassword(BranchOffice branchOffice, LoginManager loginManager) {
        return create(branchOffice, loginManager)
    }

    Long findMaxCode() {
        BranchOffice branchOffice = BranchOffice.withCriteria {
            order("id", "desc")
        }.find() as BranchOffice
        if (!branchOffice) {
            return 1
        }
        return branchOffice.code
    }

}
