package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.administration

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Enterprise
import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Rentcar
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.services.administrator.EnterpriseService
import edu.utesa.proyecto.server.rentcarsoft.services.administrator.RentcarService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils.BasicGeneralInfo
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicEnterprise {

    @Autowired
    private EnterpriseService enterpriseService

    @Autowired
    private RentcarService rentcarService

    Enterprise Global

    BasicEnterprise() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicGeneralInfo basicGeneralInfo) {
        List<Rentcar> rentcarList = new ArrayList<>()
        for (Rentcar rentcar : rentcarService.list()) {
            rentcarList.add(rentcar)
        }
        Global = create(1, basicGeneralInfo.ronaldEnterprise, rentcarList)
    }

    private Enterprise create(int code, GeneralInfo generalInfo, List<Rentcar> rentcarList) {
        Enterprise enterprise = enterpriseService.byCode(code)
        if (!enterprise) {
            enterprise = new Enterprise()
            enterprise.code = code
            enterprise.generalInfo = generalInfo
            enterprise.listRentcars = rentcarList.toSet()
            return enterpriseService.bootStrap(enterprise)
        }
        return enterprise
    }
}
