package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Entities
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("entitiesService")
@Transactional(rollbackFor = Exception.class)
class EntitiesService {

    private Entities createBase(Entities entities, String userName) {
        if (entities != null && !entities.hasErrors()) {
            if (Entities.exists(entities.id)) {
                entities.lastUpdated = new Date()
                entities.modifyBy = userName
                return entities.merge(flush: true, failOnError: true)
            } else {
                entities.createdBy = userName
                entities.dateCreated = new Date()
                entities = entities.save(flush: true, failOnError: true)
                return entities
            }
        }
        return null
    }

    Entities bootStrap(Entities entities) {
        return createBase(entities, Constants.ROOT)
    }

    Entities create(Entities entities, LoginManager loginManager) {
        return createBase(entities, loginManager.getUsername())
    }

    List<Entities> list(boolean enabled, int start, int size) {
        return Entities.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Entities> list(boolean enabled, boolean admin, int start, int size) {
        return Entities.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Entities> list(String name, int size) {
        return Entities.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Entities> list(boolean enabled) {
        return Entities.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Entities> list(boolean enabled, boolean admin) {
        return Entities.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Entities refresh(Long id) {
        Entities entities = Entities.findById(id)
        return entities
    }

    int count(boolean enabled) {
        return Entities.countByEnabled(enabled)
    }

    Entities delete(Entities entities, LoginManager loginManager) {
        if (!entities) {
            return null
        }
        entities = Entities.findById(entities.id)
        entities.enabled = !entities.enabled
        return create(entities, loginManager)
    }

    Entities byDescription(String description) {
        return Entities.findByDescription(description)
    }

    Entities byId(Long id) {
        return Entities.findByIdAndEnabled(id, true)
    }

    Long findMaxCode() {
        Entities entities = Entities.withCriteria {
            order("id", "desc")
        }.find() as Entities
        if (!entities) {
            return 1
        }
        return entities.code
    }

}
