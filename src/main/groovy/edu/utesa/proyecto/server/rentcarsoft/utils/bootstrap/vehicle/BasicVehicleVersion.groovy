package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleVersion
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleVersionService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicVehicleVersion {

    @Autowired
    private VehicleVersionService vehicleVersionService

    VehicleVersion coupe
    VehicleVersion sedan
    VehicleVersion sub
    VehicleVersion sport

    BasicVehicleVersion() {
        Constants.instance.autoWiredClass(this)
    }

    void insert() {
        coupe = create(1, "Coupe")
        sedan = create(2, "Sedan")
        sub = create(3, "SUB")
        sport = create(4, "Sport")
    }

    private VehicleVersion create(int code, String name) {
        VehicleVersion vehicleVersion = vehicleVersionService.byCode(code)
        if (!vehicleVersion) {
            vehicleVersion = new VehicleVersion()
            vehicleVersion.code = code
            vehicleVersion.description = name
            return vehicleVersionService.bootStrap(vehicleVersion)
        }
        return vehicleVersion
    }
}
