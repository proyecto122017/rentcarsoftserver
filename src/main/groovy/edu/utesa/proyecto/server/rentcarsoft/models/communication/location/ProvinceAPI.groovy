package edu.utesa.proyecto.server.rentcarsoft.models.communication.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Municipality
import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class ProvinceAPI {

    int code
    String description
    MunicipalityAPI municipality

}
