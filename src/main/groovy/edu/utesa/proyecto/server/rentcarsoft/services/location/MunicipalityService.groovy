package edu.utesa.proyecto.server.rentcarsoft.services.location

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Municipality
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("municipalityService")
@Transactional(rollbackFor = Exception.class)
class MunicipalityService {

    private Municipality createBase(Municipality municipality, String userName) {
        if (municipality != null && !municipality.hasErrors()) {
            if (Municipality.exists(municipality.id)) {
                municipality.lastUpdated = new Date()
                municipality.modifyBy = userName
                return municipality.merge(flush: true, failOnError: true)
            } else {
                municipality.createdBy = userName
                municipality.dateCreated = new Date()
                municipality = municipality.save(flush: true, failOnError: true)
                return municipality
            }
        }
        return null
    }

    Municipality bootStrap(Municipality municipality) {
        return createBase(municipality, Constants.ROOT)
    }

    Municipality create(Municipality municipality, LoginManager loginManager) {
        return createBase(municipality, loginManager.getUsername())
    }

    List<Municipality> list(boolean enabled, int start, int size) {
        return Municipality.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Municipality> list(boolean enabled, boolean admin, int start, int size) {
        return Municipality.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Municipality> list(String name, int size) {
        return Municipality.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Municipality> list(boolean enabled) {
        return Municipality.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Municipality> list(boolean enabled, boolean admin) {
        return Municipality.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Municipality refresh(Long id) {
        Municipality municipality = Municipality.findById(id)
        return municipality
    }

    int count(boolean enabled) {
        return Municipality.countByEnabled(enabled)
    }

    Municipality delete(Municipality municipality, LoginManager loginManager) {
        if (!municipality) {
            return null
        }
        municipality = Municipality.findById(municipality.id)
        municipality.enabled = !municipality.enabled
        return create(municipality, loginManager)
    }

    Municipality byId(Long id) {
        return Municipality.findByIdAndEnabled(id, true)
    }

    Municipality byCode(int id) {
        return Municipality.findByCodeAndEnabled(id, true)
    }

    Municipality changePassword(Municipality municipality, LoginManager loginManager) {
        return create(municipality, loginManager)
    }

    Long findMaxCode() {
        Municipality municipality = Municipality.withCriteria {
            order("id", "desc")
        }.find() as Municipality
        if (!municipality) {
            return 0
        }
        return municipality.code
    }

}
