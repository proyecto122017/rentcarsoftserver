package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Photos
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("photosService")
@Transactional(rollbackFor = Exception.class)
class PhotosService {

    private Photos createBase(Photos photos, String userName) {
        if (photos != null && !photos.hasErrors()) {
            if (Photos.exists(photos.id)) {
                photos.lastUpdated = new Date()
                photos.modifyBy = userName
                return photos.merge(flush: true, failOnError: true)
            } else {
                photos.createdBy = userName
                photos.dateCreated = new Date()
                photos = photos.save(flush: true, failOnError: true)
                return photos
            }
        }
        return null
    }

    Photos bootStrap(Photos photos) {
        return createBase(photos, Constants.ROOT)
    }

    Photos create(Photos photos, LoginManager loginManager) {
        return createBase(photos, loginManager.getUsername())
    }

    List<Photos> list(boolean enabled, int start, int size) {
        return Photos.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Photos> list(boolean enabled, boolean admin, int start, int size) {
        return Photos.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Photos> list(String name, int size) {
        return Photos.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Photos> list(boolean enabled) {
        return Photos.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Photos> list(boolean enabled, boolean admin) {
        return Photos.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Photos refresh(Long id) {
        Photos photos = Photos.findById(id)
        return photos
    }

    int count(boolean enabled) {
        return Photos.countByEnabled(enabled)
    }

    Photos delete(Photos photos, LoginManager loginManager) {
        if (!photos) {
            return null
        }
        photos = Photos.findById(photos.id)
        photos.enabled = !photos.enabled
        return create(photos, loginManager)
    }

    Photos byEmail(String photos) {
        return Photos.findByEmailAndEnabled(photos, true)
    }

    Photos byId(Long id) {
        return Photos.findByIdAndEnabled(id, true)
    }

}
