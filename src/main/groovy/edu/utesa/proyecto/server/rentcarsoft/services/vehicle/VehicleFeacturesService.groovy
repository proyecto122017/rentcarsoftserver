package edu.utesa.proyecto.server.rentcarsoft.services.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleFeactures
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleFeacturesService")
@Transactional(rollbackFor = Exception.class)
class VehicleFeacturesService {

    private VehicleFeactures createBase(VehicleFeactures vehicleFeactures, String userName) {
        if (vehicleFeactures != null && !vehicleFeactures.hasErrors()) {
            if (VehicleFeactures.exists(vehicleFeactures.id)) {
                vehicleFeactures.lastUpdated = new Date()
                vehicleFeactures.modifyBy = userName
                return vehicleFeactures.merge(flush: true, failOnError: true)
            } else {
                vehicleFeactures.createdBy = userName
                vehicleFeactures.dateCreated = new Date()
                vehicleFeactures = vehicleFeactures.save(flush: true, failOnError: true)
                return vehicleFeactures
            }
        }
        return null
    }

    VehicleFeactures bootStrap(VehicleFeactures vehicleFeactures) {
        return createBase(vehicleFeactures, Constants.ROOT)
    }

    VehicleFeactures create(VehicleFeactures vehicleFeactures, LoginManager loginManager) {
        return createBase(vehicleFeactures, loginManager.getUsername())
    }

    List<VehicleFeactures> list(boolean enabled, int start, int size) {
        return VehicleFeactures.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleFeactures> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleFeactures.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleFeactures> list(String name, int size) {
        return VehicleFeactures.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleFeactures> list(boolean enabled) {
        return VehicleFeactures.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleFeactures> list(boolean enabled, boolean admin) {
        return VehicleFeactures.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleFeactures refresh(Long id) {
        VehicleFeactures vehicleFeactures = VehicleFeactures.findById(id)
        return vehicleFeactures
    }

    int count(boolean enabled) {
        return VehicleFeactures.countByEnabled(enabled)
    }

    VehicleFeactures delete(VehicleFeactures vehicleFeactures, LoginManager loginManager) {
        if (!vehicleFeactures) {
            return null
        }
        vehicleFeactures = VehicleFeactures.findById(vehicleFeactures.id)
        vehicleFeactures.enabled = !vehicleFeactures.enabled
        return create(vehicleFeactures, loginManager)
    }

    VehicleFeactures byEmail(String vehicleFeactures) {
        return VehicleFeactures.findByEmailAndEnabled(vehicleFeactures, true)
    }

    VehicleFeactures byId(Long id) {
        return VehicleFeactures.findByIdAndEnabled(id, true)
    }

    VehicleFeactures byCode(int id) {
        return VehicleFeactures.findByCodeAndEnabled(id, true)
    }

    Long findMaxCode() {
        VehicleFeactures vehicleFeactures = VehicleFeactures.withCriteria {
            order("id", "desc")
        }.find() as VehicleFeactures
        if (!vehicleFeactures) {
            return 0
        }
        return vehicleFeactures.code
    }

}
