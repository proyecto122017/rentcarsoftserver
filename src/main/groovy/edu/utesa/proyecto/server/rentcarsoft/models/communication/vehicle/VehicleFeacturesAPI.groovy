package edu.utesa.proyecto.server.rentcarsoft.models.communication.vehicle

import groovy.transform.CompileStatic

/**
 *
 * Created by Sandy on 6/18/17.
 */
@CompileStatic
class VehicleFeacturesAPI {

    int code
    String description

}
