package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.Reservation
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("reservationService")
@Transactional(rollbackFor = Exception.class)
class ReservationService {

    private Reservation createBase(Reservation reservation, String userName) {
        if (reservation != null && !reservation.hasErrors()) {
            if (Reservation.exists(reservation.id)) {
                reservation.lastUpdated = new Date()
                reservation.modifyBy = userName
                return reservation.merge(flush: true, failOnError: true)
            } else {
                reservation.createdBy = userName
                reservation.dateCreated = new Date()
                reservation = reservation.save(flush: true, failOnError: true)
                return reservation
            }
        }
        return null
    }

    Reservation bootStrap(Reservation reservation) {
        return createBase(reservation, Constants.ROOT)
    }

    Reservation create(Reservation reservation, LoginManager loginManager) {
        return createBase(reservation, loginManager.getUsername())
    }

    List<Reservation> list(boolean enabled, int start, int size) {
        return Reservation.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Reservation> list(boolean enabled, boolean admin, int start, int size) {
        return Reservation.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Reservation> list(String name, int size) {
        return Reservation.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Reservation> list(boolean enabled) {
        return Reservation.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Reservation> list(boolean enabled, boolean admin) {
        return Reservation.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Reservation refresh(Long id) {
        Reservation reservation = Reservation.findById(id)
        return reservation
    }

    int count(boolean enabled) {
        return Reservation.countByEnabled(enabled)
    }

    Reservation delete(Reservation reservation, LoginManager loginManager) {
        if (!reservation) {
            return null
        }
        reservation = Reservation.findById(reservation.id)
        reservation.enabled = !reservation.enabled
        return create(reservation, loginManager)
    }

    Reservation byId(Long id) {
        return Reservation.findByIdAndEnabled(id, true)
    }

    Reservation changePassword(Reservation reservation, LoginManager loginManager) {
        return create(reservation, loginManager)
    }

    Long findMaxCode() {
        Reservation reservation = Reservation.withCriteria {
            order("id", "desc")
        }.find() as Reservation
        if (!reservation) {
            return 0
        }
        return reservation.code
    }

}
