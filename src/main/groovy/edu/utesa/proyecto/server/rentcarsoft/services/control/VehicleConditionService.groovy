package edu.utesa.proyecto.server.rentcarsoft.services.control

import edu.utesa.proyecto.server.rentcarsoft.domains.control.VehicleCondition
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("vehicleConditionService")
@Transactional(rollbackFor = Exception.class)
class VehicleConditionService {

    private VehicleCondition createBase(VehicleCondition vehicleCondition, String userName) {
        if (vehicleCondition != null && !vehicleCondition.hasErrors()) {
            if (VehicleCondition.exists(vehicleCondition.id)) {
                vehicleCondition.lastUpdated = new Date()
                vehicleCondition.modifyBy = userName
                return vehicleCondition.merge(flush: true, failOnError: true)
            } else {
                vehicleCondition.createdBy = userName
                vehicleCondition.dateCreated = new Date()
                vehicleCondition = vehicleCondition.save(flush: true, failOnError: true)
                return vehicleCondition
            }
        }
        return null
    }

    VehicleCondition bootStrap(VehicleCondition vehicleCondition) {
        return createBase(vehicleCondition, Constants.ROOT)
    }

    VehicleCondition create(VehicleCondition vehicleCondition, LoginManager loginManager) {
        return createBase(vehicleCondition, loginManager.getUsername())
    }

    List<VehicleCondition> list(boolean enabled, int start, int size) {
        return VehicleCondition.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleCondition> list(boolean enabled, boolean admin, int start, int size) {
        return VehicleCondition.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<VehicleCondition> list(String name, int size) {
        return VehicleCondition.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<VehicleCondition> list(boolean enabled) {
        return VehicleCondition.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<VehicleCondition> list(boolean enabled, boolean admin) {
        return VehicleCondition.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    VehicleCondition refresh(Long id) {
        VehicleCondition vehicleCondition = VehicleCondition.findById(id)
        return vehicleCondition
    }

    int count(boolean enabled) {
        return VehicleCondition.countByEnabled(enabled)
    }

    VehicleCondition delete(VehicleCondition vehicleCondition, LoginManager loginManager) {
        if (!vehicleCondition) {
            return null
        }
        vehicleCondition = VehicleCondition.findById(vehicleCondition.id)
        vehicleCondition.enabled = !vehicleCondition.enabled
        return create(vehicleCondition, loginManager)
    }

    VehicleCondition byId(Long id) {
        return VehicleCondition.findByIdAndEnabled(id, true)
    }

    VehicleCondition changePassword(VehicleCondition vehicleCondition, LoginManager loginManager) {
        return create(vehicleCondition, loginManager)
    }

    Long findMaxCode() {
        VehicleCondition vehicleCondition = VehicleCondition.withCriteria {
            order("id", "desc")
        }.find() as VehicleCondition
        if (!vehicleCondition) {
            return 0
        }
        return vehicleCondition.code
    }

}
