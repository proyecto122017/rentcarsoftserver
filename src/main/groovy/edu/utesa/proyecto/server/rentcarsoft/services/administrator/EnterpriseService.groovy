package edu.utesa.proyecto.server.rentcarsoft.services.administrator

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Enterprise
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("enterpriseService")
@Transactional(rollbackFor = Exception.class)
class EnterpriseService {

    private Enterprise createBase(Enterprise enterprise, String userName) {
        if (enterprise != null && !enterprise.hasErrors()) {
            if (Enterprise.exists(enterprise.id)) {
                enterprise.lastUpdated = new Date()
                enterprise.modifyBy = userName
                return enterprise.merge(flush: true, failOnError: true)
            } else {
                enterprise.createdBy = userName
                enterprise.dateCreated = new Date()
                enterprise = enterprise.merge(flush: true, failOnError: true)
                return enterprise
            }
        }
        return null
    }

    Enterprise bootStrap(Enterprise enterprise) {
        return createBase(enterprise, Constants.ROOT)
    }

    Enterprise create(Enterprise enterprise, LoginManager loginManager) {
        return createBase(enterprise, loginManager.getUsername())
    }

    List<Enterprise> list(boolean enabled, int start, int size) {
        return Enterprise.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Enterprise> list(boolean enabled, boolean admin, int start, int size) {
        return Enterprise.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Enterprise> list(String name, int size) {
        return Enterprise.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Enterprise> list() {
        return Enterprise.findAll()
    }

    List<Enterprise> list(boolean enabled, boolean admin) {
        return Enterprise.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Enterprise refresh(Long id) {
        Enterprise enterprise = Enterprise.findById(id)
        return enterprise
    }

    int count(boolean enabled) {
        return Enterprise.countByEnabled(enabled)
    }

    Enterprise delete(Enterprise enterprise, LoginManager loginManager) {
        if (!enterprise) {
            return null
        }
        enterprise = Enterprise.findById(enterprise.id)
        enterprise.enabled = !enterprise.enabled
        return create(enterprise, loginManager)
    }

    Enterprise byId(Long id) {
        return Enterprise.findByIdAndEnabled(id, true)
    }

    Enterprise changePassword(Enterprise enterprise, LoginManager loginManager) {
        return create(enterprise, loginManager)
    }

    Enterprise byCode(Integer code) {
        return Enterprise.findByCodeAndEnabled(code, true)
    }

    Long findMaxCode() {
        Enterprise enterprise = Enterprise.withCriteria {
            order("id", "desc")
        }.find() as Enterprise
        if (!enterprise) {
            return 0
        }
        return enterprise.code
    }

}
