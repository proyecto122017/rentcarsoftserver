package edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.vehicle

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.Vehicle
import edu.utesa.proyecto.server.rentcarsoft.domains.vehicle.VehicleFeactures
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleBrandService
import edu.utesa.proyecto.server.rentcarsoft.services.vehicle.VehicleService
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import edu.utesa.proyecto.server.rentcarsoft.utils.bootstrap.utils.BasicTypes
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

/**
 *  Created by ronald on 7/8/17.
 */
@CompileStatic
class BasicVehicle {

    @Autowired
    private VehicleService vehicleService
    @Autowired
    private VehicleBrandService vehicleBrandService

    Vehicle vehicle1
    Vehicle vehicle2
    Vehicle vehicle3
    Vehicle vehicle4

    BasicVehicle() {
        Constants.instance.autoWiredClass(this)
    }

    void insert(BasicTypes basicTypes, BasicVehicleFeactures basicVehicleFeactures) {
        vehicle1 = create(1, basicTypes.typesVehiculoCamion, 54, basicVehicleFeactures.gasoil)
        vehicle2 = create(2, basicTypes.typesVehiculocarro, 3, basicVehicleFeactures.gasoline)
        vehicle3 = create(3, basicTypes.typesVehiculojeep, 34, basicVehicleFeactures.fourDoors)
        vehicle4 = create(4, basicTypes.typesVehiculojeepeta, 11, basicVehicleFeactures.ecoMode)
    }

    private Vehicle create(int code, Types types, Integer vehicleBrandCode, VehicleFeactures vehicleFeactures) {
        Vehicle vehicle = vehicleService.byCode(code)
        if (!vehicle) {
            vehicle = new Vehicle()
            vehicle.code = code
            vehicle.vehicleType = types
            vehicle.vehicleBrand = vehicleBrandService.byCode(vehicleBrandCode)
            vehicle.vehicleFeactures = vehicleFeactures
            return vehicleService.bootStrap(vehicle)
        }
        return vehicle
    }
}
