package edu.utesa.proyecto.server.rentcarsoft.services.administrator

import edu.utesa.proyecto.server.rentcarsoft.domains.administration.Contact
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("contactService")
@Transactional(rollbackFor = Exception.class)
class ContactService {

    private Contact createBase(Contact contact, String userName) {
        if (contact != null && !contact.hasErrors()) {
            if (Contact.exists(contact.id)) {
                contact.lastUpdated = new Date()
                contact.modifyBy = userName
                return contact.merge(flush: true, failOnError: true)
            } else {
                contact.createdBy = userName
                contact.dateCreated = new Date()
                contact = contact.save(flush: true, failOnError: true)
                return contact
            }
        }
        return null
    }

    Contact bootStrap(Contact contact) {
        return createBase(contact, Constants.ROOT)
    }

    Contact create(Contact contact, LoginManager loginManager) {
        return createBase(contact, loginManager.getUsername())
    }

    List<Contact> list(boolean enabled, int start, int size) {
        return Contact.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Contact> list(boolean enabled, boolean admin, int start, int size) {
        return Contact.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<Contact> list(String name, int size) {
        return Contact.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<Contact> list(boolean enabled) {
        return Contact.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<Contact> list(boolean enabled, boolean admin) {
        return Contact.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    Contact refresh(Long id) {
        Contact contact = Contact.findById(id)
        return contact
    }

    int count(boolean enabled) {
        return Contact.countByEnabled(enabled)
    }

    Contact delete(Contact contact, LoginManager loginManager) {
        if (!contact) {
            return null
        }
        contact = Contact.findById(contact.id)
        contact.enabled = !contact.enabled
        return create(contact, loginManager)
    }

    Contact byId(Long id) {
        return Contact.findByIdAndEnabled(id, true)
    }

    Contact changePassword(Contact contact, LoginManager loginManager) {
        return create(contact, loginManager)
    }

    Long findMaxCode() {
        Contact contact = Contact.withCriteria {
            order("id", "desc")
        }.find() as Contact
        if (!contact) {
            return 0
        }
        return contact.code
    }

}
