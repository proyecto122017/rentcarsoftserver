package edu.utesa.proyecto.server.rentcarsoft.services.utils

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.models.LoginManager
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 *  Created by Ronald on 16/11/17.
 */
@SuppressWarnings("GrMethodMayBeStatic")
@Service("generalInfoService")
@Transactional(rollbackFor = Exception.class)
class GeneralInfoService {

    private GeneralInfo createBase(GeneralInfo generalInfo, String userName) {
        if (generalInfo != null && !generalInfo.hasErrors()) {
            if (GeneralInfo.exists(generalInfo.id)) {
                generalInfo.lastUpdated = new Date()
                generalInfo.modifyBy = userName
                return generalInfo.merge(flush: true, failOnError: true)
            } else {
                generalInfo.createdBy = userName
                generalInfo.dateCreated = new Date()
                generalInfo = generalInfo.save(flush: true, failOnError: true)
                return generalInfo
            }
        }
        return null
    }

    GeneralInfo bootStrap(GeneralInfo generalInfo) {
        return createBase(generalInfo, Constants.ROOT)
    }

    GeneralInfo create(GeneralInfo generalInfo, LoginManager loginManager) {
        return createBase(generalInfo, loginManager.getUsername())
    }

    List<GeneralInfo> list(boolean enabled, int start, int size) {
        return GeneralInfo.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<GeneralInfo> list(boolean enabled, boolean admin, int start, int size) {
        return GeneralInfo.withCriteria {
            firstResult(start)
            maxResults(size)
            order("id", "desc")
            eq "enabled", enabled
            eq "admin", admin
        }.findAll()
    }

    List<GeneralInfo> list(String name, int size) {
        return GeneralInfo.withCriteria {
            if (size > 0) {
                maxResults(size)
            }
            order("id", "desc")
            like "name", "%" + name + "%"
            eq "enabled", true
        }.findAll()
    }

    List<GeneralInfo> list(boolean enabled) {
        return GeneralInfo.withCriteria {
            order("id", "desc")
            eq "enabled", enabled
        }.findAll()
    }

    List<GeneralInfo> list(boolean enabled, boolean admin) {
        return GeneralInfo.withCriteria {
            order("id", "desc")
            eq "admin", admin
            eq "enabled", enabled
        }.findAll()
    }

    GeneralInfo refresh(Long id) {
        GeneralInfo generalInfo = GeneralInfo.findById(id)
        return generalInfo
    }

    int count(boolean enabled) {
        return GeneralInfo.countByEnabled(enabled)
    }

    GeneralInfo delete(GeneralInfo generalInfo, LoginManager loginManager) {
        if (!generalInfo) {
            return null
        }
        generalInfo = GeneralInfo.findById(generalInfo.id)
        generalInfo.enabled = !generalInfo.enabled
        return create(generalInfo, loginManager)
    }

    GeneralInfo byEmail(String generalInfo) {
        return GeneralInfo.findByEmailAndEnabled(generalInfo, true)
    }

    GeneralInfo byId(Long id) {
        return GeneralInfo.findByIdAndEnabled(id, true)
    }

    GeneralInfo byName(String name) {
        return GeneralInfo.findByNameAndEnabled(name, true)
    }

    Long findMaxCode() {
        GeneralInfo generalInfo = GeneralInfo.withCriteria {
            order("id", "desc")
        }.find() as GeneralInfo
        if (!generalInfo) {
            return 0
        }
        return generalInfo.code
    }

}
