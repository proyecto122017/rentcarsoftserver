package edu.utesa.proyecto.server.rentcarsoft.junit

import groovy.transform.CompileStatic

@CompileStatic
class SessionTest {

    String username
    String session
    String key
    Date date


    @Override
    String toString() {
        return "SessionTest{" +
                "username='" + username + '\'' +
                ", session='" + session + '\'' +
                ", key='" + key + '\'' +
                ", date=" + date +
                '}';
    }
}
