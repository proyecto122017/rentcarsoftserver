package edu.utesa.proyecto.server.rentcarsoft.junit

import edu.utesa.proyecto.server.rentcarsoft.domains.location.Coordinates
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.GeneralInfo
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.administration.*
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.jsoup.Connection
import org.jsoup.Jsoup

/**
 *  Created by Desckop on 17/7/17.
 */
@CompileStatic
class AdministratorTest {

    protected final String userAgent = 'Ronald Rules'
    static final String PARAM_FORM_RESOURCE = "data"

    protected final int TIME_OUT = 20 * 1000

    protected Connection confConnection(Connection.Method method, Connection connection, String session, String data) {
        connection.userAgent(userAgent)
        connection.timeout(TIME_OUT)
        connection.ignoreContentType(true)
        connection.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
        connection.method(method)
        if (session) {
            connection.data(PARAM_FORM_RESOURCE, session)
        }
        connection.data(PARAM_FORM_RESOURCE, data)
        return connection
    }

    private String sendData(Connection.Method method, String url, String session, String data) {
        try {
            Connection.Response queryConnection = confConnection(method, Jsoup.connect(url), session, data).execute()
            return Jsoup.parse(queryConnection.body()).text()
        } catch (Exception ignored) {
            return null
        }
    }

    protected final String sendGET(String url, String session, String data) {
        return sendData(Connection.Method.GET, url, session, data)
    }

    protected final String sendPOST(String url, String session, String data) {
        return sendData(Connection.Method.POST, url, session, data)
    }

    protected final Integer getCount(String url, Object o) {
        String data = sendPOST(url, "", "klk")
        List<Object> genderAPIS = Constants.instance.convert(data, new Object[1].class as Class<Object>) as List<Object>
        return genderAPIS.size()
    }


    void testAPIBranchOffice() {
        String data = sendPOST(Constants.TEST_URL + "administration/branchOffices", "", "klk")
        List<BranchOfficeAPI> branchOfficeAPIS = Constants.instance.convert(data, new BranchOfficeAPI[1].class as Class<Object>) as List<BranchOfficeAPI>
        println "server response: Converted " + Constants.instance.stringify(branchOfficeAPIS)
    }


    void testCreateBranchOffice() {
        BranchOfficeAPI branchOfficeAPI = new BranchOfficeAPI()
        branchOfficeAPI.code = getCount(Constants.TEST_URL + "administration/branchoffice", branchOfficeAPI) + 1
        branchOfficeAPI.branchOfficeType = new Types().toAPI()
        branchOfficeAPI.coordinates = new Coordinates().toAPI()
        branchOfficeAPI.generalInfo = new GeneralInfo().toAPI()
        String data = sendPOST(Constants.TEST_URL + "administration/create/branchoffice", "", Constants.instance.stringify(branchOfficeAPI))
        println data
    }


    void testAPIContact() {
        String data = sendPOST(Constants.TEST_URL + "administration/contact", "", "klk")
        List<ContactAPI> contactAPIS = Constants.instance.convert(data, new ContactAPI[1].class as Class<Object>) as List<ContactAPI>
        println "server response: Converted " + Constants.instance.stringify(contactAPIS)
    }


    void testCreateContact() {
        ContactAPI contactAPI = new ContactAPI()
        contactAPI.code = getCount(Constants.TEST_URL + "administration/contact", contactAPI) + 1
        contactAPI.description = new String() //Description()
        String data = sendPOST(Constants.TEST_URL + "administration/create/contact", "", Constants.instance.stringify(contactAPI))
        println data
    }


    void testAPIEnterprise() {
        String data = sendPOST(Constants.TEST_URL + "administration/enterprises", "", "klk")
        List<EnterpriseAPI> enterpriseAPIS = Constants.instance.convert(data, new EnterpriseAPI[1].class as Class<Object>) as List<EnterpriseAPI>
        println "server response: Converted " + Constants.instance.stringify(enterpriseAPIS)
    }


    void testCreateEnterprise() {
        EnterpriseAPI enterpriseAPI = new EnterpriseAPI()
        enterpriseAPI.code = getCount(Constants.TEST_URL + "administration/enterprise", enterpriseAPI) + 1
        enterpriseAPI.generalInfo = new GeneralInfo().toAPI()
        String data = sendPOST(Constants.TEST_URL + "administration/create/enterprise", "", Constants.instance.stringify(enterpriseAPI))
        println data
    }


    void testAPINotifications() {
        String data = sendPOST(Constants.TEST_URL + "administration/notifications", "", "klk")
        List<NotificationsAPI> notificationsAPIS = Constants.instance.convert(data, new NotificationsAPI[1].class as Class<Object>) as List<NotificationsAPI>
        println "server response: Converted " + Constants.instance.stringify(notificationsAPIS)
    }


    void testCreateNotifications() {
        NotificationsAPI notificationsAPI = new NotificationsAPI()
        notificationsAPI.code = getCount(Constants.TEST_URL + "administration/notifications", notificationsAPI) + 1
        notificationsAPI.description = new String() //Description()
        String data = sendPOST(Constants.TEST_URL + "administration/create/notifications", "", Constants.instance.stringify(notificationsAPI))
        println data
    }


    void testAPIRentcar() {
        String data = sendPOST(Constants.TEST_URL + "administration/rentcars", "", "klk")
        List<RentcarAPI> rentcarAPIS = Constants.instance.convert(data, new RentcarAPI[1].class as Class<Object>) as List<RentcarAPI>
        println "server response: Converted " + Constants.instance.stringify(rentcarAPIS)
    }


    void testCreateRentcar() {
        RentcarAPI rentcarAPI = new RentcarAPI()
        rentcarAPI.code = getCount(Constants.TEST_URL + "administration/rentcar", rentcarAPI) + 1
        rentcarAPI.generalInfo = new GeneralInfo().toAPI()
        String data = sendPOST(Constants.TEST_URL + "administration/create/rentcar", "", Constants.instance.stringify(rentcarAPI))
        println data
    }
}
