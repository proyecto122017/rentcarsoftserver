package edu.utesa.proyecto.server.rentcarsoft.junit

import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.accounting.PaymentAPI
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.jsoup.Connection
import org.jsoup.Jsoup

/**
 *  Created by ronald on 6/5/17.
 */
@CompileStatic
class LocationTest {

    protected final String userAgent = 'Ronald Rules'
    static final String PARAM_FORM_RESOURCE = "data"

    protected final int TIME_OUT = 20 * 1000

    protected Connection confConnection(Connection.Method method, Connection connection, String data) {
        connection.userAgent(userAgent)
        connection.timeout(TIME_OUT)
        connection.ignoreContentType(true)
        connection.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
        connection.method(method)
        connection.data(PARAM_FORM_RESOURCE, data)
        return connection
    }

    private String sendData(Connection.Method method, String url, String data) {
        try {
            Connection.Response queryConnection = confConnection(method, Jsoup.connect(url), data).execute()
            return Jsoup.parse(queryConnection.body()).text()
        } catch (Exception ignored) {
            return null
        }
    }

    protected final String sendGET(String url, String data) {
        return sendData(Connection.Method.GET, url, data)
    }

    protected final String sendPOST(String url, String data) {
        return sendData(Connection.Method.POST, url, data)
    }

    protected final Integer getCount(String url, Object o) {
        String data = sendPOST(url, "klk")
        List<Object> list = Constants.instance.convert(data, new Object[1].class as Class<Object>) as List<Object>
        return list.size()
    }

    void testAPIPayments() {
        String data = sendPOST(Constants.TEST_URL + "accounting/payments", "klk")
        List<PaymentAPI> paymentAPIS = Constants.instance.convert(data, new PaymentAPI[1].class as Class<Object>) as List<PaymentAPI>
        println "server response: Converted " + Constants.instance.stringify(paymentAPIS)
    }

    void testCreatePayments() {
        PaymentAPI paymentAPI = new PaymentAPI()
        paymentAPI.code = getCount(Constants.TEST_URL + "accounting/payments", paymentAPI) + 1
        paymentAPI.paymentType = new Types().toAPI()
        paymentAPI.paymentDate = new Date()
        paymentAPI.amount = new BigDecimal(4000)
        String data = sendPOST(Constants.TEST_URL + "accounting/create/payment", Constants.instance.stringify(paymentAPI))
        println data
    }
}
