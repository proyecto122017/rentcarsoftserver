package edu.utesa.proyecto.server.rentcarsoft.junit

import groovy.transform.CompileStatic
import org.jsoup.Connection
import org.jsoup.Jsoup

/**
 *  Created by ronald on 6/5/17.*/
@CompileStatic
class ResourceTest {

    protected final String userAgent = 'Ronald Rules'
    static final String PARAM_FORM_RESOURCE = "data"

    protected final int TIME_OUT = 20 * 1000

    protected Connection confConnection(Connection.Method method, Connection connection, String data) {
        connection.userAgent(userAgent)
        connection.timeout(TIME_OUT)
        connection.ignoreContentType(true)
        connection.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
        connection.method(method)
        connection.data(PARAM_FORM_RESOURCE, data)
        return connection
    }

    private String sendData(Connection.Method method, String url, String data) {
        try {
            Connection.Response queryConnection = confConnection(method, Jsoup.connect(url), data).execute()
            return Jsoup.parse(queryConnection.body()).text()
        } catch (Exception ignored) {
            return null
        }
    }

    protected final String sendPOST(String url, String data) {
        return sendData(Connection.Method.GET, url, data)
    }

//    @Test
//    void testAPIUserss() {
//        String data = sendPOST("http://localhost:8085/api/external/profesors", "klk")
//        String descripted = Constants.descifrar(data, Constants.instance.KEY)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<ProfesorAPI> profesorAPIS = Constants.instance.convert(descripted, new ProfesorAPI[1].class) as List<ProfesorAPI>
//        println "server response: Converted " + Constants.instance.stringify(profesorAPIS)
//
//    }
//
//    @Test
//    void testAPIGrados() {
//        String data = sendPOST("http://localhost:8085/api/external/grados", "klk")
//        String descripted = Constants.descifrar(data, Constants.instance.KEY)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<GradoAPI> gradoAPIS = Constants.instance.convert(descripted, List.class) as List<GradoAPI>
//        println "server response: Converted " + Constants.instance.stringify(gradoAPIS)
//
//    }
//
//    @Test
//    void testAPIMaterias() {
//        String data = sendPOST("http://localhost:8085/api/external/materias", "Octavo")
//        String descripted = Constants.descifrar(data, Constants.instance.KEY)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<MateriaAPI> materiaAPIS = Constants.instance.convert(descripted, List.class) as List<MateriaAPI>
//        println "server response: Converted " + Constants.instance.stringify(materiaAPIS)
//
//    }
//
//    @Test
//    void apiProfesorsYelson() {
//        String data = sendPOST(YELSON_SERVER_URL + "/sync/teachers", "")
//        println YELSON_SERVER_URL + "/sync/teachers"
//        String descripted = Constants.descifrar(data, 999999)
//        println "server response ENCRIPT: " + data
//        println "server response: DESCRIPT " + descripted
//        List<ProfesorAPI> profesorAPIS = Constants.instance.convert(descripted, List.class) as List<ProfesorAPI>
//        println "server response: Converted " + Constants.instance.stringify(profesorAPIS)
//    }
}
