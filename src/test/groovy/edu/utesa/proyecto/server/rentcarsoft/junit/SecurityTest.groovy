package edu.utesa.proyecto.server.rentcarsoft.junit

import edu.utesa.proyecto.server.rentcarsoft.models.communication.LoginAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.GenderAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.security.UserAPI
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.jsoup.Connection
import org.jsoup.Jsoup

/**
 *  Created by ronald on 6/5/17.*/
@CompileStatic
class SecurityTest {

    protected final String userAgent = 'Ronald Rules'
    static final String PARAM_FORM_RESOURCE = "data"

    protected final int TIME_OUT = 20 * 1000

    protected Connection confConnection(Connection.Method method, Connection connection, String session, String data) {
        connection.userAgent(userAgent)
        connection.timeout(TIME_OUT)
        connection.ignoreContentType(true)
        connection.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
        connection.method(method)
        if (session) {
            connection.data(PARAM_FORM_RESOURCE, session)
        }
        connection.data(PARAM_FORM_RESOURCE, data)
        return connection
    }

    private String sendData(Connection.Method method, String url, String session, String data) {
        try {
            Connection.Response queryConnection = confConnection(method, Jsoup.connect(url), session, data).execute()
            return Jsoup.parse(queryConnection.body()).text()
        } catch (Exception ignored) {
            return null
        }
    }

    protected final String sendGET(String url, String session, String data) {
        return sendData(Connection.Method.GET, url, session, data)
    }

    protected final String sendPOST(String url, String session, String data) {
        return sendData(Connection.Method.POST, url, session, data)
    }

    protected final Integer getCount(String url, Object o) {
        String data = sendPOST(url, "", "klk")
        List<Object> genderAPIS = Constants.instance.convert(data, new Object[1].class as Class<Object>) as List<Object>
        return genderAPIS.size()
    }

    void testLogin() {
        LoginAPI loginAPI = new LoginAPI()
        loginAPI.username = "generic"
        loginAPI.password = "generic"
        String data = sendPOST(Constants.TEST_URL + "security/login", null, Constants.instance.stringify(loginAPI))
        println "server response: Converted " + Constants.instance.stringify(loginAPI)
        SessionTest sessionTest = Constants.instance.convert(data, SessionTest.class)
        println "" + sessionTest.toString()
         println data
    }


    void testAPIUsers() {
        String data = sendPOST(Constants.TEST_URL + "security/users", null, "klk")
        List<UserAPI> userAPIS = Constants.instance.convert(data, new UserAPI[1].class as Class<Object>) as List<UserAPI>
        println "server response: Converted " + Constants.instance.stringify(userAPIS)
    }


    void testAPIGenders() {
        String data = sendPOST(Constants.TEST_URL + "security/genders", null, "klk")
        List<GenderAPI> genderAPIS = Constants.instance.convert(data, new GenderAPI[1].class as Class<Object>) as List<GenderAPI>
        println "server response: Converted " + Constants.instance.stringify(genderAPIS)
    }


    void testAPIGendersLike() {
        String data = sendPOST(Constants.TEST_URL + "security/genderlike", null, "m")
        println data
        List<GenderAPI> genderAPIS = Constants.instance.convert(data, new GenderAPI[1].class as Class<Object>) as List<GenderAPI>
        println "server response: Converted " + Constants.instance.stringify(genderAPIS)
    }


    void testCreateGender() {
        GenderAPI genderAPI = new GenderAPI()
        genderAPI.code = getCount(Constants.TEST_URL + "security/genders", genderAPI) + 1
//        genderAPI.code = 1
        genderAPI.description = "mujer"
        String data = sendPOST(Constants.TEST_URL + "security/create/gender", null, Constants.instance.stringify(genderAPI))
        println data
    }
}
