package edu.utesa.proyecto.server.rentcarsoft.junit

import edu.utesa.proyecto.server.rentcarsoft.domains.accounting.Payment
import edu.utesa.proyecto.server.rentcarsoft.domains.utils.Types
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.InsuranceAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.OfferAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.RentalAPI
import edu.utesa.proyecto.server.rentcarsoft.models.communication.control.ReservationAPI
import edu.utesa.proyecto.server.rentcarsoft.utils.Constants
import groovy.transform.CompileStatic
import org.jsoup.Connection
import org.jsoup.Jsoup

/**
 *  Created by Desckop on 17/7/17.*/
@CompileStatic
class ControlTest {

    protected final String userAgent = 'Ronald Rules'
    static final String PARAM_FORM_RESOURCE = "data"

    protected final int TIME_OUT = 20 * 1000

    protected Connection confConnection(Connection.Method method, Connection connection, String data) {
        connection.userAgent(userAgent)
        connection.timeout(TIME_OUT)
        connection.ignoreContentType(true)
        connection.header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
        connection.method(method)
        connection.data(PARAM_FORM_RESOURCE, data)
        return connection
    }

    private String sendData(Connection.Method method, String url, String data) {
        try {
            Connection.Response queryConnection = confConnection(method, Jsoup.connect(url), data).execute()
            return Jsoup.parse(queryConnection.body()).text()
        } catch (Exception ignored) {
            return null
        }
    }

    protected final String sendGET(String url, String data) {
        return sendData(Connection.Method.GET, url, data)
    }

    protected final String sendPOST(String url, String data) {
        return sendData(Connection.Method.POST, url, data)
    }

    protected final Integer getCount(String url, Object o) {
        String data = sendPOST(url, "klk")
        List<Object> list = Constants.instance.convert(data, new Object[1].class as Class<Object>) as List<Object>
        return list.size()
    }

    void testAPIInsurance() {
        String data = sendPOST(Constants.TEST_URL + "control/insurance", "klk")
        List<InsuranceAPI> insuranceAPIS = Constants.instance.convert(data, new InsuranceAPI[1].class as Class<Object>) as List<InsuranceAPI>
        println "server response: Converted " + Constants.instance.stringify(insuranceAPIS)
    }

    void testCreateInsurance() {
        InsuranceAPI insuranceAPI = new InsuranceAPI()
        insuranceAPI.code = getCount(Constants.TEST_URL + "control/insurance", insuranceAPI) + 1
        insuranceAPI.description = new String()//Description()
        insuranceAPI.coverage = new Long(4000)
        String data = sendPOST(Constants.TEST_URL + "control/create/insurance", Constants.instance.stringify(insuranceAPI))
        println data
    }

    void testAPIOffer() {
        String data = sendPOST(Constants.TEST_URL + "control/offer", "klk")
        List<OfferAPI> offerAPIS = Constants.instance.convert(data, new OfferAPI[1].class as Class<Object>) as List<OfferAPI>
        println "server response: Converted " + Constants.instance.stringify(offerAPIS)
    }

    void testCreateOffer() {
        OfferAPI offerAPI = new OfferAPI()
        offerAPI.code = getCount(Constants.TEST_URL + "control/offer", offerAPI) + 1
        offerAPI.description = new String()
        String data = sendPOST(Constants.TEST_URL + "control/create/offer", Constants.instance.stringify(offerAPI))
        println data
    }

    void testAPIRental() {
        String data = sendPOST(Constants.TEST_URL + "control/rentals", "klk")
        List<RentalAPI> rentalAPIS = Constants.instance.convert(data, new RentalAPI[1].class as Class<Object>) as List<RentalAPI>
        println "server response: Converted " + Constants.instance.stringify(rentalAPIS)
    }

    void testCreateRental() {
        RentalAPI rentalAPI = new RentalAPI()
        rentalAPI.code = getCount(Constants.TEST_URL + "control/rental", rentalAPI) + 1
        rentalAPI.description = new String()
        rentalAPI.rentalType = new Types().toAPI()
        rentalAPI.pickup_date = new Date()
        rentalAPI.return_date = new Date()
        String data = sendPOST(Constants.TEST_URL + "control/create/rental", Constants.instance.stringify(rentalAPI))
        println data
    }

    void testAPIReservation() {
        String data = sendPOST(Constants.TEST_URL + "control/reservation", "klk")
        List<ReservationAPI> reservationAPIS = Constants.instance.convert(data, new ReservationAPI[1].class as Class<Object>) as List<ReservationAPI>
        println "server response: Converted " + Constants.instance.stringify(reservationAPIS)
    }

    void testCreateReservation() {
        ReservationAPI reservationAPI = new ReservationAPI()
        reservationAPI.code = getCount(Constants.TEST_URL + "control/reservation", reservationAPI) + 1
        reservationAPI.description = new String()
        reservationAPI.pickup_date = new Date()
        reservationAPI.return_date = new Date()
        reservationAPI.payment = new Payment().toAPI()

        String data = sendPOST(Constants.TEST_URL + "create/control/reservation", Constants.instance.stringify(reservationAPI))
        println data
    }
}
